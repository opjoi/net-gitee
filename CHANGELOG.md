# 更新说明
## V0.2.5-beta(2021-03-16)
- 修复datetime类型空值问题
- 增加企业基础模型
- 完成user接口
## V0.2.4 (2021-03-11)
- 优化CodeGen（修复可空类型）
- 增加更新企业issue接口
- 修复issue返回值模型
## V0.2.3-beta (2021-03-10)
- 整理Python脚本
- 更新request类为partial，优化codegen代码
- 完成所有request类
- 完成webhook相关模型
- 更新gitee openApi v3.4.00版本
## V0.2.2 (2021-03-06)
- 修复创建分支links模型问题
## V0.2.2-beta (2021-03-04)
- 使用IsSuccessStatusCode 修饰成功状态
## V0.2.1 (2021-03-04)
- 完成webhook模型改造
## V0.1.8 (2021-03-03)
- 完成webhook Issue模型支持
## V0.1.7-beta (2021-03-02)
- 替换部分Request类
- 增加CodeGen C#版（Leo）
## V0.1.6-beta (2021-03-01)
- 增加webhook支持
- 完成webhook签名校验
- 增加CodeGen Python版（rey）
## V0.1.5-beta (2021-02-28)
- 重新基础Execute方法
- 集合GiteeOpenAPi,支持列表和单实体
- 支持创建分支功能
## V0.1.4-beta (2021-02-27)
- 修复查询企业任务
## V0.1.3-beta (2021-02-27)(缺陷)
- 命名规则设计
- 第一个功能实现
## V0.1.2-beta (2021-02-25)
- 模型设计
- 授权
- 刷新授权
## V0.1.1-beta (2021-02-24)
- 继续框架设计
## V0.1.0-beta (2021-02-17)
- 创建代码仓库 API
- 框架设计