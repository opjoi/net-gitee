﻿        /// <summary>
        /// {{summary}}
        /// </summary>
       [JsonProperty("{{jsonProperty}}")]
        public {{propertyType}} {{propertyName}} { get; set; }
