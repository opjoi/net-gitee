﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class Note
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 评论内容。eg：好的东西应该开源...
        /// </summary>
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 评论的作者信息。
        /// </summary>
        [JsonProperty("user")]
        public User User { get; set; }
        /// <summary>
        /// 评论的创建时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        /// <summary>
        /// 评论的更新时间。eg：2020-11-11T11:11:11+08:00
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        /// <summary>
        /// 这条评论在 Gitee 上的 url。eg：https://gitee.com/oschina/git-osc#note_1
        /// </summary>
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        /// <summary>
        /// 在代码 commit 评论中对应的代码位置。eg：76ec1c6df700af34ae5f8dd00bd7bcb56c1bd706_9_9
        /// </summary>
        [JsonProperty("position")]
        public string Position { get; set; }
        /// <summary>
        /// 在代码 commit 评论中对应的 commit id。eg：611de62f634d353bb75a290f59fa238ff2d8d3c7
        /// </summary>
        [JsonProperty("commit_id")]
        public string commitId { get; set; }
    }
}