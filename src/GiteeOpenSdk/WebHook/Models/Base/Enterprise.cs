﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class Enterprise
    {
        /// <summary>
        /// 企业名。eg：开源中国
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 企业在 Gitee 上的 url。eg：https://gitee.com/oschina
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
