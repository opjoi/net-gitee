﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class PullRequest
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 与上面 id 一致
        /// </summary>
        [JsonProperty("number")]
        public int Number { get; set; }
        /// <summary>
        /// PR 状态。eg：open
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// PR 在 Gitee 上 url。eg：https://gitee.com/oschina/pulls/1
        /// </summary>
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        /// <summary>
        /// PR diff 信息 url。eg：https://gitee.com/oschina/pulls/1.diff
        /// </summary>
        [JsonProperty("diff_url")]
        public string DiffUrl { get; set; }
        /// <summary>
        /// PR patch 信息 url。eg：https://gitee.com/oschina/pulls/1.patch
        /// </summary>
        [JsonProperty("patch_url")]
        public string PatchUrl { get; set; }
        /// <summary>
        /// PR 的标题。eg：这是一个 PR 标题
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// PR 的内容。eg：升级服务...
        /// </summary>
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// PR 的创建时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        /// <summary>
        /// PR 的更新时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        /// <summary>
        /// PR 的关闭时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("closed_at")]
        public string ClosedAt { get; set; }
        /// <summary>
        /// PR 的合并时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("merged_at")]
        public string MergedAt { get; set; }
        /// <summary>
        /// PR 合并产生的 commit id。eg：51b1acb1b4044fcdb2ff8a75ad15a4b655101754
        /// </summary>
        [JsonProperty("merge_commit_sha")]
        public string MergeCommitSha { get; set; }
        /// <summary>
        /// PR 的源分支目标。eg：refs/pull/1/MERGE
        /// </summary>
        [JsonProperty("merge_reference_name")]
        public string MergeReferenceName { get; set; }
        /// <summary>
        /// PR 的创建者。
        /// </summary>
        [JsonProperty("user")]
        public User User { get; set; }
        /// <summary>
        /// PR 的负责人。
        /// </summary>
        [JsonProperty("assignee")]
        public User Assignee { get; set; }
        /// <summary>
        /// PR 的审核人。
        /// </summary>
        [JsonProperty("assignees")]
        public List<User> Assignees { get; set; }
        /// <summary>
        /// PR 的测试者。
        /// </summary>
        [JsonProperty("tester")]
        public string Tester { get; set; }
        /// <summary>
        /// PR 的所有测试者。
        /// </summary>
        [JsonProperty("testers")]
        public List<User> Testers { get; set; }
        /// <summary>
        /// PR 是否需要测试。
        /// </summary>
        [JsonProperty("need_test")]
        public bool NeedTest { get; set; }
        /// <summary>
        /// PR 是否需要审核。
        /// </summary>
        [JsonProperty("need_review")]
        public bool NeedReview { get; set; }
        /// <summary>
        /// PR 所属的里程碑。
        /// </summary>
        [JsonProperty("milestone")]
        public Milestone Milestone { get; set; }
        /// <summary>
        /// PR 的源分支。
        /// </summary>
        [JsonProperty("head")]
        public Branch Head { get; set; }
        /// <summary>
        /// PR 要合并的目标分支
        /// </summary>
        [JsonProperty("base")]
        public Branch Base { get; set; }
        /// <summary>
        /// PR 是否已合并。
        /// </summary>
        [JsonProperty("merged")]
        public bool Merged { get; set; }
        /// <summary>
        /// PR 是否可以合并。
        /// </summary>
        [JsonProperty("mergeable")]
        public bool Mergeable { get; set; }
        /// <summary>
        /// PR 的合并状态。eg：unchecked
        /// </summary>
        [JsonProperty("merge_status")]
        public string MergeStatus { get; set; }
        /// <summary>
        /// PR 的修改者。
        /// </summary>
        [JsonProperty("updated_by")]
        public User UpdatedBy { get; set; }
        /// <summary>
        /// PR 的总评论数量。
        /// </summary>
        [JsonProperty("comments")]
        public int Comments { get; set; }
        /// <summary>
        /// PR 的总 commit 数量。
        /// </summary>
        [JsonProperty("commits")]
        public int Commits { get; set; }
        /// <summary>
        /// PR 新增了多少行。
        /// </summary>
        [JsonProperty("additions")]
        public int Additions { get; set; }
        /// <summary>
        /// PR 删除了多少行。
        /// </summary>
        [JsonProperty("deletions")]
        public int Deletions { get; set; }
        /// <summary>
        /// PR 修改了多少行。
        /// </summary>
        [JsonProperty("changed_files")]
        public int ChangedFiles { get; set; }
    }
}
