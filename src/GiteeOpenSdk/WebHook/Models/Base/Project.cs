﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class Project
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 仓库名。eg：gitee
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库所属的空间地址。eg：oschian
        /// </summary>
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 完整的名字，name + path。eg：gitee/oschian
        /// </summary>
        [JsonProperty("full_name")]
        public string FullName { get; set; }
        /// <summary>
        /// 仓库的所有者。
        /// </summary>
        [JsonProperty("owner")]
        public User Owner { get; set; }
        /// <summary>
        /// 是否公开。
        /// </summary>
        [JsonProperty("private")]
        public bool Private { get; set; }
        /// <summary>
        /// 对应 Gitee 的 url。eg：https://gitee.com/oschina/git-osc
        /// </summary>
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        /// <summary>
        /// 与上面 html_url 一致
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// 仓库描述。eg：这是一个开源仓库...
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 是不是 fork 仓库。
        /// </summary>
        [JsonProperty("fork")]
        public bool Fork { get; set; }
        /// <summary>
        /// 仓库的创建时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        /// <summary>
        /// 仓库的更新时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
        /// <summary>
        /// 仓库的最近一次推送时间。eg：2020-01-01T00:00:00+08:00
        /// </summary>
        [JsonProperty("pushed_at")]
        public string PushedAt { get; set; }
        /// <summary>
        /// 仓库的 git 地址。eg：git://gitee.com:oschina/git-osc.git
        /// </summary>
        [JsonProperty("git_url")]
        public string GitUrl { get; set; }
        /// <summary>
        /// 仓库的 ssh 地址。eg：git@gitee.com:oschina/git-osc.git
        /// </summary>
        [JsonProperty("ssh_url")]
        public string SshUrl { get; set; }
        /// <summary>
        /// 仓库的 clone 地址。eg：https://gitee.com/oschina/git-osc.git
        /// </summary>
        [JsonProperty("clone_url")]
        public string CloneUrl { get; set; }
        /// <summary>
        /// 仓库的 svn 地址。eg：svn://gitee.com/oschina/git-osc
        /// </summary>
        [JsonProperty("svn_url")]
        public string SvnUrl { get; set; }
        /// <summary>
        /// 与上面的 clone_url 一致。
        /// </summary>
        [JsonProperty("git_http_url")]
        public string GitHttpUrl { get; set; }
        /// <summary>
        /// 与上面的 ssh_url 一致。
        /// </summary>
        [JsonProperty("git_ssh_url")]
        public string GitSshUrl { get; set; }
        /// <summary>
        /// 与上面的 svn_url 一致。
        /// </summary>
        [JsonProperty("git_svn_url")]
        public string GitSvnUrl { get; set; }
        /// <summary>
        /// 仓库的网页主页。eg：https://gitee.com
        /// </summary>
        [JsonProperty("homepage")]
        public string Homepage { get; set; }
        /// <summary>
        /// 仓库的 star 数量。
        /// </summary>
        [JsonProperty("stargazers_count")]
        public int StargazersCount { get; set; }
        /// <summary>
        /// 仓库的 watch 数量。
        /// </summary>
        [JsonProperty("watchers_count")]
        public int WatchersCount { get; set; }
        /// <summary>
        /// 仓库的 fork 数量。
        /// </summary>
        [JsonProperty("forks_count")]
        public int ForksCount { get; set; }
        /// <summary>
        /// 仓库的编程语言。eg： Ruby
        /// </summary>
        [JsonProperty("language")]
        public string Language { get; set; }
        /// <summary>
        /// 仓库的是否开启了 issue 功能。
        /// </summary>
        [JsonProperty("has_issues")]
        public bool HasIssues { get; set; }
        /// <summary>
        /// 仓库的是否开启了 wiki 功能。
        /// </summary>
        [JsonProperty("has_wiki")]
        public string HasWiki { get; set; }
        /// <summary>
        /// 仓库的是否开启了 page 服务。
        /// </summary>
        [JsonProperty("has_pages")]
        public string HasPages { get; set; }
        /// <summary>
        /// 仓库的开源协议。eg：MIT
        /// </summary>
        [JsonProperty("license")]
        public string License { get; set; }
        /// <summary>
        /// 仓库开启状态的 issue 总数。
        /// </summary>
        [JsonProperty("open_issues_count")]
        public int OpenIssuesCount { get; set; }
        /// <summary>
        /// 仓库的默认复制。eg：master
        /// </summary>
        [JsonProperty("default_branch")]
        public string DefaultBranch { get; set; }
        /// <summary>
        /// 仓库所属的 Gitee 地址。eg：oschina
        /// </summary>
        [JsonProperty("namespace")]
        public string Namespace { get; set; }
        /// <summary>
        /// 与上面的 full_name 一致。
        /// </summary>
        [JsonProperty("NameWithNamespace")]
        public string name_with_namespace { get; set; }
        /// <summary>
        /// 仓库的在 Gitee 的资源唯一标识。eg：oschia/git-osc
        /// </summary>
        [JsonProperty("path_with_namespace")]
        public string PathWithNamespace { get; set; }
    }
}
