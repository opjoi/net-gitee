﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.Common
{
    /// <summary>
    /// 参数类型 query,path,fromData
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class RequestMethodAttribute : Attribute
    {
        public RequestMethodAttribute(string httpMethod)
        {
            this.RequestMethod = httpMethod;
        }

        private string _RequestMethod;
        /// <summary>
        /// 参数类型
        /// </summary>
        public string RequestMethod
        {
            get { return _RequestMethod; }
            set { _RequestMethod = value; }
        }
    }
}
