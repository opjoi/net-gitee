using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 收藏/取消收藏文件节点
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocNodesDocNodeIdCollectionRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// doc_node 节点 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_node_id")]
        public int DocNodeId { get; set; }
        /// <summary>
        /// 收藏: set; 取消收藏: unset
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("operate_type")]
        public string OperateType { get; set; }

    }
}