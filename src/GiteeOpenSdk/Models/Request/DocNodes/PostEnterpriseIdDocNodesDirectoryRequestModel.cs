using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 创建文件夹
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdDocNodesDirectoryRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 文件夹名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 父级 id （默认：0）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }
        /// <summary>
        /// 归属的项目 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 权限; 继承: inherit; 私有: private; 只读 read_only; 读写: read_write
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("auth_type")]
        public string AuthType { get; set; }
        /// <summary>
        /// 迭代 ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("scrum_sprint_id")]
        public string ScrumSprintId { get; set; }

    }
}