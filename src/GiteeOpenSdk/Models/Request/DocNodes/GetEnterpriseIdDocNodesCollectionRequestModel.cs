using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 获取授权用户收藏的文件节点（仅顶层）
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdDocNodesCollectionRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 项目 id（默认：0）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 排序字段(name size updated_at created_at creator_id public)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 可选。升序/降序
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 迭代 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scrum_sprint_id")]
        public int? ScrumSprintId { get; set; }
        /// <summary>
        /// 是否仅展示文档
        /// </summary>
        [RequestType("query")]
        [JsonProperty("wiki_info_only")]
        public bool? WikiInfoOnly { get; set; }

    }
}