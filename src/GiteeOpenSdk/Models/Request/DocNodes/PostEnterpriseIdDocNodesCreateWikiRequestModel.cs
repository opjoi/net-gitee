using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 新建 Wiki
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdDocNodesCreateWikiRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 仓库 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }

    }
}