using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 上传附件（覆盖）
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdDocNodesCoverAttachFileRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        ///// <summary>
        ///// 上传的文件
        ///// </summary>
        //[RequestType("formData")]
        //[JsonProperty("file")]
        //public unknow File { get; set; }
        /// <summary>
        /// 上传的层级
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }
        /// <summary>
        /// 覆盖的文件节点
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("file_id")]
        public int FileId { get; set; }

    }
}