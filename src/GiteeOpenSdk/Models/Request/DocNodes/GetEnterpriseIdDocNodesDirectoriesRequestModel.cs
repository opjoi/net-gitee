using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 获取文件夹
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdDocNodesDirectoriesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 迭代 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scrum_sprint_id")]
        public int? ScrumSprintId { get; set; }
        /// <summary>
        /// 迭代默认生成的文件夹 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sprint_doc_id")]
        public int? SprintDocId { get; set; }

    }
}