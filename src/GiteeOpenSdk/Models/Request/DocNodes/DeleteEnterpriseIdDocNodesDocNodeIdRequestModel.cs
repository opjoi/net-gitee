using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 彻底删除文件节点
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdDocNodesDocNodeIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_node_id")]
        public int DocNodeId { get; set; }

    }
}