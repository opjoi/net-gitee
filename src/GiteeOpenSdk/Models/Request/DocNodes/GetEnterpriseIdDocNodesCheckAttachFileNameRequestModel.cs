using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 检测附件是否重名
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdDocNodesCheckAttachFileNameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 附件名称
        /// </summary>
        [RequestType("query")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 层级 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }

    }
}