using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 置顶节点
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocNodesDocNodeIdIsTopRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// doc_node 节点 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_node_id")]
        public int DocNodeId { get; set; }
        /// <summary>
        /// 是否置顶（置顶: 1，取消置顶: 0）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("value")]
        public int Value { get; set; }

    }
}