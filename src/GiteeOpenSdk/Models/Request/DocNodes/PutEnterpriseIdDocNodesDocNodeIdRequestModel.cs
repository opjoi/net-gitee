using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 更新文件节点
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocNodesDocNodeIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// doc_node 节点 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_node_id")]
        public int DocNodeId { get; set; }
        /// <summary>
        /// 文件夹名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 设置访问密码（此字段填入空串可取消密码）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("password")]
        public string Password { get; set; }

    }
}