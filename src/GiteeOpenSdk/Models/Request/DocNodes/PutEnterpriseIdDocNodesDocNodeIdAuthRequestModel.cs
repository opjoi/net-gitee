using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 更新文件节点的权限
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocNodesDocNodeIdAuthRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// doc_node 节点 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_node_id")]
        public int DocNodeId { get; set; }
        /// <summary>
        /// 访客的访问权限; (无权限: 0; 只读: 1;)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("guess_access")]
        public int? GuessAccess { get; set; }
        /// <summary>
        /// 企业成员的访问权限(无权限: 0; 只读: 1; 读写: 2)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("enterprise_access")]
        public int? EnterpriseAccess { get; set; }
        /// <summary>
        /// 关联类型(target_type: program|group|user)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access[target_type]")]
        public object AccessTargetType { get; set; }
        /// <summary>
        /// 关联类型的 id(如 target_type = program，则此处应为对应 program 的 id)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access[target_id]")]
        public object AccessTargetId { get; set; }
        /// <summary>
        /// 访问属性（无权限: 0; 只读: 1; 读写: 2）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access[access_level]")]
        public object AccessAccessLevel { get; set; }

    }
}