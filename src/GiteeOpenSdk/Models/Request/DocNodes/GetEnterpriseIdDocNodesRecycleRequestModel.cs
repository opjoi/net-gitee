using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 获取回收站的内容列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdDocNodesRecycleRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 父级 id （默认：0）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }
        /// <summary>
        /// 迭代 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scrum_sprint_id")]
        public int? ScrumSprintId { get; set; }

    }
}