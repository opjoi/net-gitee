using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 批量移除到回收站
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdDocNodesBatchRecycleRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 文件 id，使用,，隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("doc_node_ids")]
        public string DocNodeIds { get; set; }

    }
}