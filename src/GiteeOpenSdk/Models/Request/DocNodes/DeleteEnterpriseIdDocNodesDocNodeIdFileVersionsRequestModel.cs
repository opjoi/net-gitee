using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 删除历史版本
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdDocNodesDocNodeIdFileVersionsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 节点 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_node_id")]
        public int DocNodeId { get; set; }
        /// <summary>
        /// 要删除的文件id（多个用逗号分隔）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("attach_file_ids")]
        public string AttachFileIds { get; set; }

    }
}