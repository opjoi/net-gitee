using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 批量收藏/取消收藏文件节点
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocNodesBatchCollectionRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 文件 id，使用,，隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("doc_node_ids")]
        public string DocNodeIds { get; set; }
        /// <summary>
        /// 收藏: set; 取消收藏: unset
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("operate_type")]
        public string OperateType { get; set; }

    }
}