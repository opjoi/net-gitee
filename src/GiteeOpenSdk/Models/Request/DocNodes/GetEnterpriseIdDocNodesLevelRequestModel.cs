using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 获取文件节点列表（层级）
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdDocNodesLevelRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 父级 id（默认：0）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 可筛选类型：directory, recycle
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 文件类型（WikiInfo,DocDirectory,AttachFile），支持多选，用逗号分割
        /// </summary>
        [RequestType("query")]
        [JsonProperty("file_type")]
        public string FileType { get; set; }

    }
}