using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 查看文件节点详情
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdDocNodesDocNodeIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_node_id")]
        public int DocNodeId { get; set; }

    }
}