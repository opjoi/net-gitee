using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DocNodes
{
    /// <summary>
    /// 批量移动文件节点
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocNodesBatchMoveRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 文件 id，使用,，隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("doc_node_ids")]
        public string DocNodeIds { get; set; }
        /// <summary>
        /// 挂载到文件下（此处填入上级的 doc_node 节点的 id; 若移动到根目录，parent_id: 0）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }
        /// <summary>
        /// 挂载到某项目下（此处填入项目 id）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }

    }
}