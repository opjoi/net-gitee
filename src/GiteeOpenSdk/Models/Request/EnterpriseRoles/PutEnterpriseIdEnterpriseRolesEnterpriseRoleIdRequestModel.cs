using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.EnterpriseRoles
{
    /// <summary>
    /// 更新企业角色
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdEnterpriseRolesEnterpriseRoleIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 角色的描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 是否设置为企业默认角色
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("is_default")]
        public bool? IsDefault { get; set; }
        /// <summary>
        ///           参数名含义：///          week_report: 周报///          issue: 任务///          program: 项目///          project: 仓库///          doc: 文档///          statistic: 统计///          admin: 管理权限///          格式如下（具体可对照着 web 端的权限设置）：///          &quot;rule&quot;: {&quot;week_report&quot;:{&quot;general&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;update_history&quot;:false},&quot;global&quot;:{&quot;read&quot;:true}},&quot;issue&quot;:{&quot;general&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;modify_attr&quot;:true,&quot;modify_main&quot;:true,&quot;destroy&quot;:true},&quot;global&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;modify_attr&quot;:true,&quot;modify_main&quot;:true,&quot;destroy&quot;:true}},&quot;program&quot;:{&quot;general&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;destroy&quot;:true,&quot;setting&quot;:true},&quot;global&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;destroy&quot;:true,&quot;setting&quot;:true}},&quot;project&quot;:{&quot;general&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;destroy&quot;:true,&quot;pull&quot;:true,&quot;push&quot;:true,&quot;merge_pr&quot;:true,&quot;setting&quot;:true},&quot;global&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;destroy&quot;:true,&quot;pull&quot;:true,&quot;push&quot;:true,&quot;open_source&quot;:true,&quot;read_internal_source&quot;:true,&quot;merge_pr&quot;:true,&quot;setting&quot;:true}},&quot;doc&quot;:{&quot;general&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;update&quot;:true,&quot;destroy&quot;:true,&quot;setting&quot;:true},&quot;global&quot;:{&quot;read&quot;:true,&quot;create&quot;:true,&quot;update&quot;:true,&quot;destroy&quot;:true,&quot;setting&quot;:true}},&quot;member&quot;:{&quot;read&quot;:true},&quot;statistic&quot;:{&quot;read&quot;:true},&quot;admin&quot;:{&quot;member&quot;:{&quot;add_member&quot;:true,&quot;destroy&quot;:true,&quot;setting&quot;:true,&quot;create_group&quot;:true,&quot;manage_group&quot;:true,&quot;change_role&quot;:false},&quot;log&quot;:{&quot;read&quot;:true},&quot;emergency_log&quot;:{&quot;read&quot;:true},&quot;git_lfs&quot;:{&quot;read&quot;:true},&quot;order&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;hook&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;key&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;issue_type_status&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;label_manage&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;info&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;role&quot;:{&quot;read&quot;:true,&quot;setting&quot;:false},&quot;emergency&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;security&quot;:{&quot;read&quot;:true,&quot;setting&quot;:true},&quot;notice&quot;:{&quot;read&quot;:true,&quot;create&quot;:true},&quot;message&quot;:{&quot;read&quot;:true,&quot;create&quot;:true},&quot;is_admin&quot;:true}}}///
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("rule")]
        public string Rule { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_role_id")]
        public int EnterpriseRoleId { get; set; }

    }
}