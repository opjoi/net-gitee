using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.EnterpriseRoles
{
    /// <summary>
    /// 获取企业角色列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdEnterpriseRolesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 排序字段(created_at: 创建时间 updated_at: 更新时间)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 筛选项 (system: 系统默认类型; custom: 自定义类型; admin: 管理员类型; can_operate: 非企业拥有者角色, can_invite: 能添加成员角色)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}