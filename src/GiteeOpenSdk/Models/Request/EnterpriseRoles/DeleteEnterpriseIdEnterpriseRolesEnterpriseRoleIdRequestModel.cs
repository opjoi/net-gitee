using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.EnterpriseRoles
{
    /// <summary>
    /// 删除角色
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdEnterpriseRolesEnterpriseRoleIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_role_id")]
        public int EnterpriseRoleId { get; set; }

    }
}