using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.EnterpriseRoles
{
    /// <summary>
    /// 获取企业角色详情
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdEnterpriseRolesEnterpriseRoleIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_role_id")]
        public int EnterpriseRoleId { get; set; }

    }
}