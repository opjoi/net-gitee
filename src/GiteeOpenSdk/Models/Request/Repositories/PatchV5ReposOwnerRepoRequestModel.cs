using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 更新仓库设置
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5ReposOwnerRepoRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 主页(eg: https://gitee.com)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("homepage")]
        public string Homepage { get; set; }
        /// <summary>
        /// 允许提Issue与否。默认: 允许(true)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("has_issues")]
        public bool? HasIssues { get; set; }
        /// <summary>
        /// 提供Wiki与否。默认: 提供(true)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("has_wiki")]
        public bool? HasWiki { get; set; }
        /// <summary>
        /// 允许用户对仓库进行评论。默认： 允许(true)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("can_comment")]
        public bool? CanComment { get; set; }
        /// <summary>
        /// 允许对“关闭”状态的 Issue 进行评论。默认: 不允许(false)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_comment")]
        public bool? IssueComment { get; set; }
        /// <summary>
        /// 这个Issue涉及到安全/隐私问题，提交后不公开此Issue（可见范围：仓库成员, 企业成员）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("security_hole_enabled")]
        public bool? SecurityHoleEnabled { get; set; }
        /// <summary>
        /// 仓库公开或私有。
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("private")]
        public bool? Private { get; set; }
        /// <summary>
        /// 更新仓库路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 更新默认分支
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("default_branch")]
        public string DefaultBranch { get; set; }
        /// <summary>
        /// 接受 pull request，协作开发
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pull_requests_enabled")]
        public bool? PullRequestsEnabled { get; set; }
        /// <summary>
        /// 是否允许仓库文件在线编辑
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("online_edit_enabled")]
        public bool? OnlineEditEnabled { get; set; }
        /// <summary>
        /// 是否接受轻量级 pull request
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("lightweight_pr_enabled")]
        public bool? LightweightPrEnabled { get; set; }

    }
}