using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 修改仓库推送规则设置
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5ReposOwnerRepoPushConfigRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 启用只能推送自己的提交（所推送提交中的邮箱必须与推送者所设置的提交邮箱一致）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("restrict_push_own_commit")]
        public bool? RestrictPushOwnCommit { get; set; }
        /// <summary>
        /// 启用只允许指定邮箱域名后缀的提交
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("restrict_author_email_suffix")]
        public bool? RestrictAuthorEmailSuffix { get; set; }
        /// <summary>
        /// 指定邮箱域名的后缀
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("author_email_suffix")]
        public string AuthorEmailSuffix { get; set; }
        /// <summary>
        /// 启用提交信息正则表达式校验
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("restrict_commit_message")]
        public bool? RestrictCommitMessage { get; set; }
        /// <summary>
        /// 用于验证提交信息的正则表达式
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("commit_message_regex")]
        public string CommitMessageRegex { get; set; }
        /// <summary>
        /// 启用限制单文件大小
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("restrict_file_size")]
        public bool? RestrictFileSize { get; set; }
        /// <summary>
        /// 限制单文件大小（MB）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("max_file_size")]
        public int? MaxFileSize { get; set; }
        /// <summary>
        /// 仓库管理员不受上述规则限制
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("except_manager")]
        public bool? ExceptManager { get; set; }

    }
}