using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 获取周报可关联issue和pull request
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdWeekReportsRelationsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 周报所属年
        /// </summary>
        [RequestType("query")]
        [JsonProperty("year")]
        public int Year { get; set; }
        /// <summary>
        /// 周报所属周
        /// </summary>
        [RequestType("query")]
        [JsonProperty("week_index")]
        public int WeekIndex { get; set; }

    }
}