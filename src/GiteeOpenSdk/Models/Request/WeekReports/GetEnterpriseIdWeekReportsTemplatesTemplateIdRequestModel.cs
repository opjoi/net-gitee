using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 查看周报模版详情
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdWeekReportsTemplatesTemplateIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 模版ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("template_id")]
        public int TemplateId { get; set; }

    }
}