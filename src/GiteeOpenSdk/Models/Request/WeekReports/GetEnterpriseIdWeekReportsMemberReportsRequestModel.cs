using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 成员周报列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdWeekReportsMemberReportsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        [RequestType("query")]
        [JsonProperty("year")]
        public int? Year { get; set; }
        /// <summary>
        /// 周报所属周
        /// </summary>
        [RequestType("query")]
        [JsonProperty("week_index")]
        public int? WeekIndex { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 团队ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("group_id")]
        public int? GroupId { get; set; }
        /// <summary>
        /// 成员ID，逗号分隔，如：1,2
        /// </summary>
        [RequestType("query")]
        [JsonProperty("member_ids")]
        public string MemberIds { get; set; }
        /// <summary>
        /// 周报日期(格式：2019-03-25)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("date")]
        public string Date { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}