using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 编辑周报模版
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdWeekReportsTemplatesTemplateIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 模版ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("template_id")]
        public int TemplateId { get; set; }
        /// <summary>
        /// 模版名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 模版内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 是否设为默认模版。0(否)或1(是)，默认0(否)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("is_default")]
        public int? IsDefault { get; set; }

    }
}