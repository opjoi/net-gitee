using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 获取未提交周报的用户列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdWeekReportsNotSubmitUsersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        [RequestType("query")]
        [JsonProperty("year")]
        public int Year { get; set; }
        /// <summary>
        /// 当前周数（默认当前周）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("week_index")]
        public int? WeekIndex { get; set; }
        /// <summary>
        /// 项目的 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }

    }
}