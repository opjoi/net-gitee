using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 预览周报
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdWeekReportsPreviewDataRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 需要预览的周报内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 关联动态ids，以逗号分隔，如：3241,2324
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("event_ids")]
        public string EventIds { get; set; }
        /// <summary>
        /// 关联issue ids，以逗号分隔，如：3241,2324
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_ids")]
        public string IssueIds { get; set; }
        /// <summary>
        /// 关联pull request ids，以逗号分隔，如：3241,2324
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pull_request_ids")]
        public string PullRequestIds { get; set; }

    }
}