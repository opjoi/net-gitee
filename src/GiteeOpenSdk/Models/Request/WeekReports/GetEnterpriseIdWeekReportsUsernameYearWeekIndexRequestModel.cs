using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 获取某人某年某周的周报详情
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdWeekReportsUsernameYearWeekIndexRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 用户个性域名
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 周报所处年
        /// </summary>
        [RequestType("path")]
        [JsonProperty("year")]
        public int Year { get; set; }
        /// <summary>
        /// 周报所处周
        /// </summary>
        [RequestType("path")]
        [JsonProperty("week_index")]
        public int WeekIndex { get; set; }

    }
}