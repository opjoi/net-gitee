using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 周报可关联动态列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdWeekReportsRelateEventsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 周报所属年
        /// </summary>
        [RequestType("query")]
        [JsonProperty("year")]
        public int Year { get; set; }
        /// <summary>
        /// 周报所属周
        /// </summary>
        [RequestType("query")]
        [JsonProperty("week_index")]
        public int WeekIndex { get; set; }
        /// <summary>
        /// 一关联的动态ids， 用逗号分隔，如：1,2,3
        /// </summary>
        [RequestType("query")]
        [JsonProperty("event_ids")]
        public int? EventIds { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}