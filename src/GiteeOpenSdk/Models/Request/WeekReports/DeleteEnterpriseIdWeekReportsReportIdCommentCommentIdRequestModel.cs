using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 删除周报某个评论
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdWeekReportsReportIdCommentCommentIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 周报的ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("report_id")]
        public int ReportId { get; set; }
        /// <summary>
        /// 评论ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("comment_id")]
        public int CommentId { get; set; }

    }
}