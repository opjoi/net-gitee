using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.WeekReports
{
    /// <summary>
    /// 新建/编辑周报
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdWeekReportsUsernameYearWeekIndexRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 周报所属年
        /// </summary>
        [RequestType("path")]
        [JsonProperty("year")]
        public int Year { get; set; }
        /// <summary>
        /// 周报内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 周报所属周
        /// </summary>
        [RequestType("path")]
        [JsonProperty("week_index")]
        public int WeekIndex { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 周报关联PR
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pull_request_ids")]
        public object PullRequestIds { get; set; }
        /// <summary>
        /// 周报关联issue
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_ids")]
        public object IssueIds { get; set; }
        /// <summary>
        /// 关联动态
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("event_ids")]
        public object EventIds { get; set; }

    }
}