using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectBranch
{
    /// <summary>
    /// 编辑分支状态
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdBranchesNameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 分支名
        /// </summary>
        [RequestType("path")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 分支状态,常规分支:0,保护分支:1,只读分支:2
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("type")]
        public int Type { get; set; }

    }
}