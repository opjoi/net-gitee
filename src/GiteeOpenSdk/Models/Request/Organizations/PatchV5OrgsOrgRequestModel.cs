using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Organizations
{
    /// <summary>
    /// 更新授权用户所管理的组织资料
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5OrgsOrgRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 组织的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("org")]
        public string Org { get; set; }
        /// <summary>
        /// 组织公开的邮箱地址
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 组织所在地
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("location")]
        public string Location { get; set; }
        /// <summary>
        /// 组织名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 组织简介
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 组织站点
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }

    }
}