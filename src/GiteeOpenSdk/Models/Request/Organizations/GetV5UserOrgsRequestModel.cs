using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Organizations
{
    /// <summary>
    /// 列出授权用户所属的组织
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UserOrgsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 只列出授权用户管理的组织
        /// </summary>
        [RequestType("query")]
        [JsonProperty("admin")]
        public bool? Admin { get; set; }

    }
}