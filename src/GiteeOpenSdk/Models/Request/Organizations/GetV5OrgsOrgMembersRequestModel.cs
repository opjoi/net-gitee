using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Organizations
{
    /// <summary>
    /// 列出一个组织的所有成员
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5OrgsOrgMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 组织的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("org")]
        public string Org { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 根据角色筛选成员
        /// </summary>
        [RequestType("query")]
        [JsonProperty("role")]
        public string Role { get; set; }

    }
}