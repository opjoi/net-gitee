using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 删除任务附件
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdIssuesIssueIdAttachFilesAttachFileIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务的 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_id")]
        public string IssueId { get; set; }
        /// <summary>
        /// 附件 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("attach_file_id")]
        public int AttachFileId { get; set; }

    }
}