using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 更新任务
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdIssuesIssueIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_id")]
        public string IssueId { get; set; }
        /// <summary>
        /// 任务标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 负责人的 user id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee_id")]
        public int? AssigneeId { get; set; }
        /// <summary>
        /// 协作者的 user id，如有多个请用英文逗号分割。eg: 1,2,3
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("collaborator_ids")]
        public string CollaboratorIds { get; set; }
        /// <summary>
        /// 任务类型的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_type_id")]
        public int? IssueTypeId { get; set; }
        /// <summary>
        /// 任务状态的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_state_id")]
        public int? IssueStateId { get; set; }
        /// <summary>
        /// 关联项目的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 关联仓库的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project_id")]
        public int? ProjectId { get; set; }
        /// <summary>
        /// 关联里程碑的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("milestone_id")]
        public int? MilestoneId { get; set; }
        /// <summary>
        /// 关联标签的 id，如有多个请用英文逗号分割。eg: 1,2,3
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("label_ids")]
        public string LabelIds { get; set; }
        /// <summary>
        /// 优先级(0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("priority")]
        public int? Priority { get; set; }
        /// <summary>
        /// 父级任务的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }
        /// <summary>
        /// 关联分支的名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("branch")]
        public string Branch { get; set; }
        /// <summary>
        /// 预计工时。（单位：分钟）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("duration")]
        public int? Duration { get; set; }
        /// <summary>
        /// 计划开始日期。格式：yyyy-mm-ddTHH:MM:SS
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("plan_started_at")]
        public string PlanStartedAt { get; set; }
        /// <summary>
        /// 计划完成日期。格式：yyyy-mm-ddTHH:MM:SS
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("deadline")]
        public string Deadline { get; set; }
        /// <summary>
        /// 是否是私有issue, 0:否，1:是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("security_hole")]
        public int? SecurityHole { get; set; }
        /// <summary>
        /// 迭代ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("scrum_sprint_id")]
        public int? ScrumSprintId { get; set; }
        /// <summary>
        /// 版本ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("scrum_version_id")]
        public int? ScrumVersionId { get; set; }
        /// <summary>
        /// 看板ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("kanban_id")]
        public int? KanbanId { get; set; }
        /// <summary>
        /// 看板的栏ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("kanban_column_id")]
        public int? KanbanColumnId { get; set; }

    }
}