using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取任务列表-筛选器查询
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdIssuesFilterRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 筛选器id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("filter_id")]
        public int? FilterId { get; set; }
        /// <summary>
        /// 筛选类型
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("filter_conditions[property]")]
        public object FilterConditionsProperty { get; set; }
        /// <summary>
        /// 比较符
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("filter_conditions[comparator]")]
        public object FilterConditionsComparator { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("filter_conditions[value]")]
        public object FilterConditionsValue { get; set; }
        /// <summary>
        /// 自定义字段值类型
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("filter_conditions[issue_field_type]")]
        public object FilterConditionsIssueFieldType { get; set; }
        /// <summary>
        /// 自定义字段id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("filter_conditions[issue_field_id]")]
        public object FilterConditionsIssueFieldId { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("priority_filters[issue_type]")]
        public string PriorityFiltersIssueType { get; set; }
        /// <summary>
        /// 任务状态
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("priority_filters[issue_state]")]
        public string PriorityFiltersIssueState { get; set; }
        /// <summary>
        /// 任务负责人
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("priority_filters[issue_assignee]")]
        public string PriorityFiltersIssueAssignee { get; set; }
        /// <summary>
        /// 任务标签
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("priority_filters[issue_label]")]
        public string PriorityFiltersIssueLabel { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 排序字段(created_at、updated_at、deadline、priority)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}