using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取任务列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdIssuesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 企业 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public string ProgramId { get; set; }
        /// <summary>
        /// 里程碑 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("milestone_id")]
        public string MilestoneId { get; set; }
        /// <summary>
        /// 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 是否仅列出与授权用户相关的任务（0: 否 1: 是）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("only_related_me")]
        public string OnlyRelatedMe { get; set; }
        /// <summary>
        /// 负责人 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("assignee_id")]
        public string AssigneeId { get; set; }
        /// <summary>
        /// 创建者 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("author_id")]
        public string AuthorId { get; set; }
        /// <summary>
        /// 协作者。(,分隔的id字符串)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("collaborator_ids")]
        public string CollaboratorIds { get; set; }
        /// <summary>
        /// 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）&lt;yyyymmddTHH:MM:SS+08:00，（大于指定日期）&gt;yyyymmddTHH:MM:SS+08:00
        /// </summary>
        [RequestType("query")]
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        /// <summary>
        /// 任务完成日期，格式同上
        /// </summary>
        [RequestType("query")]
        [JsonProperty("finished_at")]
        public string FinishedAt { get; set; }
        /// <summary>
        /// 计划开始时间，(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("plan_started_at")]
        public string PlanStartedAt { get; set; }
        /// <summary>
        /// 任务截止日期，(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("deadline")]
        public string Deadline { get; set; }
        /// <summary>
        /// 搜索参数
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 是否过滤子任务(0: 否, 1: 是)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("filter_child")]
        public string FilterChild { get; set; }
        /// <summary>
        /// 任务状态id，多选，用逗号分隔。
        /// </summary>
        [RequestType("query")]
        [JsonProperty("issue_state_ids")]
        public string IssueStateIds { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
        [RequestType("query")]
        [JsonProperty("issue_type_id")]
        public string IssueTypeId { get; set; }
        /// <summary>
        /// 标签 id（可多选，用逗号分隔）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("label_ids")]
        public string LabelIds { get; set; }
        /// <summary>
        /// 优先级（可多选，用逗号分隔）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("priority")]
        public string Priority { get; set; }
        /// <summary>
        /// 迭代id(可多选，用逗号分隔)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scrum_sprint_ids")]
        public string ScrumSprintIds { get; set; }
        /// <summary>
        /// 版本id(可多选，用逗号分隔)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scrum_version_ids")]
        public string ScrumVersionIds { get; set; }
        /// <summary>
        /// 看板id(可多选，用逗号分隔)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("kanban_ids")]
        public string KanbanIds { get; set; }
        /// <summary>
        /// 看板栏id(可多选，用逗号分隔)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("kanban_column_ids")]
        public string KanbanColumnIds { get; set; }
        /// <summary>
        /// 排序字段(created_at、updated_at、deadline、priority)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 任务类型属性
        /// </summary>
        [RequestType("query")]
        [JsonProperty("category")]
        public string Category { get; set; }

    }
}