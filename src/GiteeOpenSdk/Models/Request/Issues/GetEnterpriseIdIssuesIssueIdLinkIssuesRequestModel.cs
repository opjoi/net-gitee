using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取任务的关联任务
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdIssuesIssueIdLinkIssuesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_id")]
        public string IssueId { get; set; }

    }
}