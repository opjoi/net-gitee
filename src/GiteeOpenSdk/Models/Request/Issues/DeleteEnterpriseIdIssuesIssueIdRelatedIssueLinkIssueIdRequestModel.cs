using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 移除任务的关联关系
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdIssuesIssueIdRelatedIssueLinkIssueIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_id")]
        public string IssueId { get; set; }
        /// <summary>
        /// 待删除的关联的任务id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("link_issue_id")]
        public int LinkIssueId { get; set; }

    }
}