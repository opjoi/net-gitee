using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取成员看板的成员列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdIssuesBoardMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 以,分隔的负责人ID字符串, 如: 23,12
        /// </summary>
        [RequestType("query")]
        [JsonProperty("assignee_ids")]
        public string AssigneeIds { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_id")]
        public int? ProjectId { get; set; }
        /// <summary>
        /// 以,分隔的组织ID字符串
        /// </summary>
        [RequestType("query")]
        [JsonProperty("group_ids")]
        public string GroupIds { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 里程碑ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("milestone_id")]
        public int? MilestoneId { get; set; }
        /// <summary>
        /// 任务类型ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("issue_type_id")]
        public int? IssueTypeId { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}