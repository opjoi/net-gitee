using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 为指定任务添加关联任务
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdIssuesIssueIdRelatedIssueRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_id")]
        public string IssueId { get; set; }
        /// <summary>
        /// 需要关联的任务 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("link_issue_id")]
        public int LinkIssueId { get; set; }
        /// <summary>
        /// 关联关系(normal, finish_to_finish, finish_to_start, start_to_start, start_to_finish)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("ref_type")]
        public string RefType { get; set; }
        /// <summary>
        /// 关联关系的方向(none, pre, latter)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 返回结果类型：包括issue, dependence
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("result_type")]
        public string ResultType { get; set; }

    }
}