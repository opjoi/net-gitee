using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取任务指派的成员列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdIssuesMemberSelectRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public string ProgramId { get; set; }
        /// <summary>
        /// 仓库 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}