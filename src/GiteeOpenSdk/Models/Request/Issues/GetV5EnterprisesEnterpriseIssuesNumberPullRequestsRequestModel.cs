using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取企业 issue 关联的 Pull Requests
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseIssuesNumberPullRequestsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// Issue 编号(区分大小写，无需添加 # 号)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public string Number { get; set; }

    }
}