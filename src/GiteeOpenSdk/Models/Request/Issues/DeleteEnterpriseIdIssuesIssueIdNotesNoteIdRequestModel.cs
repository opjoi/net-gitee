using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 删除某任务下评论
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdIssuesIssueIdNotesNoteIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_id")]
        public string IssueId { get; set; }
        /// <summary>
        /// 评论的 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("note_id")]
        public int NoteId { get; set; }

    }
}