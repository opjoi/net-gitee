using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.GitData
{
    /// <summary>
    /// 获取目录Tree
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoGitTreesShaRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 可以是分支名(如master)、Commit或者目录Tree的SHA值
        /// </summary>
        [RequestType("path")]
        [JsonProperty("sha")]
        public string Sha { get; set; }
        /// <summary>
        /// 赋值为1递归获取目录
        /// </summary>
        [RequestType("query")]
        [JsonProperty("recursive")]
        public int? Recursive { get; set; }

    }
}