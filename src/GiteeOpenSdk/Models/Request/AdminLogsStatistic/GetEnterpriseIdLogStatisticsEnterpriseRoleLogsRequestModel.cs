using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.AdminLogsStatistic
{
    /// <summary>
    /// 企业角色管理日志
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdLogStatisticsEnterpriseRoleLogsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 成员username
        /// </summary>
        [RequestType("query")]
        [JsonProperty("member")]
        public string Member { get; set; }
        /// <summary>
        /// 查询的起始时间。(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("start_date")]
        public string StartDate { get; set; }
        /// <summary>
        /// 查询的结束时间。(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("end_date")]
        public string EndDate { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}