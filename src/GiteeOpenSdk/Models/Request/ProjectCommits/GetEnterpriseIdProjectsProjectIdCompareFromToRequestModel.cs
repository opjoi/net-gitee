using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectCommits
{
    /// <summary>
    /// 对比commit
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProjectsProjectIdCompareFromToRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 起始 commit
        /// </summary>
        [RequestType("path")]
        [JsonProperty("from")]
        public string From { get; set; }
        /// <summary>
        /// 结束 commit
        /// </summary>
        [RequestType("path")]
        [JsonProperty("to")]
        public string To { get; set; }

    }
}