using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectCommits
{
    /// <summary>
    /// 获取 commit 详情中差异较大的文件内容
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProjectsProjectIdCommitCommitIdDiffForPathRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// commit sha
        /// </summary>
        [RequestType("path")]
        [JsonProperty("commit_id")]
        public string CommitId { get; set; }
        /// <summary>
        /// git 文件标识符
        /// </summary>
        [RequestType("query")]
        [JsonProperty("file_identifier")]
        public string FileIdentifier { get; set; }
        /// <summary>
        /// 旧路径
        /// </summary>
        [RequestType("query")]
        [JsonProperty("new_path")]
        public string NewPath { get; set; }
        /// <summary>
        /// 新路径
        /// </summary>
        [RequestType("query")]
        [JsonProperty("old_path")]
        public string OldPath { get; set; }

    }
}