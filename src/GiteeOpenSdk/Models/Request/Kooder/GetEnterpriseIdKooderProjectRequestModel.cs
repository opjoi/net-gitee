using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Kooder
{
    /// <summary>
    /// 仓库查询
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdKooderProjectRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 搜索内容
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 仓库语言
        /// </summary>
        [RequestType("query")]
        [JsonProperty("language")]
        public string Language { get; set; }
        /// <summary>
        /// 仓库id，用英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_ids")]
        public string ProjectIds { get; set; }
        /// <summary>
        /// 仓库命名空间id，用英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("namespace_ids")]
        public string NamespaceIds { get; set; }
        /// <summary>
        /// 仓库开源属性，公开：0，私有：1，内源：2，可多选，用英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("visibility")]
        public string Visibility { get; set; }
        /// <summary>
        /// 检索范围，null：默认，仓库名、仓库描述， human_name：仓库名称
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 触发场景，用户做分析统计
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scene")]
        public int? Scene { get; set; }

    }
}