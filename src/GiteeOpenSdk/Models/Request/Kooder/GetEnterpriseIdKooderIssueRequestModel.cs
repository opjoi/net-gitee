using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Kooder
{
    /// <summary>
    /// issue查询
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdKooderIssueRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 搜索内容
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 任务类型id，用英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("issue_type_ids")]
        public string IssueTypeIds { get; set; }
        /// <summary>
        /// 仓库id，用英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 项目id，用英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public string ProgramId { get; set; }
        /// <summary>
        /// 检索范围，null：默认，任务标题、任务描述， title：任务标题
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 触发场景，用户做分析统计
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scene")]
        public int? Scene { get; set; }

    }
}