using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Gists
{
    /// <summary>
    /// Star代码片段
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5GistsIdStarRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 代码片段的ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public string Id { get; set; }

    }
}