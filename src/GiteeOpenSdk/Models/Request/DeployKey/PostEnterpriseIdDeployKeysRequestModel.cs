using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DeployKey
{
    /// <summary>
    /// 添加部署公钥
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdDeployKeysRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 公钥
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("key")]
        public string Key { get; set; }
        /// <summary>
        /// 部署仓库id列表，用英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project_ids")]
        public string ProjectIds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}