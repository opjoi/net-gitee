using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DeployKey
{
    /// <summary>
    /// 修改部署公钥
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDeployKeysDeployKeyIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 部署公钥id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("deploy_key_id")]
        public int DeployKeyId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}