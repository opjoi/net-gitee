using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.DeployKey
{
    /// <summary>
    /// 部署公钥移除仓库
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdDeployKeysDeployKeyIdProjectsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 部署公钥id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("deploy_key_id")]
        public int DeployKeyId { get; set; }
        /// <summary>
        /// 添加仓库id，英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_ids")]
        public string ProjectIds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}