using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueTypes
{
    /// <summary>
    /// 获取任务状态的流转关系
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdIssueTypesIssueTypeIdWorkflowRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务类型 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_type_id")]
        public int IssueTypeId { get; set; }

    }
}