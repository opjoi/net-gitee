using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueTypes
{
    /// <summary>
    /// 新增任务类型
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdIssueTypesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务类型名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务状态ID(注意预期顺序排列), 逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("state_ids")]
        public string StateIds { get; set; }
        /// <summary>
        /// 状态属性：进行中。任务状态ID, 逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("progressing_ids")]
        public string ProgressingIds { get; set; }
        /// <summary>
        /// 状态属性：已完成。任务状态ID, 逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("closed_ids")]
        public string ClosedIds { get; set; }
        /// <summary>
        /// 任务类型描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 状态属性：已拒绝。任务状态ID, 逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("rejected_ids")]
        public string RejectedIds { get; set; }
        /// <summary>
        /// 任务类型模版
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("template")]
        public string Template { get; set; }
        /// <summary>
        /// 任务类型属性
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("category")]
        public string Category { get; set; }

    }
}