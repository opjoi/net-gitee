using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueTypes
{
    /// <summary>
    /// 删除任务类型
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdIssueTypesIssueTypeIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务类型 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_type_id")]
        public int IssueTypeId { get; set; }

    }
}