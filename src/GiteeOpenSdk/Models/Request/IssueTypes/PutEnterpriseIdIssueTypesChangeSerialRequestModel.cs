using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueTypes
{
    /// <summary>
    /// 更新任务类型排序
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdIssueTypesChangeSerialRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务类型ids(预期顺序), 逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("type_ids")]
        public string TypeIds { get; set; }

    }
}