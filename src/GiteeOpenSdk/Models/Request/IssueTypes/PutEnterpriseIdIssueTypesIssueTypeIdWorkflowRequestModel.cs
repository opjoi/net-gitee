using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueTypes
{
    /// <summary>
    /// 变更任务状态流转关系
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdIssueTypesIssueTypeIdWorkflowRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务类型 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_type_id")]
        public int IssueTypeId { get; set; }
        /// <summary>
        /// 当前状态 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("relations[current_state_id]")]
        public object RelationsCurrentStateId { get; set; }
        /// <summary>
        /// 下一个任务状态的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("relations[next_state_id]")]
        public object RelationsNextStateId { get; set; }

    }
}