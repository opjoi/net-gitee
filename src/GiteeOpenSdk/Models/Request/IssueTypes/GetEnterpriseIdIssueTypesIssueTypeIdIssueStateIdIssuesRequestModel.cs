using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueTypes
{
    /// <summary>
    /// 指定任务类型,任务状态下任务列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdIssueTypesIssueTypeIdIssueStateIdIssuesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务类型 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_type_id")]
        public int IssueTypeId { get; set; }
        /// <summary>
        /// 任务状态 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_state_id")]
        public int IssueStateId { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}