using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.TestPlan
{
    /// <summary>
    /// 删除测试计划
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdTestPlansTestPlanIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 项目 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("test_plan_id")]
        public int TestPlanId { get; set; }

    }
}