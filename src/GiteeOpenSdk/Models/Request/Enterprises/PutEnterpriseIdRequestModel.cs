using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 更新企业基础信息
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 企业名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 企业属性 (0: 不公开 1: 公开）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("public")]
        public string Public { get; set; }
        /// <summary>
        /// 企业邮箱
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 企业网站
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("website")]
        public string Website { get; set; }
        /// <summary>
        /// 企业简介
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("phone")]
        public string Phone { get; set; }
        /// <summary>
        /// 企业水印
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("watermark")]
        public bool? Watermark { get; set; }

    }
}