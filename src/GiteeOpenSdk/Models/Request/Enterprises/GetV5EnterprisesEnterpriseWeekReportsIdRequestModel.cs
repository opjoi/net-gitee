using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 周报详情
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseWeekReportsIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 周报ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public int Id { get; set; }

    }
}