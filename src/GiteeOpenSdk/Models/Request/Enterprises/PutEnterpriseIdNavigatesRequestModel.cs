using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 更新企业导航栏设置
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdNavigatesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 导航卡id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("navigates[id]")]
        public object NavigatesId { get; set; }
        /// <summary>
        /// 导航卡icon
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("navigates[icon]")]
        public object NavigatesIcon { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("navigates[active]")]
        public object NavigatesActive { get; set; }

    }
}