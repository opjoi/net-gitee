using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 获取企业的一个成员
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseMembersUsernameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }

    }
}