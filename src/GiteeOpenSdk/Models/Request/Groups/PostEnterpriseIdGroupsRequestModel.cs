using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Groups
{
    /// <summary>
    /// 新建企业团队
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdGroupsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 团队 path
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 类型, 0:内部，1:公开，2:外包
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("public")]
        public int Public { get; set; }
        /// <summary>
        /// 负责人 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("owner_id")]
        public int OwnerId { get; set; }
        /// <summary>
        /// 成员ids, 逗号隔开：1,2,3
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("user_ids")]
        public string UserIds { get; set; }

    }
}