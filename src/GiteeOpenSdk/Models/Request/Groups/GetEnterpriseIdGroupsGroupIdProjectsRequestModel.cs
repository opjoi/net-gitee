using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Groups
{
    /// <summary>
    /// 企业团队下仓库列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdGroupsGroupIdProjectsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 团队id/path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("group_id")]
        public string GroupId { get; set; }
        /// <summary>
        /// 范围筛选
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 搜索参数
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库
        /// </summary>
        [RequestType("query")]
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 状态: 0: 开始，1: 暂停，2: 关闭 
        /// </summary>
        [RequestType("query")]
        [JsonProperty("status")]
        public int? Status { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [RequestType("query")]
        [JsonProperty("creator_id")]
        public int? CreatorId { get; set; }
        /// <summary>
        /// form_from仓库id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }
        /// <summary>
        /// 非fork的(not_fork), 只看fork的(only_fork)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("fork_filter")]
        public string ForkFilter { get; set; }
        /// <summary>
        /// 是否外包：0：内部，1：外包
        /// </summary>
        [RequestType("query")]
        [JsonProperty("outsourced")]
        public int? Outsourced { get; set; }
        /// <summary>
        /// 排序字段(created_at: 创建时间 last_push_at: 最近push时间)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}