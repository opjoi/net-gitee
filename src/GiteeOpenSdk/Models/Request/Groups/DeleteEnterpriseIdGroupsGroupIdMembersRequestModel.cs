using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Groups
{
    /// <summary>
    /// 移除团队成员
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdGroupsGroupIdMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 团队 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("group_id")]
        public int GroupId { get; set; }
        /// <summary>
        /// 成员 id，多个成员id通过英文逗号分隔
        /// </summary>
        [RequestType("query")]
        [JsonProperty("user_ids")]
        public string UserIds { get; set; }

    }
}