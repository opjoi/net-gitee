using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectAdmin
{
    /// <summary>
    /// 添加部署公钥
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsProjectIdDeployKeysRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 公钥
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("key")]
        public string Key { get; set; }

    }
}