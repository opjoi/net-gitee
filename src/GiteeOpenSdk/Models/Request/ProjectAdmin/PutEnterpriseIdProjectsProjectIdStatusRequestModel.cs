using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectAdmin
{
    /// <summary>
    /// 仓库归档(状态管理)
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdStatusRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 仓库状态，0：开始，1：暂停，2：关闭
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("status")]
        public int Status { get; set; }
        /// <summary>
        /// 短信验证码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("sms_captcha")]
        public string SmsCaptcha { get; set; }
        /// <summary>
        /// 用户密码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("password")]
        public string Password { get; set; }
        /// <summary>
        /// 验证方式
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("validate_type")]
        public string ValidateType { get; set; }

    }
}