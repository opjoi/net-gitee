using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectAdmin
{
    /// <summary>
    /// 指派审查、测试人员
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdPrAssignerRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 审查人员id，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assigner_ids")]
        public string AssignerIds { get; set; }
        /// <summary>
        /// 可合并的审查人员门槛数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pr_assign_num")]
        public int? PrAssignNum { get; set; }
        /// <summary>
        /// 测试人员id，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("tester_ids")]
        public string TesterIds { get; set; }
        /// <summary>
        /// 可合并的测试人员门槛数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pr_test_num")]
        public int? PrTestNum { get; set; }

    }
}