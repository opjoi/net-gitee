using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectAdmin
{
    /// <summary>
    /// 启用/停用公钥
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdDeployKeysDeployKeyIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 公钥id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("deploy_key_id")]
        public int DeployKeyId { get; set; }
        /// <summary>
        /// 0:停用，1：启用
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("enabled")]
        public int Enabled { get; set; }

    }
}