using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 添加企业成员或团队进项目
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProgramsProgramIdMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 成员ids，英文逗号(,)隔开: 1,2
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("user_ids")]
        public string UserIds { get; set; }
        /// <summary>
        /// 团队ids，英文逗号(,)隔开: 1,2
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("group_ids")]
        public string GroupIds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}