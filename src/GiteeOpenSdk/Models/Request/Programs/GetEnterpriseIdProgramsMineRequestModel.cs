using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 获取用户最近浏览的项目集合
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProgramsMineRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目的 id 列表，如有多个用逗号分割。eg: 1,2,3
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_ids")]
        public string ProgramIds { get; set; }
        /// <summary>
        /// 项目状态（0:开始 1:暂停 2:关闭）, 逗号分隔,如: 0,1
        /// </summary>
        [RequestType("query")]
        [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}