using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 更新项目
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProgramsProgramIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 项目简介
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 负责人ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee_id")]
        public string AssigneeId { get; set; }
        /// <summary>
        /// 项目类型:内部(false)/外包(true)项目
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("outsourced")]
        public bool? Outsourced { get; set; }
        /// <summary>
        /// 项目状态:（0:开始 1:暂停 2:关闭）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("status")]
        public int? Status { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("color")]
        public string Color { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}