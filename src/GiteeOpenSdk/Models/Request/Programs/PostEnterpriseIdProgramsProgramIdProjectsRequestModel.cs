using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 项目关联仓库
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProgramsProgramIdProjectsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 仓库ids
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("add_project_ids")]
        public string AddProjectIds { get; set; }
        /// <summary>
        /// 是否导入仓库成员
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_project_users")]
        public int? ImportProjectUsers { get; set; }
        /// <summary>
        /// 是否导入仓库任务
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_project_issues")]
        public int? ImportProjectIssues { get; set; }
        /// <summary>
        /// 是否导入仓库里程碑
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_project_milestones")]
        public int? ImportProjectMilestones { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}