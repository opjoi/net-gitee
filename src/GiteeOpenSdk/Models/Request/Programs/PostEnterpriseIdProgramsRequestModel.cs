using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 新建项目
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProgramsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 项目简介
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 负责人ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee_id")]
        public string AssigneeId { get; set; }
        /// <summary>
        /// 项目类型:内部(false)/外包(true)项目
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("outsourced")]
        public bool? Outsourced { get; set; }
        /// <summary>
        /// 项目状态:（0:开始 1:暂停 2:关闭）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("status")]
        public int? Status { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("color")]
        public string Color { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 项目类型(standard、scrum、kanban)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("category")]
        public string Category { get; set; }
        /// <summary>
        /// 关联仓库ids，逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project_ids")]
        public string ProjectIds { get; set; }
        /// <summary>
        /// 是否导入仓库成员
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_project_users")]
        public bool? ImportProjectUsers { get; set; }
        /// <summary>
        /// 是否导入仓库任务
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_project_issues")]
        public bool? ImportProjectIssues { get; set; }
        /// <summary>
        /// 是否导入仓库里程碑
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_project_milestones")]
        public bool? ImportProjectMilestones { get; set; }
        /// <summary>
        /// 成员ids，逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("user_ids")]
        public string UserIds { get; set; }
        /// <summary>
        /// 团队ids，逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("group_ids")]
        public string GroupIds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}