using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 删除项目
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdProgramsProgramIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 是否删除里程碑
        /// </summary>
        [RequestType("query")]
        [JsonProperty("delete_milestones")]
        public bool? DeleteMilestones { get; set; }
        /// <summary>
        /// 是否删除任务
        /// </summary>
        [RequestType("query")]
        [JsonProperty("delete_issues")]
        public bool? DeleteIssues { get; set; }
        /// <summary>
        /// 短信验证码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sms_captcha")]
        public string SmsCaptcha { get; set; }
        /// <summary>
        /// 用户密码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("password")]
        public string Password { get; set; }
        /// <summary>
        /// 验证方式
        /// </summary>
        [RequestType("query")]
        [JsonProperty("validate_type")]
        public string ValidateType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}