using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 移出项目下的团队
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdProgramsProgramIdGroupsGroupIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 团队 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("group_id")]
        public int GroupId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}