using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 获取项目下动态列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProgramsProgramIdEventsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 查询的起始时间。(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("start_date")]
        public string StartDate { get; set; }
        /// <summary>
        /// 查询的结束时间。(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("end_date")]
        public string EndDate { get; set; }
        /// <summary>
        /// 项目范围：所有，仓库，任务，外部，我的
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 上一次动态列表中最小动态 ID (返回列表不包含该ID记录)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("prev_id")]
        public int? PrevId { get; set; }
        /// <summary>
        /// 每次获取动态的条数
        /// </summary>
        [RequestType("query")]
        [JsonProperty("limit")]
        public int? Limit { get; set; }

    }
}