using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 获取项目下的仓库列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProgramsProgramIdProjectsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 范围筛选
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 搜索参数
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库
        /// </summary>
        [RequestType("query")]
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 状态: 0: 开始，1: 暂停，2: 关闭 
        /// </summary>
        [RequestType("query")]
        [JsonProperty("status")]
        public int? Status { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [RequestType("query")]
        [JsonProperty("creator_id")]
        public int? CreatorId { get; set; }
        /// <summary>
        /// form_from仓库id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }
        /// <summary>
        /// 非fork的(not_fork), 只看fork的(only_fork)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("fork_filter")]
        public string ForkFilter { get; set; }
        /// <summary>
        /// 是否外包：0：内部，1：外包
        /// </summary>
        [RequestType("query")]
        [JsonProperty("outsourced")]
        public int? Outsourced { get; set; }
        /// <summary>
        /// 团队id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("group_id")]
        public int? GroupId { get; set; }
        /// <summary>
        /// 排序字段(created_at、last_push_at)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}