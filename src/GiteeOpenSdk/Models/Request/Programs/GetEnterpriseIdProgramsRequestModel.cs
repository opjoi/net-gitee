using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Programs
{
    /// <summary>
    /// 获取项目列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProgramsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 筛选不同类型的项目列表。我参与的: joined; 我负责的: assigned; 我创建的: created; 我星标的: only_star
        /// </summary>
        [RequestType("query")]
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 排序字段(created_at: 创建时间 updated_at: 更新时间 users_count: 成员数 projects_count: 仓库数 issues_count: 任务数 accessed_at: 访问时间 name: 项目名称)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 是否按照置顶优先排序
        /// </summary>
        [RequestType("query")]
        [JsonProperty("priority_topped")]
        public bool? PriorityTopped { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 项目状态（0:开始 1:暂停 2:关闭）, 逗号分隔,如: 0,1
        /// </summary>
        [RequestType("query")]
        [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// 项目类别（all: 所有，common: 普通，kanban: 看板）, 支持多种类型，以,分隔，如：common,kanban
        /// </summary>
        [RequestType("query")]
        [JsonProperty("category")]
        public string Category { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}