using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Git_Data
{
    /// <summary>
    /// 新建仓库
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[name]")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 归属路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[namespace_path]")]
        public string ProjectNamespacePath { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[path]")]
        public string ProjectPath { get; set; }
        /// <summary>
        /// 模板仓库 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[template_id]")]
        public int? ProjectTemplateId { get; set; }
        /// <summary>
        /// 仓库介绍
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[description]")]
        public string ProjectDescription { get; set; }
        /// <summary>
        /// 是否开源
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[public]")]
        public int? ProjectPublic { get; set; }
        /// <summary>
        /// 类型，0：内部，1：外包
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[outsourced]")]
        public int? ProjectOutsourced { get; set; }
        /// <summary>
        /// 关联项目列表，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[program_ids]")]
        public string ProjectProgramIds { get; set; }
        /// <summary>
        /// 关联成员列表，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[member_ids]")]
        public string ProjectMemberIds { get; set; }
        /// <summary>
        /// 授权团队列表，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[group_ids]")]
        public string ProjectGroupIds { get; set; }
        /// <summary>
        /// 导入已有仓库路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project[import_url]")]
        public string ProjectImportUrl { get; set; }
        /// <summary>
        /// 是否包含所有分支
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("normal_refs")]
        public bool? NormalRefs { get; set; }
        /// <summary>
        /// 是否导入项目成员: 0:否，1:是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_program_users")]
        public int? ImportProgramUsers { get; set; }
        /// <summary>
        /// 是否初始化readme: 0:否，1:是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("readme")]
        public int? Readme { get; set; }
        /// <summary>
        /// 是否初始化issue模版: 0:否，1:是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_template")]
        public int? IssueTemplate { get; set; }
        /// <summary>
        /// 是否初始化PR模版: 0:否，1:是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pull_request_template")]
        public int? PullRequestTemplate { get; set; }
        /// <summary>
        /// 仓库导入-账号
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("user_sync_code")]
        public string UserSyncCode { get; set; }
        /// <summary>
        /// 仓库导入-密码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("password_sync_code")]
        public string PasswordSyncCode { get; set; }
        /// <summary>
        /// 语言id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("language")]
        public int? Language { get; set; }
        /// <summary>
        /// .gitignore
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("ignore")]
        public string Ignore { get; set; }
        /// <summary>
        /// 开源许可证
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("license")]
        public string License { get; set; }
        /// <summary>
        /// 分支模型id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("model")]
        public string Model { get; set; }
        /// <summary>
        /// Production environment branch
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("custom_branches[prod]")]
        public string CustomBranchesProd { get; set; }
        /// <summary>
        /// Development branch
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("custom_branches[dev]")]
        public string CustomBranchesDev { get; set; }
        /// <summary>
        /// Function branch
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("custom_branches[feat]")]
        public string CustomBranchesFeat { get; set; }
        /// <summary>
        /// Release branch
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("custom_branches[rel]")]
        public string CustomBranchesRel { get; set; }
        /// <summary>
        /// Patch branch
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("custom_branches[bugfix]")]
        public string CustomBranchesBugfix { get; set; }
        /// <summary>
        /// Version tag branch
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("custom_branches[tag]")]
        public string CustomBranchesTag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}