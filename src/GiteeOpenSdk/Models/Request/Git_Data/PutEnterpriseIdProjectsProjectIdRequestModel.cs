using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Git_Data
{
    /// <summary>
    /// 更新仓库设置
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 仓库介绍
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 主页
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("homepage")]
        public string Homepage { get; set; }
        /// <summary>
        /// 语言
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("lang_id")]
        public int? LangId { get; set; }
        /// <summary>
        /// 默认分支
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("default_branch")]
        public string DefaultBranch { get; set; }
        /// <summary>
        /// 类型，0：内部，1：外包
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("outsourced")]
        public int? Outsourced { get; set; }
        /// <summary>
        /// 仓库负责人
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("creator_id")]
        public int? CreatorId { get; set; }
        /// <summary>
        /// 允许用户对仓库进行评论
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("can_comment")]
        public int? CanComment { get; set; }
        /// <summary>
        /// 允许用户对&quot;关闭&quot;状态的Issues进行评论
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issue_comment")]
        public int? IssueComment { get; set; }
        /// <summary>
        /// 轻量级的issue跟踪系统
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issues_enabled")]
        public int? IssuesEnabled { get; set; }
        /// <summary>
        /// 允许用户创建涉及敏感信息的Issue，提交后不公开此Issue（可见范围：仓库成员、企业成员）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("security_hole_enabled")]
        public int? SecurityHoleEnabled { get; set; }
        /// <summary>
        /// 是否允许仓库被Fork
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("fork_enabled")]
        public int? ForkEnabled { get; set; }
        /// <summary>
        /// 是否允许仓库文件在线编辑
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("online_edit_enabled")]
        public int? OnlineEditEnabled { get; set; }
        /// <summary>
        /// 接受pull request，协作开发
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pull_requests_enabled")]
        public int? PullRequestsEnabled { get; set; }
        /// <summary>
        /// 可以编写文档
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("wiki_enabled")]
        public int? WikiEnabled { get; set; }
        /// <summary>
        /// 接受轻量级 Pull Request
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("lightweight_pr_enabled")]
        public int? LightweightPrEnabled { get; set; }
        /// <summary>
        /// 开启的Pull Request，仅管理员、审查者、测试者可见
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pr_master_only")]
        public int? PrMasterOnly { get; set; }
        /// <summary>
        /// 禁止强制推送
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("forbid_force_push")]
        public int? ForbidForcePush { get; set; }
        /// <summary>
        /// 仓库远程地址
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_url")]
        public string ImportUrl { get; set; }
        /// <summary>
        /// 禁止仓库同步
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("forbid_force_sync")]
        public int? ForbidForceSync { get; set; }
        /// <summary>
        /// 使用SVN管理您的仓库
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("svn_enabled")]
        public int? SvnEnabled { get; set; }
        /// <summary>
        /// 开启文件/文件夹只读功能
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("can_readonly")]
        public int? CanReadonly { get; set; }
        /// <summary>
        /// 待添加的关联项目列表，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program_add_ids")]
        public string ProgramAddIds { get; set; }
        /// <summary>
        /// 待移除的关联项目列表，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program_remove_ids")]
        public string ProgramRemoveIds { get; set; }
        /// <summary>
        /// GiteeGo 启停状态, 1表示启用，0表示停用
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("gitee_go_enabled")]
        public int? GiteeGoEnabled { get; set; }
        /// <summary>
        /// 仓库模板启停状态
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("template_enabled")]
        public bool? TemplateEnabled { get; set; }

    }
}