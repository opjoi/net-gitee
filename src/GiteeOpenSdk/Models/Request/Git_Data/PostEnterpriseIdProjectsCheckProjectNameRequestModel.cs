using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Git_Data
{
    /// <summary>
    /// 新建仓库-仓库名/路径是否已经存在
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsCheckProjectNameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 名称。二选一检验某一项
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 路径。二选一检验某一项
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 归属路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("namespace_path")]
        public string NamespacePath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}