using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Git_Data
{
    /// <summary>
    /// 新建仓库-导入仓库参数是否有效
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsCheckProjectCanImportRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 待导入仓库克隆路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("import_url")]
        public string ImportUrl { get; set; }
        /// <summary>
        /// 仓库导入-账号（私有仓库需要）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("user_sync_code")]
        public string UserSyncCode { get; set; }
        /// <summary>
        /// 仓库导入-密码（私有仓库需要）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("password_sync_code")]
        public string PasswordSyncCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}