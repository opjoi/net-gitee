using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Git_Data
{
    /// <summary>
    /// 新建发行版
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsProjectIdReleasesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 发行版版本
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("release[tag_version]")]
        public string ReleaseTagVersion { get; set; }
        /// <summary>
        /// 发行版标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("release[title]")]
        public string ReleaseTitle { get; set; }
        /// <summary>
        /// 发行版所属分支
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("release[ref]")]
        public string ReleaseRef { get; set; }
        /// <summary>
        /// 发行版描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("release[description]")]
        public string ReleaseDescription { get; set; }
        /// <summary>
        /// 发行版类型, 0：发行版、1：预发行版
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("release[release_type]")]
        public string ReleaseReleaseType { get; set; }
        /// <summary>
        /// 附件id列表，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("attach_ids")]
        public string AttachIds { get; set; }

    }
}