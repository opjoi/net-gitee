using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectEnv
{
    /// <summary>
    /// 修改环境变量
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdEnvVariablesEnvVariableIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 变量id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("env_variable_id")]
        public int EnvVariableId { get; set; }
        /// <summary>
        /// 变量名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 变量值
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("value")]
        public string Value { get; set; }
        /// <summary>
        /// 是否只读,1为true,0为false
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("read_only")]
        public int? ReadOnly { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("remark")]
        public string Remark { get; set; }

    }
}