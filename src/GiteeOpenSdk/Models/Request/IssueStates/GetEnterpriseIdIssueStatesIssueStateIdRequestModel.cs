using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueStates
{
    /// <summary>
    /// 任务状态详情
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdIssueStatesIssueStateIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务状态 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_state_id")]
        public int IssueStateId { get; set; }

    }
}