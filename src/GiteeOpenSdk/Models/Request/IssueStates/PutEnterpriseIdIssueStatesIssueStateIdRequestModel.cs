using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.IssueStates
{
    /// <summary>
    /// 更新任务状态
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdIssueStatesIssueStateIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 任务状态名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务状态图标
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("icon")]
        public string Icon { get; set; }
        /// <summary>
        /// 任务状态指令
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("command")]
        public string Command { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("issue_state_id")]
        public int IssueStateId { get; set; }

    }
}