using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Labels
{
    /// <summary>
    /// 更新一个仓库任务标签
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5ReposOwnerRepoLabelsOriginalNameRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 标签原有名称
        /// </summary>
        [RequestType("path")]
        [JsonProperty("original_name")]
        public string OriginalName { get; set; }
        /// <summary>
        /// 标签新名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 标签新颜色
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("color")]
        public string Color { get; set; }

    }
}