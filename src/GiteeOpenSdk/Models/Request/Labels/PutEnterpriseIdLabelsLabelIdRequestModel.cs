using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Labels
{
    /// <summary>
    /// 更新标签
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdLabelsLabelIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 标签 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("label_id")]
        public int LabelId { get; set; }
        /// <summary>
        /// 标签名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 标签颜色
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("color")]
        public string Color { get; set; }

    }
}