using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Labels
{
    /// <summary>
    /// 更新标签排序
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdLabelsChangeSerialRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 标签id(预期顺序), 逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("label_ids")]
        public string LabelIds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}