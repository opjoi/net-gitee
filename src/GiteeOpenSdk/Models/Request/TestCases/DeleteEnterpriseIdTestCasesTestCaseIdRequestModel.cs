using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.TestCases
{
    /// <summary>
    /// 删除测试用例
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdTestCasesTestCaseIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 项目 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int ProgramId { get; set; }
        /// <summary>
        /// 测试用例 ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("test_case_id")]
        public string TestCaseId { get; set; }
        /// <summary>
        /// 查询方式(ident/id)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }

    }
}