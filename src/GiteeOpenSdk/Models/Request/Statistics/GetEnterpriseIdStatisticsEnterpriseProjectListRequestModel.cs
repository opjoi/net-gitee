using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Statistics
{
    /// <summary>
    /// 获取企业仓库的统计信息
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdStatisticsEnterpriseProjectListRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 仓库id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_id")]
        public int? ProjectId { get; set; }
        /// <summary>
        /// 查询的起始时间。(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("start_date")]
        public string StartDate { get; set; }
        /// <summary>
        /// 查询的结束时间。(格式：yyyy-mm-dd)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("end_date")]
        public string EndDate { get; set; }

    }
}