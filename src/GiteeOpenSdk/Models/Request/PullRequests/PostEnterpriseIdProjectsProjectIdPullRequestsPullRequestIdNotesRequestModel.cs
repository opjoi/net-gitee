using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 评论 Pull Request
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdNotesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 默认PR id，当pr_qt参数为iid时，此处是仓库的 PR 编号
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }
        /// <summary>
        /// 评论内容，代码建议用```suggestion ``` 包围
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 代码行标记值
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("line_code")]
        public string LineCode { get; set; }
        /// <summary>
        /// 代码评论组id，同位置已有评论时传递
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("diff_position_id")]
        public int? DiffPositionId { get; set; }
        /// <summary>
        /// 回复的上级评论id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("reply_id")]
        public int? ReplyId { get; set; }

    }
}