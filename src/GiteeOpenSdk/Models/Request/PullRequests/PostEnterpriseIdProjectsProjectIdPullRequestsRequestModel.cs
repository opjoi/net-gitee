using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 创建 PR
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsProjectIdPullRequestsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 源仓库 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("source_repo")]
        public string SourceRepo { get; set; }
        /// <summary>
        /// 源分支名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("source_branch")]
        public string SourceBranch { get; set; }
        /// <summary>
        /// 目标分支名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("target_branch")]
        public string TargetBranch { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 审查者。可多选，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee_id")]
        public string AssigneeId { get; set; }
        /// <summary>
        /// 审查人员数量
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pr_assign_num")]
        public string PrAssignNum { get; set; }
        /// <summary>
        /// 测试人员。可多选，英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("tester_id")]
        public string TesterId { get; set; }
        /// <summary>
        /// 测试人员数量
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pr_test_num")]
        public string PrTestNum { get; set; }
        /// <summary>
        /// 关联的里程碑
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("milestone_id")]
        public string MilestoneId { get; set; }
        /// <summary>
        /// 优先级 0~4
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("priority")]
        public string Priority { get; set; }
        /// <summary>
        /// 是否需要在合并 PR 后删除提交分支. 0: 否 1: 是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("prune_branch")]
        public int PruneBranch { get; set; }
        /// <summary>
        /// 是否需要在合并 PR 后关闭关联的任务. 0: 否 1: 是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("close_related_issue")]
        public string CloseRelatedIssue { get; set; }
        /// <summary>
        /// 是否指定为草稿： 草稿 - true, 非草稿 - false，缺省时为非草稿
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("draft")]
        public bool? Draft { get; set; }

    }
}