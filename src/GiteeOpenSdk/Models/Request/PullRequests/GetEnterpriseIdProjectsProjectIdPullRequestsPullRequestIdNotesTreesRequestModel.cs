using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 获取 Pull Request或相关提交的评论列表树
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdNotesTreesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 默认PR id，当pr_qt参数为iid时，此处是仓库的 PR 编号
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }
        /// <summary>
        /// head sha
        /// </summary>
        [RequestType("query")]
        [JsonProperty("head_sha")]
        public string HeadSha { get; set; }
        /// <summary>
        /// base sha
        /// </summary>
        [RequestType("query")]
        [JsonProperty("base_sha")]
        public string BaseSha { get; set; }

    }
}