using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 获取分支对比的文件改动列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProjectsProjectIdPullRequestsNewFilesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 源仓库所属空间地址
        /// </summary>
        [RequestType("query")]
        [JsonProperty("source_namespace")]
        public string SourceNamespace { get; set; }
        /// <summary>
        /// 源仓库的分支名称
        /// </summary>
        [RequestType("query")]
        [JsonProperty("source_branch")]
        public string SourceBranch { get; set; }
        /// <summary>
        /// 目标仓库所属空间地址
        /// </summary>
        [RequestType("query")]
        [JsonProperty("target_namespace")]
        public string TargetNamespace { get; set; }
        /// <summary>
        /// 目标仓库的分支名称
        /// </summary>
        [RequestType("query")]
        [JsonProperty("target_branch")]
        public string TargetBranch { get; set; }
        /// <summary>
        /// 是否隐藏空白和换行, 1：隐藏，0：不隐藏，默认不隐藏
        /// </summary>
        [RequestType("query")]
        [JsonProperty("w")]
        public int? W { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}