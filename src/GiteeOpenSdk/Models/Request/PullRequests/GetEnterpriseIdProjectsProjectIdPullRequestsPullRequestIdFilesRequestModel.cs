using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// Pull Request Commit 文件列表。最多显示100条diff
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdFilesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// PR id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// head sha
        /// </summary>
        [RequestType("query")]
        [JsonProperty("head_sha")]
        public string HeadSha { get; set; }
        /// <summary>
        /// base sha
        /// </summary>
        [RequestType("query")]
        [JsonProperty("base_sha")]
        public string BaseSha { get; set; }
        /// <summary>
        /// 是否隐藏空白和换行, 1：隐藏，0：不隐藏，默认不隐藏
        /// </summary>
        [RequestType("query")]
        [JsonProperty("w")]
        public int? W { get; set; }

    }
}