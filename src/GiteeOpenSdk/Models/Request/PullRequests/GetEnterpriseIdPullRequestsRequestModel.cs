using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 获取企业下的 Pull Request 列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdPullRequestsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// PR 状态
        /// </summary>
        [RequestType("query")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 范围筛选。指派我的: assigned_or_test，我创建或指派给我的: related_to_me，我参与仓库的PR: participate_in，草稿PR: draft
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 筛选 PR 创建者
        /// </summary>
        [RequestType("query")]
        [JsonProperty("author_id")]
        public string AuthorId { get; set; }
        /// <summary>
        /// 筛选 PR 审查者
        /// </summary>
        [RequestType("query")]
        [JsonProperty("assignee_id")]
        public string AssigneeId { get; set; }
        /// <summary>
        /// 筛选 PR 测试人员
        /// </summary>
        [RequestType("query")]
        [JsonProperty("tester_id")]
        public string TesterId { get; set; }
        /// <summary>
        /// 搜索参数
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 排序字段(created_at、closed_at、priority)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 团队 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("group_id")]
        public int? GroupId { get; set; }
        /// <summary>
        /// 里程碑 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("milestone_id")]
        public int? MilestoneId { get; set; }
        /// <summary>
        /// 标签名称。多个标签逗号(,)隔开
        /// </summary>
        [RequestType("query")]
        [JsonProperty("labels")]
        public string Labels { get; set; }
        /// <summary>
        /// 标签ID,多个标签逗号(,)隔开
        /// </summary>
        [RequestType("query")]
        [JsonProperty("label_ids")]
        public string LabelIds { get; set; }
        /// <summary>
        /// 是否可合并
        /// </summary>
        [RequestType("query")]
        [JsonProperty("can_be_merged")]
        public int? CanBeMerged { get; set; }
        /// <summary>
        /// 仓库 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_id")]
        public int? ProjectId { get; set; }
        /// <summary>
        /// 是否需要状态统计数
        /// </summary>
        [RequestType("query")]
        [JsonProperty("need_state_count")]
        public int? NeedStateCount { get; set; }
        /// <summary>
        /// 仅列出内部公开和外部公开的 PR
        /// </summary>
        [RequestType("query")]
        [JsonProperty("public_or_internal_open_only")]
        public int? PublicOrInternalOpenOnly { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}