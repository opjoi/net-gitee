using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// PR 审查通过
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdAssignRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// PR id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }
        /// <summary>
        /// 是否强制审核通过(默认否)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("force_accept")]
        public int? ForceAccept { get; set; }

    }
}