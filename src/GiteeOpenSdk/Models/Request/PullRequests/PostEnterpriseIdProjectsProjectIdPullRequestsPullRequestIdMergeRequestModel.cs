using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 合并 PR
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdMergeRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// PR id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }
        /// <summary>
        /// 可选。合并PR的方法，merge（合并所有提交）和 squash（扁平化分支合并）和 rebase（变基并合并）。默认为merge。
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("merge_method")]
        public string MergeMethod { get; set; }
        /// <summary>
        /// 可选。合并标题，默认为PR的标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 可选。合并描述，默认为 &quot;Merge pull request !{pr_id} from {author}/{source_branch}&quot;，与页面显示的默认一致。
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }

    }
}