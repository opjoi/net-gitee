using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 获取 PullRequest 的 GiteeScan 报告
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdScanReportsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// PR id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }
        /// <summary>
        /// GiteeScan 扫描任务 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("task_id")]
        public int TaskId { get; set; }
        /// <summary>
        /// 扫描类型，bug：缺陷扫描、style：规范扫描、cve：依赖项漏洞扫描
        /// </summary>
        [RequestType("query")]
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 缺陷等级，高危：1、中危：2
        /// </summary>
        [RequestType("query")]
        [JsonProperty("bug_level")]
        public string BugLevel { get; set; }
        /// <summary>
        /// 缺陷类型，BUG：0、CodeSmell：1、Security：2
        /// </summary>
        [RequestType("query")]
        [JsonProperty("bug_type")]
        public string BugType { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}