using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 更新 Pull Request
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// PR id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }
        /// <summary>
        /// 关联的里程碑 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("milestone_id")]
        public int? MilestoneId { get; set; }
        /// <summary>
        /// 关联的任务 id。如有多个，用英文逗号分隔。eg: 1,2,3
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("related_issue_id")]
        public string RelatedIssueId { get; set; }
        /// <summary>
        /// PR 标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// PR 正文内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 优先级(0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("priority")]
        public int? Priority { get; set; }
        /// <summary>
        /// 关闭 PR
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("state_event")]
        public string StateEvent { get; set; }
        /// <summary>
        /// 关联的标签 id。如有多个，用英文逗号分隔。eg: 1,2,3
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("label_ids")]
        public string LabelIds { get; set; }
        /// <summary>
        /// 是否需要在合并 PR 后关闭关联的任务. 0: 否 1: 是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("close_related_issue")]
        public int? CloseRelatedIssue { get; set; }
        /// <summary>
        /// 是否需要在合并 PR 后删除提交分支. 0: 否 1: 是
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("prune_branch")]
        public int? PruneBranch { get; set; }
        /// <summary>
        /// 审查人员的用户 id。如有多个，用英文逗号分隔。eg: 1,2,3
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee_id")]
        public string AssigneeId { get; set; }
        /// <summary>
        /// 至少需要{pr_assign_num}名审查人员审查通过后可合并
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pr_assign_num")]
        public int? PrAssignNum { get; set; }
        /// <summary>
        /// 测试人员的用户 id。如有多个，用英文逗号分隔。eg: 1,2,3
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("tester_id")]
        public string TesterId { get; set; }
        /// <summary>
        /// 至少需要{pr_assign_num}名测试人员测试通过后可合并
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("pr_test_num")]
        public int? PrTestNum { get; set; }
        /// <summary>
        /// 审查状态. 0: 非必须审查 1: 必须审查
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("check_state")]
        public int? CheckState { get; set; }
        /// <summary>
        /// 测试状态. 0: 非必须测试 1: 必须测试
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("test_state")]
        public int? TestState { get; set; }
        /// <summary>
        /// 是否指定为草稿： 草稿 - true, 非草稿 - false，缺省时为非草稿
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("draft")]
        public bool? Draft { get; set; }

    }
}