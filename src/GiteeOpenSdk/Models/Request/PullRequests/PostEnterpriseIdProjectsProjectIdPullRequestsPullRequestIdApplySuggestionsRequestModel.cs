using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 应用代码建议到PR
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsProjectIdPullRequestsPullRequestIdApplySuggestionsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 应用建议操作的提交信息
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("message")]
        public string Message { get; set; }
        /// <summary>
        /// Note的id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("suggestions[note_id]")]
        public object SuggestionsNoteId { get; set; }
        /// <summary>
        /// Note的updated_at的秒时间戳
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("suggestions[note_timestamp]")]
        public object SuggestionsNoteTimestamp { get; set; }
        /// <summary>
        /// Diff_position的updated_at的秒时间戳
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("suggestions[diff_position_timestamp]")]
        public object SuggestionsDiffPositionTimestamp { get; set; }
        /// <summary>
        /// 代码建议的内容（如果没有，则视为删除）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("suggestions[content]")]
        public object SuggestionsContent { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("pull_request_id")]
        public int PullRequestId { get; set; }

    }
}