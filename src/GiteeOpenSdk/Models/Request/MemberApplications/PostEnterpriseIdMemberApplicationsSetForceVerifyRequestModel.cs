using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.MemberApplications
{
    /// <summary>
    /// 强制审核开关
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdMemberApplicationsSetForceVerifyRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 是否开启强制审核: 开启此选项后，所有邀请(包括之前已生成的邀请)都将需要管理员审核通过后才可加入企业
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("on")]
        public bool On { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}