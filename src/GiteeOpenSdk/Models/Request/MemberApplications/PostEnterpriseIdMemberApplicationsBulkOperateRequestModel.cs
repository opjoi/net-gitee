using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.MemberApplications
{
    /// <summary>
    /// 批量处理申请记录
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdMemberApplicationsBulkOperateRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 申请记录id, 逗号隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("selected_ids")]
        public string SelectedIds { get; set; }
        /// <summary>
        /// 角色id。企业角色列表 scope: can_invite
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("role_id")]
        public int? RoleId { get; set; }
        /// <summary>
        /// 通过/拒绝
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}