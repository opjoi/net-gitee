using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.MemberApplications
{
    /// <summary>
    /// 企业/仓库/团队的成员加入申请列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdMemberApplicationsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 申请加入的类型: Enterprise,Project,Group
        /// </summary>
        [RequestType("query")]
        [JsonProperty("target_type")]
        public string TargetType { get; set; }
        /// <summary>
        /// 申请加入的主体 ID
        /// </summary>
        [RequestType("query")]
        [JsonProperty("target_id")]
        public int TargetId { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}