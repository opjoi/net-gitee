using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 把成员移出仓库
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdMembersUserIdProjectsProjectIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 成员 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("user_id")]
        public int UserId { get; set; }
        /// <summary>
        /// 仓库 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }

    }
}