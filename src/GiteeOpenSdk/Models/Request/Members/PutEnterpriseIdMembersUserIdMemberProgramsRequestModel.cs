using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 给成员添加/移出项目
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdMembersUserIdMemberProgramsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 成员 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("user_id")]
        public int UserId { get; set; }
        /// <summary>
        /// 加入ids, 逗号(,)隔开。add_ids，remove_ids 至少填一项，不能都为空
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("add_ids")]
        public string AddIds { get; set; }
        /// <summary>
        /// 退出ids, 逗号(,)隔开
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("remove_ids")]
        public string RemoveIds { get; set; }

    }
}