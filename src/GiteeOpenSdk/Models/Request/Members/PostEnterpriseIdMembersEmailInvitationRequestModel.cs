using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 添加成员到企业-邮件邀请
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdMembersEmailInvitationRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 企业角色的 ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("role_id")]
        public int? RoleId { get; set; }
        /// <summary>
        /// 是否需要管理员审核。1：需要，0：不需要
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("need_check")]
        public int? NeedCheck { get; set; }
        /// <summary>
        /// 邮件列表，逗号(,)隔开。每次邀请最多只能发送20封邀请邮件
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("emails")]
        public string Emails { get; set; }

    }
}