using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 企业外的成员-从企业组织或仓库中移除(单个/批量)
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdOutsideMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 企业外成员用户id，多个id逗号(,)隔开
        /// </summary>
        [RequestType("query")]
        [JsonProperty("user_ids")]
        public string UserIds { get; set; }

    }
}