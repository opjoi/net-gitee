using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 批量修改成员角色
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdMembersBatchChangeRoleRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 成员角色 ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("role_id")]
        public int RoleId { get; set; }
        /// <summary>
        /// 成员 ids, 逗号(,)隔开空
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("user_ids")]
        public string UserIds { get; set; }

    }
}