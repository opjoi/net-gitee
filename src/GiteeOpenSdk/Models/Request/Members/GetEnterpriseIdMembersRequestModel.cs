using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 获取企业成员列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 角色类型. (viewer: 观察者; member: 普通成员; outsourced_member: 外包成员; human_resources: 人事管理员; admin: 管理员; super_admin: 超级管理员; enterprise_owner: 企业拥有者;)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 1: 筛选已锁定的用户。
        /// </summary>
        [RequestType("query")]
        [JsonProperty("is_block")]
        public int? IsBlock { get; set; }
        /// <summary>
        /// 筛选团队的成员列表
        /// </summary>
        [RequestType("query")]
        [JsonProperty("group_id")]
        public int? GroupId { get; set; }
        /// <summary>
        /// 筛选指定角色的成员列表
        /// </summary>
        [RequestType("query")]
        [JsonProperty("role_id")]
        public int? RoleId { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 排序字段(created_at: 创建时间 remark: 在企业的备注)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}