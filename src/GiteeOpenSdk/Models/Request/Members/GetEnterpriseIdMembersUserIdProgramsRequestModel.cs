using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 获取成员加入的项目列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdMembersUserIdProgramsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 用户id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 筛选项(not_joined: 未加入的项目列表)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }

    }
}