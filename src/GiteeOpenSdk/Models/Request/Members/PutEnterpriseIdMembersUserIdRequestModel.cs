using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 更新企业成员信息
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdMembersUserIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 成员 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("user_id")]
        public int UserId { get; set; }
        /// <summary>
        /// 企业备注
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("remark")]
        public string Remark { get; set; }
        /// <summary>
        /// 职位信息
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("occupation")]
        public string Occupation { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("phone")]
        public string Phone { get; set; }
        /// <summary>
        /// 成员角色
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("role_id")]
        public string RoleId { get; set; }

    }
}