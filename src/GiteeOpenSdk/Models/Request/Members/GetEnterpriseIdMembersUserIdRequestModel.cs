using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 获取企业成员详情
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdMembersUserIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 用户id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("qt")]
        public string Qt { get; set; }

    }
}