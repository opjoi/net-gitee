using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Members
{
    /// <summary>
    /// 发送密码重置邮件
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdMembersUserIdResetPasswordEmailRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 成员 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("user_id")]
        public int UserId { get; set; }
        /// <summary>
        /// 短信验证码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("sms_captcha")]
        public string SmsCaptcha { get; set; }
        /// <summary>
        /// 用户密码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("password")]
        public string Password { get; set; }
        /// <summary>
        /// 验证方式
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("validate_type")]
        public string ValidateType { get; set; }

    }
}