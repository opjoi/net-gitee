using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Reaction
{
    /// <summary>
    /// 新增表态
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdReactionsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 目标类型(PullRequest, Note, Issue, TestNote)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("target_type")]
        public string TargetType { get; set; }
        /// <summary>
        /// 目标对象的 id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("target_id")]
        public int TargetId { get; set; }
        /// <summary>
        /// 表态内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("text")]
        public string Text { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}