using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Reaction
{
    /// <summary>
    /// 删除表态
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteEnterpriseIdReactionsReactionIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 表态的 id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("reaction_id")]
        public int ReactionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}