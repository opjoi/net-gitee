using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 获取一条通知
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5NotificationsThreadsIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 通知的 ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public string Id { get; set; }

    }
}