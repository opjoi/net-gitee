using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 标记一条通知为已读
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5NotificationsThreadsIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 通知的 ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public string Id { get; set; }

    }
}