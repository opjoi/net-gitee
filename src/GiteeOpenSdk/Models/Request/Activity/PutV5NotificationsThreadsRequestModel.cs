using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 标记所有通知为已读
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5NotificationsThreadsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 指定一组通知 ID，以 , 分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("ids")]
        public string Ids { get; set; }

    }
}