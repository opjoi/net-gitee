using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Docs
{
    /// <summary>
    /// 获取文档下的文件列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdDocsDocIdFilesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 文档 id(doc_nodes 文档列表接口的 file_id 字段)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_id")]
        public int DocId { get; set; }

    }
}