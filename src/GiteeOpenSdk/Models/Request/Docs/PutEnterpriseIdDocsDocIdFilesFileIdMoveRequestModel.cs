using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Docs
{
    /// <summary>
    /// 移动文件
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocsDocIdFilesFileIdMoveRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 文档 id(doc_nodes 文档列表接口的 file_id 字段)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_id")]
        public int DocId { get; set; }
        /// <summary>
        /// 文件 id(docs 获取文件列表的 id 字段)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("file_id")]
        public int FileId { get; set; }
        /// <summary>
        /// 文件移动后的父级的 file_id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("parent_id")]
        public int ParentId { get; set; }
        /// <summary>
        /// 移动后的同级上一个节点的 file_id（默认：0）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("prev_id")]
        public int PrevId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}