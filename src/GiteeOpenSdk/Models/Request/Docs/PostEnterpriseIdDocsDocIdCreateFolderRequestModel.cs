using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Docs
{
    /// <summary>
    /// 创建文件夹
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdDocsDocIdCreateFolderRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 文档 id(doc_nodes 文档列表接口的 file_id 字段)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_id")]
        public int DocId { get; set; }
        /// <summary>
        /// 文件夹名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 父级 id(默认为 0)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}