using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Docs
{
    /// <summary>
    /// 更新文件
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdDocsDocIdFilesFileIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 文档 id(doc_nodes 文档列表接口的 file_id 字段)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("doc_id")]
        public int DocId { get; set; }
        /// <summary>
        /// 文件 id(docs 获取文件列表的 id 字段)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("file_id")]
        public int FileId { get; set; }
        /// <summary>
        /// 文件标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 文件内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 提交
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("message")]
        public string Message { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}