using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.CustomizeIssue
{
    /// <summary>
    /// 获取管理界面的字段列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdCustomizeListRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 是否排序（被选的字段排后面）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public int? Sort { get; set; }
        /// <summary>
        /// 任务类型 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("issue_type_id")]
        public int? IssueTypeId { get; set; }
        /// <summary>
        /// 获取字段列表来源（导出/企业管理）
        /// </summary>
        [RequestType("query")]
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }

    }
}