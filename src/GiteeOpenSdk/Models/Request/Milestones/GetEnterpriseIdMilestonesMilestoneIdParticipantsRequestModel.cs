using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Milestones
{
    /// <summary>
    /// 里程碑参与人列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdMilestonesMilestoneIdParticipantsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 里程碑ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("milestone_id")]
        public int MilestoneId { get; set; }

    }
}