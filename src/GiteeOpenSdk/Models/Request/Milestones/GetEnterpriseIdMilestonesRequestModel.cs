using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Milestones
{
    /// <summary>
    /// 获取里程碑列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetEnterpriseIdMilestonesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 仓库 id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("project_id")]
        public int? ProjectId { get; set; }
        /// <summary>
        /// 状态，开启的:active，待开始:pending，进行中:processing，已关闭:closed，已逾期:overdued， 未过期：not_overdue
        /// </summary>
        [RequestType("query")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 负责人id以,分隔的字符串
        /// </summary>
        [RequestType("query")]
        [JsonProperty("assignee_ids")]
        public string AssigneeIds { get; set; }
        /// <summary>
        /// 创建者id以,分隔的字符串
        /// </summary>
        [RequestType("query")]
        [JsonProperty("author_ids")]
        public string AuthorIds { get; set; }
        /// <summary>
        /// 搜索字符串
        /// </summary>
        [RequestType("query")]
        [JsonProperty("search")]
        public string Search { get; set; }
        /// <summary>
        /// 排序字段(title、created_at、start_date、due_date)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方向(asc: 升序 desc: 倒序)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 筛选当前用户参与的里程碑
        /// </summary>
        [RequestType("query")]
        [JsonProperty("scope")]
        public string Scope { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}