using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Milestones
{
    /// <summary>
    /// 编辑里程碑
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdMilestonesMilestoneIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// 里程碑ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("milestone_id")]
        public int MilestoneId { get; set; }
        /// <summary>
        /// 里程碑标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 里程碑结束日期如：2020-08-13
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("due_date")]
        public string DueDate { get; set; }
        /// <summary>
        /// 里程碑开始日期如：2020-08-13
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("start_date")]
        public string StartDate { get; set; }
        /// <summary>
        /// 关闭或重新开启里程碑
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("state_event")]
        public string StateEvent { get; set; }
        /// <summary>
        /// 里程碑描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 关联项目ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("program_id")]
        public int? ProgramId { get; set; }
        /// <summary>
        /// 关联仓库ID, 以,分隔的字符串
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project_ids")]
        public string ProjectIds { get; set; }
        /// <summary>
        /// 里程碑负责人ID
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignee_id")]
        public int? AssigneeId { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("top")]
        public int? Top { get; set; }

    }
}