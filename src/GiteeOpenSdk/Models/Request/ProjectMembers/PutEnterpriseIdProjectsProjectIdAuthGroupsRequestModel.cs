using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectMembers
{
    /// <summary>
    /// 调整团队授权
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdAuthGroupsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 被授权团队的id，多个id用英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("group_ids")]
        public string GroupIds { get; set; }
        /// <summary>
        /// 授权团队的成员在仓库的权限
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_level")]
        public int AccessLevel { get; set; }

    }
}