using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectMembers
{
    /// <summary>
    /// 添加仓库成员
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostEnterpriseIdProjectsProjectIdMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 要添加的成员信息,例如[{&quot;id&quot;:&quot;13&quot;, &quot;access&quot;:&quot;30&quot;, &quot;name&quot;:&quot;真喜洋洋 (xiyangyang)&quot;, &quot;username&quot;:&quot;xiyangyang&quot;}]
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("users")]
        public string Users { get; set; }
        /// <summary>
        /// 授权团队的id，多个ID通过英文逗号分隔
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("groups")]
        public string Groups { get; set; }
        /// <summary>
        /// 授权团队的成员在仓库的权限
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_level")]
        public int? AccessLevel { get; set; }

    }
}