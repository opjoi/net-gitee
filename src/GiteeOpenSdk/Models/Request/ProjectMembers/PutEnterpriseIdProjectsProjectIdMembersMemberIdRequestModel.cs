using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.ProjectMembers
{
    /// <summary>
    /// 修改仓库成员权限
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutEnterpriseIdProjectsProjectIdMembersMemberIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise_id")]
        public int EnterpriseId { get; set; }
        /// <summary>
        /// path类型（查询参数为path）, 空则表示查询参数为id
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("qt")]
        public string Qt { get; set; }
        /// <summary>
        /// 仓库 id 或 path
        /// </summary>
        [RequestType("path")]
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 成员id
        /// </summary>
        [RequestType("path")]
        [JsonProperty("member_id")]
        public int MemberId { get; set; }
        /// <summary>
        /// 仓库角色级别, 报告者:15,观察者:25,开发者:30,管理员:40,负责人:100
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("project_access")]
        public int ProjectAccess { get; set; }

    }
}