using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取用户企业任务集合
    /// </summary>
    public partial class IssueListResponseModel : GiteeResponseModel
    {

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("total_count")]
        public int? Total_count { get; set; }
        /// <summary>
        /// 项目 id
        /// </summary>
        [JsonProperty("data")]
        public List<IssueResponseModel> IssueLists { get; set; }
    }
    /// <summary>
    /// 指定任务类型,任务状态下任务列表
    /// </summary>
    public partial class IssueResponseModel 
    {
        /// <summary>
        /// 任务 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 根结点 ID
        /// </summary>
       [JsonProperty("root_id")]
        public int? Root_id { get; set; }
        /// <summary>
        /// 父任务 ID
        /// </summary>
       [JsonProperty("parent_id")]
        public int? Parent_id { get; set; }
        /// <summary>
        /// 任务全局唯一标识符
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 任务标题
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务状态标识符: open, progressing, closed, rejected
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 评论数量
        /// </summary>
       [JsonProperty("comments_count")]
        public int? Comments_count { get; set; }
        /// <summary>
        /// 优先级标识符
        /// </summary>
       [JsonProperty("priority")]
        public int? Priority { get; set; }
        /// <summary>
        /// 关联的分支名
        /// </summary>
       [JsonProperty("branch")]
        public string Branch { get; set; }
        /// <summary>
        /// 优先级中文名称
        /// </summary>
       [JsonProperty("priority_human")]
        public string Priority_human { get; set; }
        /// <summary>
        /// 任务负责人
        /// </summary>
       [JsonProperty("assignee")]
        public string Assignee { get; set; }
        /// <summary>
        /// 预计工时。（单位：分钟）
        /// </summary>
       [JsonProperty("duration")]
        public int? Duration { get; set; }
        /// <summary>
        /// 任务创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 任务协作者
        /// </summary>
       [JsonProperty("collaborators")]
        public object Collaborators { get; set; }
        /// <summary>
        /// 任务创建者
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 里程碑
        /// </summary>
       [JsonProperty("milestone")]
        public MilestoneResponseModel Milestone { get; set; }
        /// <summary>
        /// 任务状态
        /// </summary>
       [JsonProperty("issue_state")]
        public IssueStateResponseModel Issue_state { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
       [JsonProperty("issue_type")]
        public IssueTypeResponseModel Issue_type { get; set; }
        /// <summary>
        /// 任务关联的标签
        /// </summary>
       [JsonProperty("labels")]
        public LabelResponseModel Labels { get; set; }
        /// <summary>
        /// 任务自定义字段值
        /// </summary>
       [JsonProperty("issue_extra")]
        public LabelResponseModel Issue_extra { get; set; }
        /// <summary>
        /// 计划开始时间
        /// </summary>
       [JsonProperty("plan_started_at")]
        public DateTime Plan_started_at { get; set; }
        /// <summary>
        /// 计划完成时间
        /// </summary>
       [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        /// <summary>
        /// 完成时间
        /// </summary>
       [JsonProperty("finished_at")]
        public DateTime Finished_at { get; set; }
        /// <summary>
        /// 是否是私有Issue
        /// </summary>
       [JsonProperty("security_hole")]
        public bool? Security_hole { get; set; }
        /// <summary>
        /// 是否星标任务
        /// </summary>
       [JsonProperty("is_star")]
        public bool? Is_star { get; set; }
        /// <summary>
        /// 所属看板
        /// </summary>
       [JsonProperty("kanban_info")]
        public bool? Kanban_info { get; set; }

    }
}