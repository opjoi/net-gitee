using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 取消PR表态
    /// </summary>
    public partial class IssueReactionResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 表态对象id
        /// </summary>
       [JsonProperty("target_id")]
        public int? Target_id { get; set; }
        /// <summary>
        /// 表态对象类型
        /// </summary>
       [JsonProperty("target_type")]
        public string Target_type { get; set; }
        /// <summary>
        /// 表态
        /// </summary>
       [JsonProperty("text")]
        public string Text { get; set; }
        /// <summary>
        /// 表态人id
        /// </summary>
       [JsonProperty("user_id")]
        public int? User_id { get; set; }
        /// <summary>
        /// 表态人名
        /// </summary>
       [JsonProperty("user_name")]
        public string User_name { get; set; }

    }
}