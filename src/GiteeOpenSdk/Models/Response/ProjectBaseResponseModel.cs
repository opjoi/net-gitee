using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ProjectBaseResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 仓库ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// namespace/path
        /// </summary>
       [JsonProperty("path_with_namespace")]
        public string Path_with_namespace { get; set; }
        /// <summary>
        /// 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
        /// </summary>
       [JsonProperty("public")]
        public int? Public { get; set; }
        /// <summary>
        /// 仓库创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 仓库更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 是否为GVP仓库
        /// </summary>
       [JsonProperty("is_gvp")]
        public bool? Is_gvp { get; set; }
        /// <summary>
        /// 是否为空仓库
        /// </summary>
       [JsonProperty("is_empty_repo")]
        public bool? Is_empty_repo { get; set; }
        /// <summary>
        /// 是否允许 fork
        /// </summary>
       [JsonProperty("fork_enabled")]
        public bool? Fork_enabled { get; set; }
        /// <summary>
        /// namespace_name/path
        /// </summary>
       [JsonProperty("name_with_namespace")]
        public string Name_with_namespace { get; set; }

    }
}