using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 修改任务下的评论
    /// </summary>
    public partial class IssueNoteResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 评论的 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 评论类型
        /// </summary>
       [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 评论发起人
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 评论内容(markdown 格式)
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 评论内容(html 格式)
        /// </summary>
       [JsonProperty("content_html")]
        public string Content_html { get; set; }
        /// <summary>
        /// 代码建议汇集信息{changed: boolean 代码是否不一致, raw: string 建议的code}
        /// </summary>
       [JsonProperty("suggestions")]
        public object Suggestions { get; set; }
        /// <summary>
        /// 表态
        /// </summary>
       [JsonProperty("reactions")]
        public ReactionResponseModel Reactions { get; set; }
        /// <summary>
        /// 评论编辑、删除权限
        /// </summary>
       [JsonProperty("operating")]
        public ReactionResponseModel Operating { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}