using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取任务指派的成员列表
    /// </summary>
    public partial class IssueMemberSelectResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 用户 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 用户个性地址
        /// </summary>
       [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 用户在企业的备注名
        /// </summary>
       [JsonProperty("remark")]
        public string Remark { get; set; }
        /// <summary>
        /// 成员备注或名称拼音
        /// </summary>
       [JsonProperty("pinyin")]
        public string Pinyin { get; set; }
        /// <summary>
        /// 用户头像
        /// </summary>
       [JsonProperty("avatar_url")]
        public string Avatar_url { get; set; }
        /// <summary>
        /// 是否企业成员
        /// </summary>
       [JsonProperty("is_enterprise_member")]
        public bool? Is_enterprise_member { get; set; }
        /// <summary>
        /// 是否外包成员
        /// </summary>
       [JsonProperty("outsourced")]
        public bool? Outsourced { get; set; }
        /// <summary>
        /// 是否项目成员
        /// </summary>
       [JsonProperty("is_program_member")]
        public bool? Is_program_member { get; set; }
        /// <summary>
        /// 是否仓库成员
        /// </summary>
       [JsonProperty("is_project_member")]
        public bool? Is_project_member { get; set; }

    }
}