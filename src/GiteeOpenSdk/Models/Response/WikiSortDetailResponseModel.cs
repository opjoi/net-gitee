using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 更新文件
    /// </summary>
    public partial class WikiSortDetailResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 文件 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 父级 id
        /// </summary>
       [JsonProperty("parent_id")]
        public int? Parent_id { get; set; }
        /// <summary>
        /// 文件创建者
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 最后编辑者
        /// </summary>
       [JsonProperty("editor")]
        public UserWithRemarkResponseModel Editor { get; set; }
        /// <summary>
        /// 文件类型(文件: 1; 文件夹: 2)
        /// </summary>
       [JsonProperty("file_type")]
        public int? File_type { get; set; }
        /// <summary>
        /// 文件的最后一次版本号
        /// </summary>
       [JsonProperty("version")]
        public string Version { get; set; }
        /// <summary>
        /// 文件的最后编辑时间
        /// </summary>
       [JsonProperty("last_edit_time")]
        public DateTime Last_edit_time { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 文件内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 文件详情 html
        /// </summary>
       [JsonProperty("content_html")]
        public string Content_html { get; set; }

    }
}