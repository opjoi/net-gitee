using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取授权用户对 PR 的权限
    /// </summary>
    public partial class PullRequestAuthResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 是否可查看此 PR
        /// </summary>
       [JsonProperty("read")]
        public bool? Read { get; set; }
        /// <summary>
        /// 是否可评论此 PR
        /// </summary>
       [JsonProperty("create_note")]
        public bool? Create_note { get; set; }
        /// <summary>
        /// 是否可更新 PR 的信息
        /// </summary>
       [JsonProperty("update")]
        public bool? Update { get; set; }
        /// <summary>
        /// PR 当前的状态是否处于可合并
        /// </summary>
       [JsonProperty("merge_status_check")]
        public bool? Merge_status_check { get; set; }
        /// <summary>
        /// 授权用户是否可以让此 PR 审查通过
        /// </summary>
       [JsonProperty("can_check")]
        public bool? Can_check { get; set; }
        /// <summary>
        /// 授权用户是否可以让此 PR 测试通过
        /// </summary>
       [JsonProperty("can_test")]
        public bool? Can_test { get; set; }
        /// <summary>
        /// 授权用户是否有权限可以合并此 PR
        /// </summary>
       [JsonProperty("can_merge")]
        public bool? Can_merge { get; set; }
        /// <summary>
        /// 授权用户是否可把 PR 的审查状态强制设置为已通过
        /// </summary>
       [JsonProperty("force_checked")]
        public bool? Force_checked { get; set; }
        /// <summary>
        /// 授权用户是否可把 PR 的测试状态强制设置为已通过
        /// </summary>
       [JsonProperty("force_tested")]
        public bool? Force_tested { get; set; }

    }
}