using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取里程碑列表
    /// </summary>
    public partial class MilestoneListResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 里程碑 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 里程碑标题名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 里程碑状态
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 里程碑起始日期
        /// </summary>
       [JsonProperty("start_date")]
        public DateTime Start_date { get; set; }
        /// <summary>
        /// 里程碑结束日期
        /// </summary>
       [JsonProperty("due_date")]
        public DateTime Due_date { get; set; }
        /// <summary>
        /// 任务标签创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务标签更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 里程碑所属项目 ID
        /// </summary>
       [JsonProperty("program_id")]
        public int? Program_id { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 描述(html格式)
        /// </summary>
       [JsonProperty("description_html")]
        public string Description_html { get; set; }
        /// <summary>
        /// 里程碑负责人
        /// </summary>
       [JsonProperty("assignee")]
        public UserWithRemarkResponseModel Assignee { get; set; }
        /// <summary>
        /// 里程碑创建者
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 里程碑issues数量
        /// </summary>
       [JsonProperty("issue_all_count")]
        public int? Issue_all_count { get; set; }
        /// <summary>
        /// 里程碑完成issue数量
        /// </summary>
       [JsonProperty("issue_complete_count")]
        public int? Issue_complete_count { get; set; }
        /// <summary>
        /// 里程碑pr数量
        /// </summary>
       [JsonProperty("pr_all_count")]
        public int? Pr_all_count { get; set; }
        /// <summary>
        /// 里程碑完成pr数量
        /// </summary>
       [JsonProperty("pr_complete_count")]
        public int? Pr_complete_count { get; set; }
        /// <summary>
        /// 里程碑关联仓库
        /// </summary>
       [JsonProperty("projects")]
        public ProjectResponseModel Projects { get; set; }
        /// <summary>
        /// 里程碑关联项目
        /// </summary>
       [JsonProperty("program")]
        public ProgramResponseModel Program { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
       [JsonProperty("top")]
        public ProgramResponseModel Top { get; set; }

    }
}