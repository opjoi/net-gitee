using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// commits列表
    /// </summary>
    public partial class CommitListResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 日期
        /// </summary>
       [JsonProperty("day")]
        public DateTime Day { get; set; }
        /// <summary>
        /// commit 数
        /// </summary>
       [JsonProperty("count")]
        public int? Count { get; set; }
        /// <summary>
        /// commits 列表
        /// </summary>
       [JsonProperty("commits")]
        public CommitBaseResponseModel Commits { get; set; }

    }
}