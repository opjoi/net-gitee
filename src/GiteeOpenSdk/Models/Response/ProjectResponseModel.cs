using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 部署公钥添加仓库
    /// </summary>
    public partial class ProjectResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 仓库ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
        /// </summary>
       [JsonProperty("public")]
        public int? Public { get; set; }
        /// <summary>
        /// 企业 id
        /// </summary>
       [JsonProperty("enterprise_id")]
        public int? Enterprise_id { get; set; }
        /// <summary>
        /// 仓库创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 仓库更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}