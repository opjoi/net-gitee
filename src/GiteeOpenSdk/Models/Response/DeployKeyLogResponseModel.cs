using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 企业部署公钥管理日志
    /// </summary>
    public partial class DeployKeyLogResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
       [JsonProperty("user_id")]
        public int? User_id { get; set; }
        /// <summary>
        /// 操作用户
        /// </summary>
       [JsonProperty("user")]
        public UserWithRemarkResponseModel User { get; set; }
        /// <summary>
        /// ip
        /// </summary>
       [JsonProperty("ip")]
        public string Ip { get; set; }
        /// <summary>
        /// 目标操作对象ID
        /// </summary>
       [JsonProperty("target_id")]
        public int? Target_id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 操作对象名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 操作对象类型
        /// </summary>
       [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 操作
        /// </summary>
       [JsonProperty("operating")]
        public string Operating { get; set; }
        /// <summary>
        /// 操作对象：仓库
        /// </summary>
       [JsonProperty("target_project")]
        public ProjectResponseModel Target_project { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
       [JsonProperty("target_project_name")]
        public string Target_project_name { get; set; }
        /// <summary>
        /// 公钥名称
        /// </summary>
       [JsonProperty("target_deploy_key_title")]
        public string Target_deploy_key_title { get; set; }
        /// <summary>
        /// 操作对象：部署公钥
        /// </summary>
       [JsonProperty("target_deploy_key")]
        public DeployKeyResponseModel Target_deploy_key { get; set; }

    }
}