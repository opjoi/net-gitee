using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 评论、回复 Commit
    /// </summary>
    public partial class CommitNoteDetailResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 评论的 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 评论类型
        /// </summary>
       [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 评论发起人
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 评论内容(markdown 格式)
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 评论内容(html 格式)
        /// </summary>
       [JsonProperty("content_html")]
        public string Content_html { get; set; }
        /// <summary>
        /// 代码建议汇集信息{changed: boolean 代码是否不一致, raw: string 建议的code}
        /// </summary>
       [JsonProperty("suggestions")]
        public object Suggestions { get; set; }
        /// <summary>
        /// 表态
        /// </summary>
       [JsonProperty("reactions")]
        public ReactionResponseModel Reactions { get; set; }
        /// <summary>
        /// 评论编辑、删除权限
        /// </summary>
       [JsonProperty("operating")]
        public ReactionResponseModel Operating { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 代码行标记
        /// </summary>
       [JsonProperty("line_code")]
        public string Line_code { get; set; }
        /// <summary>
        /// 评论引用的代码块 id
        /// </summary>
       [JsonProperty("diff_position_id")]
        public int? Diff_position_id { get; set; }
        /// <summary>
        /// 上级评论的ids，上下级关系为树组index顺序
        /// </summary>
       [JsonProperty("ancestry_ids")]
        public object Ancestry_ids { get; set; }
        /// <summary>
        /// 代码评论是否过期
        /// </summary>
       [JsonProperty("outdated")]
        public bool? Outdated { get; set; }
        /// <summary>
        /// 代码评论是否解决
        /// </summary>
       [JsonProperty("resolved")]
        public bool? Resolved { get; set; }
        /// <summary>
        /// 代码评论信息
        /// </summary>
       [JsonProperty("position")]
        public bool? Position { get; set; }
        /// <summary>
        /// 上级评论id,评论人信息
        /// </summary>
       [JsonProperty("parent")]
        public bool? Parent { get; set; }
        /// <summary>
        /// 评论解决人
        /// </summary>
       [JsonProperty("resolved_user")]
        public UserWithRemarkResponseModel Resolved_user { get; set; }

    }
}