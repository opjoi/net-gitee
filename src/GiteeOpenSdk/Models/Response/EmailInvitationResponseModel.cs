using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 添加成员到企业-邮件邀请
    /// </summary>
    public partial class EmailInvitationResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 邮箱
        /// </summary>
       [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 邀请状态，true: 成功，false: 失败
        /// </summary>
       [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// 失败原因
        /// </summary>
       [JsonProperty("message")]
        public string Message { get; set; }

    }
}