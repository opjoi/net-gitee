using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 企业文档管理日志
    /// </summary>
    public partial class DocNodeLogResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
       [JsonProperty("user_id")]
        public int? User_id { get; set; }
        /// <summary>
        /// 操作用户
        /// </summary>
       [JsonProperty("user")]
        public UserWithRemarkResponseModel User { get; set; }
        /// <summary>
        /// ip
        /// </summary>
       [JsonProperty("ip")]
        public string Ip { get; set; }
        /// <summary>
        /// 目标操作对象ID
        /// </summary>
       [JsonProperty("target_id")]
        public int? Target_id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 操作对象名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 操作
        /// </summary>
       [JsonProperty("operating")]
        public string Operating { get; set; }
        /// <summary>
        /// 操作对象
        /// </summary>
       [JsonProperty("target_doc")]
        public string Target_doc { get; set; }

    }
}