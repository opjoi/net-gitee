using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 给成员添加/移出项目
    /// </summary>
    public partial class MemberBulkModifyResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 成功删除id
        /// </summary>
       [JsonProperty("removed_ids")]
        public object Removed_ids { get; set; }
        /// <summary>
        /// 成功添加id
        /// </summary>
       [JsonProperty("added_ids")]
        public object Added_ids { get; set; }

    }
}