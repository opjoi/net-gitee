using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 指派审查、测试人员
    /// </summary>
    public partial class PrAssignerResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 审查人员id列表
        /// </summary>
       [JsonProperty("assigners")]
        public string Assigners { get; set; }
        /// <summary>
        /// 测试人员id列表
        /// </summary>
       [JsonProperty("testers")]
        public string Testers { get; set; }
        /// <summary>
        /// 可合并的审查人员门槛数
        /// </summary>
       [JsonProperty("pr_assign_num")]
        public int? Pr_assign_num { get; set; }
        /// <summary>
        /// 可合并的测试人员门槛数
        /// </summary>
       [JsonProperty("pr_test_num")]
        public int? Pr_test_num { get; set; }

    }
}