using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 添加仓库成员
    /// </summary>
    public partial class ProjectMemberAddResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 成员 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
       [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 成员名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 成员命名空间
        /// </summary>
       [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 返回信息
        /// </summary>
       [JsonProperty("msg")]
        public string Msg { get; set; }
        /// <summary>
        /// 是否成功
        /// </summary>
       [JsonProperty("success")]
        public bool? Success { get; set; }
        /// <summary>
        /// 是否是注册用户
        /// </summary>
       [JsonProperty("no_register")]
        public bool? No_register { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
       [JsonProperty("role_name")]
        public string Role_name { get; set; }
        /// <summary>
        /// 角色级别
        /// </summary>
       [JsonProperty("access")]
        public string Access { get; set; }
        /// <summary>
        /// 角色状态
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }

    }
}