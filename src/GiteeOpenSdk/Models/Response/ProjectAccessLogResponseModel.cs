using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 仓库代码日志
    /// </summary>
    public partial class ProjectAccessLogResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// uuid
        /// </summary>
       [JsonProperty("uuid")]
        public string Uuid { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
       [JsonProperty("user_id")]
        public string User_id { get; set; }
        /// <summary>
        /// 操作用户，如果user是null，表示已删除
        /// </summary>
       [JsonProperty("user")]
        public UserWithRemarkResponseModel User { get; set; }
        /// <summary>
        /// ip
        /// </summary>
       [JsonProperty("ip")]
        public string Ip { get; set; }
        /// <summary>
        /// 过滤后IP
        /// </summary>
       [JsonProperty("ip_filter")]
        public string Ip_filter { get; set; }
        /// <summary>
        /// 操作: {0=&gt;&quot;HTTP_ACCESS&quot;, 1=&gt;&quot;SSH_PULL&quot;, 2=&gt;&quot;SVN_PULL&quot;, 3=&gt;&quot;HTTP_PULL&quot;, 4=&gt;&quot;SSH_PUSH&quot;, 5=&gt;&quot;SVN_PUSH&quot;, 6=&gt;&quot;HTTP_PUSH&quot;, 7=&gt;&quot;DOWNLOAD_ZIP&quot;}
        /// </summary>
       [JsonProperty("stat_type")]
        public string Stat_type { get; set; }
        /// <summary>
        /// 操作
        /// </summary>
       [JsonProperty("stat_type_cn")]
        public string Stat_type_cn { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
       [JsonProperty("project_id")]
        public string Project_id { get; set; }
        /// <summary>
        /// 仓库，如果project是null，表示已删除
        /// </summary>
       [JsonProperty("project")]
        public ProjectResponseModel Project { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}