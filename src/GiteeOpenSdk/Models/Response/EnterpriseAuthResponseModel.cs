using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取授权用户在企业拥有的权限
    /// </summary>
    public partial class EnterpriseAuthResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 是否可查看成员列表
        /// </summary>
       [JsonProperty("read_member")]
        public bool? Read_member { get; set; }
        /// <summary>
        /// 是否可添加企业成员
        /// </summary>
       [JsonProperty("add_member")]
        public bool? Add_member { get; set; }
        /// <summary>
        /// 是否可查看授权用户参与的团队列表
        /// </summary>
       [JsonProperty("read_group")]
        public bool? Read_group { get; set; }
        /// <summary>
        /// 是否可新增团队
        /// </summary>
       [JsonProperty("add_group")]
        public bool? Add_group { get; set; }
        /// <summary>
        /// 是否可查看授权用户参与的任务列表
        /// </summary>
       [JsonProperty("read_issue")]
        public bool? Read_issue { get; set; }
        /// <summary>
        /// 是否可创建任务
        /// </summary>
       [JsonProperty("create_issue")]
        public bool? Create_issue { get; set; }
        /// <summary>
        /// 是否可查看授权用户参与的项目列表
        /// </summary>
       [JsonProperty("read_program")]
        public bool? Read_program { get; set; }
        /// <summary>
        /// 是否可创建项目
        /// </summary>
       [JsonProperty("create_program")]
        public bool? Create_program { get; set; }
        /// <summary>
        /// 是否可查看授权用户参与的仓库列表
        /// </summary>
       [JsonProperty("read_project")]
        public bool? Read_project { get; set; }
        /// <summary>
        /// 是否可读取周报
        /// </summary>
       [JsonProperty("read_week_report")]
        public bool? Read_week_report { get; set; }
        /// <summary>
        /// 是否可读取任务类型
        /// </summary>
       [JsonProperty("read_issue_type")]
        public bool? Read_issue_type { get; set; }
        /// <summary>
        /// 是否可管理任务类型
        /// </summary>
       [JsonProperty("setting_issue_type")]
        public bool? Setting_issue_type { get; set; }
        /// <summary>
        /// 是否可读取标签
        /// </summary>
       [JsonProperty("read_label")]
        public bool? Read_label { get; set; }
        /// <summary>
        /// 是否可管理标签
        /// </summary>
       [JsonProperty("setting_label")]
        public bool? Setting_label { get; set; }
        /// <summary>
        /// 是否可管理企业
        /// </summary>
       [JsonProperty("admin_enterprise")]
        public bool? Admin_enterprise { get; set; }

    }
}