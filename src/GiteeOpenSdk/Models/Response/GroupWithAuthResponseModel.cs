using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取成员加入的团队列表
    /// </summary>
    public partial class GroupWithAuthResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 团队 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 团队名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 团队路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 团队头像
        /// </summary>
       [JsonProperty("avatar_url")]
        public string Avatar_url { get; set; }
        /// <summary>
        /// 团队描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// deprecated.团队的类型值。0: 内部 1:公开 2:外包
        /// </summary>
       [JsonProperty("group_type")]
        public int? Group_type { get; set; }
        /// <summary>
        /// 团队的类型名称
        /// </summary>
       [JsonProperty("group_type_human_name")]
        public string Group_type_human_name { get; set; }
        /// <summary>
        /// 团队的类型值。0: 内部 1:公开 2:外包
        /// </summary>
       [JsonProperty("public")]
        public int? Public { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
       [JsonProperty("creator")]
        public int? Creator { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 能否编辑退出
        /// </summary>
       [JsonProperty("can_quit")]
        public bool? Can_quit { get; set; }

    }
}