using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取任务附件
    /// </summary>
    public partial class AttachFileResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 附件 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 附件名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 附件的大小
        /// </summary>
       [JsonProperty("size")]
        public int? Size { get; set; }
        /// <summary>
        /// 附件的类型
        /// </summary>
       [JsonProperty("file_type")]
        public string File_type { get; set; }
        /// <summary>
        /// 预览链接
        /// </summary>
       [JsonProperty("preview_url")]
        public string Preview_url { get; set; }
        /// <summary>
        /// 附件的创建者
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 附件的创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 附件的更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 能否删除附件
        /// </summary>
       [JsonProperty("can_delete")]
        public bool? Can_delete { get; set; }

    }
}