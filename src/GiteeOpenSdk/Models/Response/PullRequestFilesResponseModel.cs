using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取 Commit 下的 diffs
    /// </summary>
    public partial class PullRequestFilesResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 文件评论数
        /// </summary>
       [JsonProperty("comments_count")]
        public int? Comments_count { get; set; }
        /// <summary>
        /// 文件已解决评论数
        /// </summary>
       [JsonProperty("comments_resolved_count")]
        public int? Comments_resolved_count { get; set; }
        /// <summary>
        /// Commit ID
        /// </summary>
       [JsonProperty("sha")]
        public string Sha { get; set; }
        /// <summary>
        /// 文件路径 sha 值
        /// </summary>
       [JsonProperty("file_path_sha")]
        public string File_path_sha { get; set; }
        /// <summary>
        /// 文件名
        /// </summary>
       [JsonProperty("filename")]
        public string Filename { get; set; }
        /// <summary>
        /// 文件改动类型。added: 新增 renamed: 重命名 deleted: 删除
        /// </summary>
       [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// 新增文件的行数
        /// </summary>
       [JsonProperty("additions")]
        public string Additions { get; set; }
        /// <summary>
        /// 删除文件的行数
        /// </summary>
       [JsonProperty("deletions")]
        public string Deletions { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("patch")]
        public string Patch { get; set; }
        /// <summary>
        /// diff 统计
        /// </summary>
       [JsonProperty("statistic")]
        public string Statistic { get; set; }
        /// <summary>
        /// diff head
        /// </summary>
       [JsonProperty("head")]
        public string Head { get; set; }
        /// <summary>
        /// diff 内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 已阅
        /// </summary>
       [JsonProperty("viewed")]
        public string Viewed { get; set; }
        /// <summary>
        /// code owner
        /// </summary>
       [JsonProperty("code_owners")]
        public string Code_owners { get; set; }

    }
}