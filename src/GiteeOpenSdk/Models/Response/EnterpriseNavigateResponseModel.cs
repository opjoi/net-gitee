using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 更新企业导航栏设置
    /// </summary>
    public partial class EnterpriseNavigateResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 导航卡 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 导航卡名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 导航卡图标
        /// </summary>
       [JsonProperty("icon")]
        public string Icon { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
       [JsonProperty("serial")]
        public int? Serial { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
       [JsonProperty("active")]
        public bool? Active { get; set; }
        /// <summary>
        /// 是否禁止修改
        /// </summary>
       [JsonProperty("fixed")]
        public bool? Fixed { get; set; }

    }
}