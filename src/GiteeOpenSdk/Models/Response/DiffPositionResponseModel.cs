using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取 Pull Request 评论引用的代码片段
    /// </summary>
    public partial class DiffPositionResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 评论引用的代码块id
        /// </summary>
       [JsonProperty("diff_position_id")]
        public int? Diff_position_id { get; set; }
        /// <summary>
        /// diff 统计
        /// </summary>
       [JsonProperty("statistic")]
        public string Statistic { get; set; }
        /// <summary>
        /// diff head
        /// </summary>
       [JsonProperty("head")]
        public string Head { get; set; }
        /// <summary>
        /// diff 内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 解决人
        /// </summary>
       [JsonProperty("resolved_user")]
        public string Resolved_user { get; set; }
        /// <summary>
        /// 解决人
        /// </summary>
       [JsonProperty("author")]
        public string Author { get; set; }
        /// <summary>
        /// 评论解决状态
        /// </summary>
       [JsonProperty("resolved_state")]
        public bool? Resolved_state { get; set; }
        /// <summary>
        /// 最近变动时间
        /// </summary>
       [JsonProperty("updated_at")]
        public string Updated_at { get; set; }

    }
}