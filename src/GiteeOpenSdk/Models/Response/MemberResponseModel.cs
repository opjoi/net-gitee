using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 修改仓库成员权限
    /// </summary>
    public partial class MemberResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 成员 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 成员个性地址
        /// </summary>
       [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 成员名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 成员在企业的备注姓名
        /// </summary>
       [JsonProperty("remark")]
        public string Remark { get; set; }
        /// <summary>
        /// 成员备注或名称拼音
        /// </summary>
       [JsonProperty("pinyin")]
        public string Pinyin { get; set; }
        /// <summary>
        /// 职位
        /// </summary>
       [JsonProperty("occupation")]
        public string Occupation { get; set; }
        /// <summary>
        /// 是否被锁定
        /// </summary>
       [JsonProperty("is_block")]
        public bool? Is_block { get; set; }
        /// <summary>
        /// 成员被锁定的原因
        /// </summary>
       [JsonProperty("block_message")]
        public string Block_message { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
       [JsonProperty("phone")]
        public string Phone { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
       [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 用户的基础信息
        /// </summary>
       [JsonProperty("user")]
        public UserResponseModel User { get; set; }
        /// <summary>
        /// 成员在企业的角色
        /// </summary>
       [JsonProperty("enterprise_role")]
        public EnterpriseRoleBaseResponseModel Enterprise_role { get; set; }
        /// <summary>
        /// 加入企业的时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 成员新版问卷反馈时间
        /// </summary>
       [JsonProperty("feedback_time")]
        public DateTime Feedback_time { get; set; }
        /// <summary>
        /// 成员是否填写过
        /// </summary>
       [JsonProperty("is_feedback")]
        public bool? Is_feedback { get; set; }
        /// <summary>
        /// 是否完成引导
        /// </summary>
       [JsonProperty("is_guided")]
        public bool? Is_guided { get; set; }
        /// <summary>
        /// 是否关闭了新手引导
        /// </summary>
       [JsonProperty("user_guide_closed")]
        public bool? User_guide_closed { get; set; }
        /// <summary>
        /// 新手引导的步骤指示
        /// </summary>
       [JsonProperty("user_guide_steps_finished")]
        public object User_guide_steps_finished { get; set; }

    }
}