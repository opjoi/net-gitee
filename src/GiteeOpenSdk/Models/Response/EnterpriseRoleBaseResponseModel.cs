using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class EnterpriseRoleBaseResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 角色 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 角色备注信息
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 是否系统默认角色
        /// </summary>
       [JsonProperty("is_system_default")]
        public bool? Is_system_default { get; set; }
        /// <summary>
        /// 角色类型标识符
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 是否企业默认角色
        /// </summary>
       [JsonProperty("is_default")]
        public bool? Is_default { get; set; }

    }
}