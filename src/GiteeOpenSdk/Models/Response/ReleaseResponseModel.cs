using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 删除发行版
    /// </summary>
    public partial class ReleaseResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// release
        /// </summary>
       [JsonProperty("release")]
        public object Release { get; set; }
        /// <summary>
        /// tag
        /// </summary>
       [JsonProperty("tag")]
        public object Tag { get; set; }
        /// <summary>
        /// release操作权限
        /// </summary>
       [JsonProperty("operating")]
        public object Operating { get; set; }

    }
}