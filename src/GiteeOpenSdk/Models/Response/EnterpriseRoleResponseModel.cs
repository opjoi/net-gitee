using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取企业角色列表
    /// </summary>
    public partial class EnterpriseRoleResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 角色 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 角色备注信息
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 是否系统默认角色
        /// </summary>
       [JsonProperty("is_system_default")]
        public bool? Is_system_default { get; set; }
        /// <summary>
        /// 角色类型标识符
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 是否企业默认角色
        /// </summary>
       [JsonProperty("is_default")]
        public bool? Is_default { get; set; }
        /// <summary>
        /// 归属于该角色下的成员数量
        /// </summary>
       [JsonProperty("member_count")]
        public int? Member_count { get; set; }
        /// <summary>
        /// 能否设置为默认角色
        /// </summary>
       [JsonProperty("can_set_default")]
        public bool? Can_set_default { get; set; }
        /// <summary>
        /// 角色创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 角色更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}