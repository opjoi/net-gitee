using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CommitSignatureResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 是否验证
        /// </summary>
       [JsonProperty("is_verified")]
        public bool? Is_verified { get; set; }
        /// <summary>
        /// 状态文案
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// GPG key ID
        /// </summary>
       [JsonProperty("gpg_key_primary_keyid")]
        public string Gpg_key_primary_keyid { get; set; }

    }
}