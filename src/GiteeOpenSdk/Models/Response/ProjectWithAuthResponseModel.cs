using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取成员加入的仓库列表
    /// </summary>
    public partial class ProjectWithAuthResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 仓库ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
        /// </summary>
       [JsonProperty("public")]
        public int? Public { get; set; }
        /// <summary>
        /// 企业 id
        /// </summary>
       [JsonProperty("enterprise_id")]
        public int? Enterprise_id { get; set; }
        /// <summary>
        /// 仓库创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 仓库更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 是否允许用户创建涉及敏感信息的任务
        /// </summary>
       [JsonProperty("security_hole_enabled")]
        public bool? Security_hole_enabled { get; set; }
        /// <summary>
        /// 仓库挂载的空间
        /// </summary>
       [JsonProperty("namespace")]
        public NamespaceResponseModel Namespace { get; set; }
        /// <summary>
        /// 仓库创建者
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 能否编辑退出
        /// </summary>
       [JsonProperty("can_quit")]
        public bool? Can_quit { get; set; }
        /// <summary>
        /// 当前成员在该仓库的角色
        /// </summary>
       [JsonProperty("project_access")]
        public int? Project_access { get; set; }

    }
}