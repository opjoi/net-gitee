using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class EnterpriseProjectResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 创建日期
        /// </summary>
       [JsonProperty("date")]
        public DateTime Date { get; set; }
        /// <summary>
        /// 企业仓库路径
        /// </summary>
       [JsonProperty("enterprise_project_path")]
        public string Enterprise_project_path { get; set; }
        /// <summary>
        /// 仓库id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 仓库公开与否
        /// </summary>
       [JsonProperty("public")]
        public int? Public { get; set; }

    }
}