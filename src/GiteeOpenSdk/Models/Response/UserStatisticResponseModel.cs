using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取成员的统计信息
    /// </summary>
    public partial class UserStatisticResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 唯一标示
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 对应用户的 ID
        /// </summary>
       [JsonProperty("user_id")]
        public int? User_id { get; set; }
        /// <summary>
        /// 企业 ID
        /// </summary>
       [JsonProperty("enterprise_id")]
        public int? Enterprise_id { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
       [JsonProperty("date")]
        public string Date { get; set; }
        /// <summary>
        /// 关闭的任务总数
        /// </summary>
       [JsonProperty("close_issue_count")]
        public int? Close_issue_count { get; set; }
        /// <summary>
        /// 提交的总数
        /// </summary>
       [JsonProperty("commit_count")]
        public int? Commit_count { get; set; }
        /// <summary>
        /// 创建的任务数
        /// </summary>
       [JsonProperty("create_issue_count")]
        public int? Create_issue_count { get; set; }
        /// <summary>
        /// 创建的 PR 数
        /// </summary>
       [JsonProperty("create_pr_count")]
        public int? Create_pr_count { get; set; }
        /// <summary>
        /// 合并的 PR 数
        /// </summary>
       [JsonProperty("merge_pr_count")]
        public int? Merge_pr_count { get; set; }
        /// <summary>
        /// 新增代码行
        /// </summary>
       [JsonProperty("add_code_line")]
        public int? Add_code_line { get; set; }

    }
}