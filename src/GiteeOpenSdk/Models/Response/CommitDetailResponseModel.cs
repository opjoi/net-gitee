using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// commit详情
    /// </summary>
    public partial class CommitDetailResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// Commit ID
        /// </summary>
       [JsonProperty("id")]
        public string Id { get; set; }
        /// <summary>
        /// Commit Short ID
        /// </summary>
       [JsonProperty("short_id")]
        public int? Short_id { get; set; }
        /// <summary>
        /// Commit Title
        /// </summary>
       [JsonProperty("title")]
        public int? Title { get; set; }
        /// <summary>
        /// Commit Html Title
        /// </summary>
       [JsonProperty("title_html")]
        public int? Title_html { get; set; }
        /// <summary>
        /// Commit Description
        /// </summary>
       [JsonProperty("description")]
        public int? Description { get; set; }
        /// <summary>
        /// Commit Html Description
        /// </summary>
       [JsonProperty("description_html")]
        public int? Description_html { get; set; }
        /// <summary>
        /// Commit Message
        /// </summary>
       [JsonProperty("message")]
        public int? Message { get; set; }
        /// <summary>
        /// Commit Html Message
        /// </summary>
       [JsonProperty("message_html")]
        public int? Message_html { get; set; }
        /// <summary>
        /// Commit 作者
        /// </summary>
       [JsonProperty("author")]
        public int? Author { get; set; }
        /// <summary>
        /// Commit 提交人
        /// </summary>
       [JsonProperty("committer")]
        public int? Committer { get; set; }
        /// <summary>
        /// 推送时间
        /// </summary>
       [JsonProperty("authored_date")]
        public int? Authored_date { get; set; }
        /// <summary>
        /// 提交时间
        /// </summary>
       [JsonProperty("committed_date")]
        public int? Committed_date { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
       [JsonProperty("signature")]
        public CommitSignatureResponseModel Signature { get; set; }
        /// <summary>
        /// Gitee Go构建状态
        /// </summary>
       [JsonProperty("build_state")]
        public int? Build_state { get; set; }
        /// <summary>
        /// 父节点
        /// </summary>
       [JsonProperty("parents")]
        public int? Parents { get; set; }
        /// <summary>
        /// diff 文件大小
        /// </summary>
       [JsonProperty("diff_files_size")]
        public int? Diff_files_size { get; set; }
        /// <summary>
        /// 可渲染的diff文件大小
        /// </summary>
       [JsonProperty("limit_diff_files_size")]
        public int? Limit_diff_files_size { get; set; }
        /// <summary>
        /// diff大小是否超出限制
        /// </summary>
       [JsonProperty("is_overflow")]
        public bool? Is_overflow { get; set; }
        /// <summary>
        /// diff是否过大
        /// </summary>
       [JsonProperty("is_change_to_large")]
        public bool? Is_change_to_large { get; set; }
        /// <summary>
        /// 新增行数
        /// </summary>
       [JsonProperty("added_lines")]
        public int? Added_lines { get; set; }
        /// <summary>
        /// 删除行数
        /// </summary>
       [JsonProperty("removed_lines")]
        public int? Removed_lines { get; set; }
        /// <summary>
        /// 文件diff
        /// </summary>
       [JsonProperty("diffs")]
        public DiffResponseModel Diffs { get; set; }

    }
}