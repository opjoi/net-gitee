using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CommitBaseResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// Commit ID
        /// </summary>
       [JsonProperty("id")]
        public string Id { get; set; }
        /// <summary>
        /// Commit Short ID
        /// </summary>
       [JsonProperty("short_id")]
        public int? Short_id { get; set; }
        /// <summary>
        /// Commit Title
        /// </summary>
       [JsonProperty("title")]
        public int? Title { get; set; }
        /// <summary>
        /// Commit Html Title
        /// </summary>
       [JsonProperty("title_html")]
        public int? Title_html { get; set; }
        /// <summary>
        /// Commit Description
        /// </summary>
       [JsonProperty("description")]
        public int? Description { get; set; }
        /// <summary>
        /// Commit Html Description
        /// </summary>
       [JsonProperty("description_html")]
        public int? Description_html { get; set; }
        /// <summary>
        /// Commit Message
        /// </summary>
       [JsonProperty("message")]
        public int? Message { get; set; }
        /// <summary>
        /// Commit Html Message
        /// </summary>
       [JsonProperty("message_html")]
        public int? Message_html { get; set; }
        /// <summary>
        /// Commit 作者
        /// </summary>
       [JsonProperty("author")]
        public int? Author { get; set; }
        /// <summary>
        /// Commit 提交人
        /// </summary>
       [JsonProperty("committer")]
        public int? Committer { get; set; }
        /// <summary>
        /// 推送时间
        /// </summary>
       [JsonProperty("authored_date")]
        public int? Authored_date { get; set; }
        /// <summary>
        /// 提交时间
        /// </summary>
       [JsonProperty("committed_date")]
        public int? Committed_date { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
       [JsonProperty("signature")]
        public CommitSignatureResponseModel Signature { get; set; }
        /// <summary>
        /// Gitee Go构建状态
        /// </summary>
       [JsonProperty("build_state")]
        public int? Build_state { get; set; }

    }
}