using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取项目下的仓库列表
    /// </summary>
    public partial class ProjectsListResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 仓库ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;
        /// </summary>
       [JsonProperty("public")]
        public int? Public { get; set; }
        /// <summary>
        /// 企业 id
        /// </summary>
       [JsonProperty("enterprise_id")]
        public int? Enterprise_id { get; set; }
        /// <summary>
        /// 仓库创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 仓库更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 是否允许用户创建涉及敏感信息的任务
        /// </summary>
       [JsonProperty("security_hole_enabled")]
        public bool? Security_hole_enabled { get; set; }
        /// <summary>
        /// 仓库挂载的空间
        /// </summary>
       [JsonProperty("namespace")]
        public NamespaceResponseModel Namespace { get; set; }
        /// <summary>
        /// 仓库创建者
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 是否是fork仓库
        /// </summary>
       [JsonProperty("is_fork")]
        public bool? Is_fork { get; set; }
        /// <summary>
        /// 父级仓库
        /// </summary>
       [JsonProperty("parent_project")]
        public ProjectResponseModel Parent_project { get; set; }
        /// <summary>
        /// 状态值
        /// </summary>
       [JsonProperty("status")]
        public int? Status { get; set; }
        /// <summary>
        /// 状态中文名称
        /// </summary>
       [JsonProperty("status_name")]
        public int? Status_name { get; set; }
        /// <summary>
        /// 是否外包
        /// </summary>
       [JsonProperty("outsourced")]
        public bool? Outsourced { get; set; }
        /// <summary>
        /// 仓库大小
        /// </summary>
       [JsonProperty("repo_size")]
        public int? Repo_size { get; set; }
        /// <summary>
        /// 能否操作当前仓库
        /// </summary>
       [JsonProperty("can_admin_project")]
        public bool? Can_admin_project { get; set; }
        /// <summary>
        /// 成员数
        /// </summary>
       [JsonProperty("members_count")]
        public int? Members_count { get; set; }
        /// <summary>
        /// 最近push
        /// </summary>
       [JsonProperty("last_push_at")]
        public bool? Last_push_at { get; set; }
        /// <summary>
        /// watches数
        /// </summary>
       [JsonProperty("watches_count")]
        public int? Watches_count { get; set; }
        /// <summary>
        /// stars数
        /// </summary>
       [JsonProperty("stars_count")]
        public int? Stars_count { get; set; }
        /// <summary>
        /// 被fork数
        /// </summary>
       [JsonProperty("forked_count")]
        public int? Forked_count { get; set; }
        /// <summary>
        /// 是否开启备份
        /// </summary>
       [JsonProperty("enable_backup")]
        public bool? Enable_backup { get; set; }
        /// <summary>
        /// 是否有备份
        /// </summary>
       [JsonProperty("has_backups")]
        public bool? Has_backups { get; set; }
        /// <summary>
        /// 是否vip
        /// </summary>
       [JsonProperty("vip")]
        public bool? Vip { get; set; }
        /// <summary>
        /// 是否推荐
        /// </summary>
       [JsonProperty("recomm")]
        public bool? Recomm { get; set; }
        /// <summary>
        /// 模板仓库基本信息
        /// </summary>
       [JsonProperty("template")]
        public ProjectResponseModel Template { get; set; }
        /// <summary>
        /// 是否为模板仓库
        /// </summary>
       [JsonProperty("template_enabled")]
        public bool? Template_enabled { get; set; }
        /// <summary>
        /// 仓库描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 默认分支
        /// </summary>
       [JsonProperty("get_default_branch")]
        public string Get_default_branch { get; set; }
        /// <summary>
        /// 发行版数
        /// </summary>
       [JsonProperty("releases_count")]
        public int? Releases_count { get; set; }
        /// <summary>
        /// PR数
        /// </summary>
       [JsonProperty("total_pr_count")]
        public int? Total_pr_count { get; set; }
        /// <summary>
        /// 是否收藏
        /// </summary>
       [JsonProperty("is_star")]
        public bool? Is_star { get; set; }
        /// <summary>
        /// 使用此仓库作为模板的仓库总数
        /// </summary>
       [JsonProperty("used_template_count")]
        public int? Used_template_count { get; set; }
        /// <summary>
        /// 是否允许被Fork
        /// </summary>
       [JsonProperty("fork_enabled")]
        public bool? Fork_enabled { get; set; }
        /// <summary>
        /// 是否接受 PR
        /// </summary>
       [JsonProperty("pull_requests_enabled")]
        public bool? Pull_requests_enabled { get; set; }

    }
}