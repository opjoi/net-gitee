using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取企业项目的统计信息
    /// </summary>
    public partial class EnterpriseProgramsListResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 实际完成任务数
        /// </summary>
       [JsonProperty("close_issue_count")]
        public object Close_issue_count { get; set; }
        /// <summary>
        /// 代码行数
        /// </summary>
       [JsonProperty("code_line_count")]
        public object Code_line_count { get; set; }
        /// <summary>
        /// 提交数量
        /// </summary>
       [JsonProperty("commit_count")]
        public object Commit_count { get; set; }
        /// <summary>
        /// 时间列表
        /// </summary>
       [JsonProperty("delay_issue_count")]
        public object Delay_issue_count { get; set; }
        /// <summary>
        /// 按期完成任务数
        /// </summary>
       [JsonProperty("finish_issue_count")]
        public object Finish_issue_count { get; set; }
        /// <summary>
        /// 时间列表
        /// </summary>
       [JsonProperty("date_list")]
        public object Date_list { get; set; }
        /// <summary>
        /// 项目id
        /// </summary>
       [JsonProperty("program_ids")]
        public object Program_ids { get; set; }
        /// <summary>
        /// 项目列表
        /// </summary>
       [JsonProperty("programs")]
        public EnterpriseProgramResponseModel Programs { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
       [JsonProperty("start_date")]
        public DateTime Start_date { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
       [JsonProperty("end_date")]
        public DateTime End_date { get; set; }

    }
}