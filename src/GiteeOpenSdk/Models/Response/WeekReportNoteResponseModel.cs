using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 评论周报
    /// </summary>
    public partial class WeekReportNoteResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 评论的 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 评论类型
        /// </summary>
       [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 评论发起人
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 评论内容(markdown 格式)
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 评论内容(html 格式)
        /// </summary>
       [JsonProperty("content_html")]
        public string Content_html { get; set; }
        /// <summary>
        /// 表态
        /// </summary>
       [JsonProperty("reactions")]
        public ReactionResponseModel Reactions { get; set; }
        /// <summary>
        /// 评论编辑、删除权限
        /// </summary>
       [JsonProperty("operating")]
        public ReactionResponseModel Operating { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}