using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WikiInfoResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 文档 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 文档名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 文档路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 文档创建者
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 文档归属的仓库
        /// </summary>
       [JsonProperty("project")]
        public ProjectResponseModel Project { get; set; }
        /// <summary>
        /// 文档节点
        /// </summary>
       [JsonProperty("doc_node")]
        public DocNodeResponseModel Doc_node { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}