using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 任务类型详情(状态管理)
    /// </summary>
    public partial class IssueTypeWithStateRefResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 任务类型 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 任务类型的名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务类型模板
        /// </summary>
       [JsonProperty("template")]
        public string Template { get; set; }
        /// <summary>
        /// 唯一标识符
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
       [JsonProperty("color")]
        public string Color { get; set; }
        /// <summary>
        /// 是否系统默认类型
        /// </summary>
       [JsonProperty("is_system")]
        public bool? Is_system { get; set; }
        /// <summary>
        /// 任务类型创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务类型更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 类型属性
        /// </summary>
       [JsonProperty("category")]
        public string Category { get; set; }
        /// <summary>
        /// 任务类型描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 关联任务状态
        /// </summary>
       [JsonProperty("issue_states_flow")]
        public string Issue_states_flow { get; set; }

    }
}