using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取 Pull Request或相关提交的评论列表树
    /// </summary>
    public partial class PullRequestNoteTreeResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// Pull Request id
        /// </summary>
       [JsonProperty("pr_id")]
        public int? Pr_id { get; set; }
        /// <summary>
        /// Pull Request的评论数量
        /// </summary>
       [JsonProperty("count")]
        public int? Count { get; set; }
        /// <summary>
        /// Pull Request的操作日志、普通评论、文件行评论包含代码建议的集合
        /// </summary>
       [JsonProperty("list")]
        public object List { get; set; }
        /// <summary>
        /// Pull Request的diff position社区版请求地址
        /// </summary>
       [JsonProperty("diff_position_context_path")]
        public string Diff_position_context_path { get; set; }
        /// <summary>
        /// 是否能关闭Pull Request
        /// </summary>
       [JsonProperty("can_close_pr")]
        public bool? Can_close_pr { get; set; }

    }
}