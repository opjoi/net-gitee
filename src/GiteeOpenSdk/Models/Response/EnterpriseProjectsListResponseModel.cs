using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取企业仓库的统计信息
    /// </summary>
    public partial class EnterpriseProjectsListResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("close_issue_count")]
        public object Close_issue_count { get; set; }
        /// <summary>
        /// 代码行数
        /// </summary>
       [JsonProperty("code_line_count")]
        public object Code_line_count { get; set; }
        /// <summary>
        /// 提交数量
        /// </summary>
       [JsonProperty("commit_count")]
        public object Commit_count { get; set; }
        /// <summary>
        /// 创建任务数
        /// </summary>
       [JsonProperty("create_issue_count")]
        public object Create_issue_count { get; set; }
        /// <summary>
        /// 创建PR数
        /// </summary>
       [JsonProperty("create_pr_count")]
        public object Create_pr_count { get; set; }
        /// <summary>
        /// 日期列表
        /// </summary>
       [JsonProperty("date_list")]
        public object Date_list { get; set; }
        /// <summary>
        /// fork数量
        /// </summary>
       [JsonProperty("fork_count")]
        public object Fork_count { get; set; }
        /// <summary>
        /// 合并PR数
        /// </summary>
       [JsonProperty("merge_pr_count")]
        public object Merge_pr_count { get; set; }
        /// <summary>
        /// 仓库id
        /// </summary>
       [JsonProperty("project_ids")]
        public object Project_ids { get; set; }
        /// <summary>
        /// 仓库列表
        /// </summary>
       [JsonProperty("projects")]
        public EnterpriseProjectResponseModel Projects { get; set; }
        /// <summary>
        /// star数量
        /// </summary>
       [JsonProperty("stars_count")]
        public EnterpriseProjectResponseModel Stars_count { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
       [JsonProperty("start_date")]
        public DateTime Start_date { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
       [JsonProperty("end_date")]
        public DateTime End_date { get; set; }

    }
}