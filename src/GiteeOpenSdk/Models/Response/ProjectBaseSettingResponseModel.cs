using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 更新仓库设置
    /// </summary>
    public partial class ProjectBaseSettingResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 仓库ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// namespace/path
        /// </summary>
       [JsonProperty("path_with_namespace")]
        public string Path_with_namespace { get; set; }
        /// <summary>
        /// 仓库开源属性，0:私有，1:开源，2:内部开源
        /// </summary>
       [JsonProperty("public")]
        public string Public { get; set; }
        /// <summary>
        /// 仓库创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 仓库更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 是否为GVP仓库
        /// </summary>
       [JsonProperty("is_gvp")]
        public bool? Is_gvp { get; set; }
        /// <summary>
        /// 是否为空仓库
        /// </summary>
       [JsonProperty("is_empty_repo")]
        public bool? Is_empty_repo { get; set; }
        /// <summary>
        /// 是否允许仓库被Fork
        /// </summary>
       [JsonProperty("fork_enabled")]
        public bool? Fork_enabled { get; set; }
        /// <summary>
        /// namespace_name/path
        /// </summary>
       [JsonProperty("name_with_namespace")]
        public string Name_with_namespace { get; set; }
        /// <summary>
        /// 仓库介绍
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 仓库主页
        /// </summary>
       [JsonProperty("homepage")]
        public string Homepage { get; set; }
        /// <summary>
        /// 仓库语言
        /// </summary>
       [JsonProperty("lang_id")]
        public string Lang_id { get; set; }
        /// <summary>
        /// 仓库状态(已关闭/开发中/已完结/维护中)
        /// </summary>
       [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// 默认分支
        /// </summary>
       [JsonProperty("default_branch")]
        public string Default_branch { get; set; }
        /// <summary>
        /// 仓库类型，0内部，1外包
        /// </summary>
       [JsonProperty("outsourced")]
        public bool? Outsourced { get; set; }
        /// <summary>
        /// 仓库负责人
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("programs")]
        public ProgramResponseModel Programs { get; set; }
        /// <summary>
        /// 允许用户对仓库进行评论
        /// </summary>
       [JsonProperty("can_comment")]
        public bool? Can_comment { get; set; }
        /// <summary>
        /// 允许用户对&quot;关闭&quot;状态的Issues进行评论
        /// </summary>
       [JsonProperty("issue_comment")]
        public bool? Issue_comment { get; set; }
        /// <summary>
        /// 轻量级的issue跟踪系统
        /// </summary>
       [JsonProperty("issues_enabled")]
        public bool? Issues_enabled { get; set; }
        /// <summary>
        /// 允许用户创建涉及敏感信息的Issue，提交后不公开此Issue（可见范围：仓库成员、企业成员）
        /// </summary>
       [JsonProperty("security_hole_enabled")]
        public bool? Security_hole_enabled { get; set; }
        /// <summary>
        /// 是否允许仓库文件在线编辑
        /// </summary>
       [JsonProperty("online_edit_enabled")]
        public bool? Online_edit_enabled { get; set; }
        /// <summary>
        /// 接受pull request，协作开发
        /// </summary>
       [JsonProperty("pull_requests_enabled")]
        public bool? Pull_requests_enabled { get; set; }
        /// <summary>
        /// 可以编写文档
        /// </summary>
       [JsonProperty("wiki_enabled")]
        public bool? Wiki_enabled { get; set; }
        /// <summary>
        /// 接受轻量级 Pull Request（用户可以发起轻量级 Pull Request 而无需 Fork 仓库）
        /// </summary>
       [JsonProperty("lightweight_pr_enabled")]
        public bool? Lightweight_pr_enabled { get; set; }
        /// <summary>
        /// 开启的Pull Request，仅管理员、审查者、测试者可见
        /// </summary>
       [JsonProperty("pr_master_only")]
        public bool? Pr_master_only { get; set; }
        /// <summary>
        /// 禁止强制推送
        /// </summary>
       [JsonProperty("forbid_force_push")]
        public bool? Forbid_force_push { get; set; }
        /// <summary>
        /// 仓库远程地址(用于强制同步)
        /// </summary>
       [JsonProperty("import_url")]
        public bool? Import_url { get; set; }
        /// <summary>
        /// 禁止仓库同步(禁止从仓库远程地址或Fork的源仓库强制同步代码，禁止后将关闭同步按钮)
        /// </summary>
       [JsonProperty("forbid_force_sync")]
        public bool? Forbid_force_sync { get; set; }
        /// <summary>
        /// 使用SVN管理您的仓库
        /// </summary>
       [JsonProperty("svn_enabled")]
        public bool? Svn_enabled { get; set; }
        /// <summary>
        /// 开启文件/文件夹只读功能(只读文件和SVN支持无法同时启用)
        /// </summary>
       [JsonProperty("can_readonly")]
        public bool? Can_readonly { get; set; }
        /// <summary>
        /// 企业是否禁用SVN
        /// </summary>
       [JsonProperty("enterprise_forbids_svn")]
        public bool? Enterprise_forbids_svn { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("parent")]
        public ProjectResponseModel Parent { get; set; }
        /// <summary>
        /// 是否为模板仓库
        /// </summary>
       [JsonProperty("template_enabled")]
        public bool? Template_enabled { get; set; }
        /// <summary>
        /// 仓库是启用了GiteeGo
        /// </summary>
       [JsonProperty("gitee_go_enabled")]
        public bool? Gitee_go_enabled { get; set; }

    }
}