using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 新增表态
    /// </summary>
    public partial class ReactionResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 表态的类型。(PullRequest、Note、Issue)
        /// </summary>
       [JsonProperty("target_type")]
        public string Target_type { get; set; }
        /// <summary>
        /// 对应表态类型的 id
        /// </summary>
       [JsonProperty("target_id")]
        public int? Target_id { get; set; }
        /// <summary>
        /// 用户的 id
        /// </summary>
       [JsonProperty("user_id")]
        public int? User_id { get; set; }
        /// <summary>
        /// 用户的详情信息
        /// </summary>
       [JsonProperty("user")]
        public UserWithRemarkResponseModel User { get; set; }
        /// <summary>
        /// 表态内容
        /// </summary>
       [JsonProperty("text")]
        public string Text { get; set; }

    }
}