using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ScrumSprintResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 迭代ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 迭代标题
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 企业ID
        /// </summary>
       [JsonProperty("enterprise_id")]
        public int? Enterprise_id { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
       [JsonProperty("program_id")]
        public int? Program_id { get; set; }
        /// <summary>
        /// 迭代状态
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
       [JsonProperty("started_at")]
        public DateTime Started_at { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
       [JsonProperty("finished_at")]
        public DateTime Finished_at { get; set; }
        /// <summary>
        /// 实际结束时间
        /// </summary>
       [JsonProperty("actual_finished_at")]
        public DateTime Actual_finished_at { get; set; }

    }
}