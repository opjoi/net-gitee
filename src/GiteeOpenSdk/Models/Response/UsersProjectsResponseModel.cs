using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取仓库的操作权限
    /// </summary>
    public partial class UsersProjectsResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 用户 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 用户个性地址
        /// </summary>
       [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 用户头像
        /// </summary>
       [JsonProperty("avatar_url")]
        public string Avatar_url { get; set; }
        /// <summary>
        /// 账号是否可用(active: 可用 blocked: 已被系统屏蔽)
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 最近活跃时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 用户代码风格
        /// </summary>
       [JsonProperty("user_color")]
        public int? User_color { get; set; }
        /// <summary>
        /// 用户对仓库的权限(报告者:15; 观察者:25; 开发者:30; 管理员: 40)
        /// </summary>
       [JsonProperty("access_level")]
        public int? Access_level { get; set; }
        /// <summary>
        /// 仓库角色名称
        /// </summary>
       [JsonProperty("access_name")]
        public string Access_name { get; set; }
        /// <summary>
        /// 加入仓库的时间
        /// </summary>
       [JsonProperty("join_at")]
        public DateTime Join_at { get; set; }
        /// <summary>
        /// 用户在企业的备注名
        /// </summary>
       [JsonProperty("remark")]
        public string Remark { get; set; }
        /// <summary>
        /// 成员备注或名称拼音
        /// </summary>
       [JsonProperty("pinyin")]
        public string Pinyin { get; set; }

    }
}