using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 修改部署公钥
    /// </summary>
    public partial class EnterpriseDeployKeyResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 部署公钥 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 部署公钥 title
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 部署公钥内容
        /// </summary>
       [JsonProperty("key")]
        public string Key { get; set; }
        /// <summary>
        /// sha256 指纹
        /// </summary>
       [JsonProperty("sha256_fingerprint")]
        public string Sha256_fingerprint { get; set; }
        /// <summary>
        /// 公钥的创建者
        /// </summary>
       [JsonProperty("user")]
        public UserWithRemarkResponseModel User { get; set; }
        /// <summary>
        /// 已部署仓库数量
        /// </summary>
       [JsonProperty("projects_count")]
        public int? Projects_count { get; set; }
        /// <summary>
        /// 已部署的仓库
        /// </summary>
       [JsonProperty("projects")]
        public ProjectResponseModel Projects { get; set; }
        /// <summary>
        /// 是否是系统公钥
        /// </summary>
       [JsonProperty("system")]
        public bool? System { get; set; }
        /// <summary>
        /// 是否是bae公钥
        /// </summary>
       [JsonProperty("is_bae")]
        public bool? Is_bae { get; set; }
        /// <summary>
        /// 是否是pages公钥
        /// </summary>
       [JsonProperty("is_pages")]
        public bool? Is_pages { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }

    }
}