using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取 Pull Request 操作日志
    /// </summary>
    public partial class PrOperateLogResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
       [JsonProperty("icon")]
        public string Icon { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 操作者
        /// </summary>
       [JsonProperty("user")]
        public UserWithRemarkResponseModel User { get; set; }

    }
}