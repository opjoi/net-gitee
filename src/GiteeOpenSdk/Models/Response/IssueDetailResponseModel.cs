using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 更新任务的关联关系
    /// </summary>
    public partial class IssueDetailResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 任务 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 根结点 ID
        /// </summary>
       [JsonProperty("root_id")]
        public int? Root_id { get; set; }
        /// <summary>
        /// 父任务 ID
        /// </summary>
       [JsonProperty("parent_id")]
        public int? Parent_id { get; set; }
        /// <summary>
        /// 任务全局唯一标识符
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 任务标题
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务状态标识符: open, progressing, closed, rejected
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 评论数量
        /// </summary>
       [JsonProperty("comments_count")]
        public int? Comments_count { get; set; }
        /// <summary>
        /// 优先级标识符
        /// </summary>
       [JsonProperty("priority")]
        public int? Priority { get; set; }
        /// <summary>
        /// 关联的分支名
        /// </summary>
       [JsonProperty("branch")]
        public string Branch { get; set; }
        /// <summary>
        /// 优先级中文名称
        /// </summary>
       [JsonProperty("priority_human")]
        public string Priority_human { get; set; }
        /// <summary>
        /// 任务负责人
        /// </summary>
       [JsonProperty("assignee")]
        public string Assignee { get; set; }
        /// <summary>
        /// 预计工时。（单位：分钟）
        /// </summary>
       [JsonProperty("duration")]
        public int? Duration { get; set; }
        /// <summary>
        /// 任务创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 任务协作者
        /// </summary>
       [JsonProperty("collaborators")]
        public object Collaborators { get; set; }
        /// <summary>
        /// 任务创建者
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 关联的里程碑
        /// </summary>
       [JsonProperty("milestone")]
        public MilestoneResponseModel Milestone { get; set; }
        /// <summary>
        /// 任务状态
        /// </summary>
       [JsonProperty("issue_state")]
        public IssueStateResponseModel Issue_state { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
       [JsonProperty("issue_type")]
        public IssueTypeResponseModel Issue_type { get; set; }
        /// <summary>
        /// 任务关联的标签
        /// </summary>
       [JsonProperty("labels")]
        public List<LabelResponseModel> Labels { get; set; }
        /// <summary>
        /// 任务自定义字段值
        /// </summary>
       [JsonProperty("issue_extra")]
        public List<LabelResponseModel> Issue_extra { get; set; }
        /// <summary>
        /// 计划开始时间
        /// </summary>
       [JsonProperty("plan_started_at")]
        public DateTime? Plan_started_at { get; set; }
        /// <summary>
        /// 计划完成时间
        /// </summary>
       [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        /// <summary>
        /// 完成时间
        /// </summary>
       [JsonProperty("finished_at")]
        public DateTime Finished_at { get; set; }
        /// <summary>
        /// 是否是私有Issue
        /// </summary>
       [JsonProperty("security_hole")]
        public bool? Security_hole { get; set; }
        /// <summary>
        /// 当前用户是否收藏过此任务
        /// </summary>
       [JsonProperty("is_star")]
        public bool? Is_star { get; set; }
        /// <summary>
        /// 所属看板
        /// </summary>
       [JsonProperty("kanban_info")]
        public bool? Kanban_info { get; set; }
        /// <summary>
        /// 任务内容(markdown 格式)
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 任务内容(html 格式)
        /// </summary>
       [JsonProperty("description_html")]
        public string Description_html { get; set; }
        /// <summary>
        /// PC的任务详情链接
        /// </summary>
       [JsonProperty("issue_url")]
        public string Issue_url { get; set; }
        /// <summary>
        /// 关联的项目
        /// </summary>
       [JsonProperty("program")]
        public ProgramResponseModel Program { get; set; }
        /// <summary>
        /// 关联的仓库
        /// </summary>
       [JsonProperty("project")]
        public ProjectResponseModel Project { get; set; }
        /// <summary>
        /// 父级任务
        /// </summary>
       [JsonProperty("parent")]
        public IssueResponseModel Parent { get; set; }
        /// <summary>
        /// 子任务
        /// </summary>
       [JsonProperty("children")]
        public IssueResponseModel Children { get; set; }
        /// <summary>
        /// 子任务数量
        /// </summary>
       [JsonProperty("children_count")]
        public int? Children_count { get; set; }
        /// <summary>
        /// 操作日志的数量
        /// </summary>
       [JsonProperty("operate_logs_count")]
        public int? Operate_logs_count { get; set; }
        /// <summary>
        /// 表态
        /// </summary>
       [JsonProperty("reactions")]
        public object Reactions { get; set; }
        /// <summary>
        /// issue附件id列表
        /// </summary>
       [JsonProperty("attach_file_ids")]
        public int? Attach_file_ids { get; set; }
        /// <summary>
        /// 私有issue操作者
        /// </summary>
       [JsonProperty("security_hole_operater")]
        public bool? Security_hole_operater { get; set; }
        /// <summary>
        /// 关联迭代
        /// </summary>
       [JsonProperty("scrum_sprint")]
        public ScrumSprintResponseModel Scrum_sprint { get; set; }
        /// <summary>
        /// 关联版本
        /// </summary>
       [JsonProperty("scrum_version")]
        public ScrumVersionResponseModel Scrum_version { get; set; }
        /// <summary>
        /// 收藏了该任务的用户列表
        /// </summary>
       [JsonProperty("starred_users")]
        public ScrumVersionResponseModel Starred_users { get; set; }
        /// <summary>
        /// 是否有异步执行的触发器
        /// </summary>
       [JsonProperty("trigger_execute_info")]
        public bool? Trigger_execute_info { get; set; }

    }
}