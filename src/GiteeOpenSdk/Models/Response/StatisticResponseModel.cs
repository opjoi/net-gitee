using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class StatisticResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 查看统计
        /// </summary>
       [JsonProperty("read")]
        public object Read { get; set; }

    }
}