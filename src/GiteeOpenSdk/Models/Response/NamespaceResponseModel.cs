using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class NamespaceResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 空间 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 空间类型(Enterprise: 企业; Group: 组织; 其它情况：个人)
        /// </summary>
       [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 空间名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 空间路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }

    }
}