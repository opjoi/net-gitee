using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DiffResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// Commit ID
        /// </summary>
       [JsonProperty("sha")]
        public string Sha { get; set; }
        /// <summary>
        /// 文件路径 sha 值
        /// </summary>
       [JsonProperty("file_path_sha")]
        public string File_path_sha { get; set; }
        /// <summary>
        /// 文件名
        /// </summary>
       [JsonProperty("filename")]
        public string Filename { get; set; }
        /// <summary>
        /// 文件改动类型。added: 新增 renamed: 重命名 deleted: 删除
        /// </summary>
       [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// 新增代码行数
        /// </summary>
       [JsonProperty("additions")]
        public int? Additions { get; set; }
        /// <summary>
        /// 删除代码行数
        /// </summary>
       [JsonProperty("deletions")]
        public int? Deletions { get; set; }
        /// <summary>
        /// diff文件代码行数
        /// </summary>
       [JsonProperty("diff_file_loc")]
        public int? Diff_file_loc { get; set; }
        /// <summary>
        /// diff 统计
        /// </summary>
       [JsonProperty("statistic")]
        public string Statistic { get; set; }
        /// <summary>
        /// diff head
        /// </summary>
       [JsonProperty("head")]
        public string Head { get; set; }
        /// <summary>
        /// diff 内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// diff内容
        /// </summary>
       [JsonProperty("patch")]
        public string Patch { get; set; }

    }
}