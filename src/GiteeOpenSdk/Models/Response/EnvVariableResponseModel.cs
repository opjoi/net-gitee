using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 修改环境变量
    /// </summary>
    public partial class EnvVariableResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 环境变量id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库id
        /// </summary>
       [JsonProperty("project_id")]
        public int? Project_id { get; set; }
        /// <summary>
        /// 环境变量名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 环境变量值
        /// </summary>
       [JsonProperty("value")]
        public string Value { get; set; }
        /// <summary>
        /// 环境变量备注
        /// </summary>
       [JsonProperty("remark")]
        public string Remark { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
       [JsonProperty("updated_at")]
        public string Updated_at { get; set; }
        /// <summary>
        /// 是否只读,1:true,0:false
        /// </summary>
       [JsonProperty("read_only")]
        public int? Read_only { get; set; }
        /// <summary>
        /// 是否密钥,1:true,0:false
        /// </summary>
       [JsonProperty("is_secret")]
        public int? Is_secret { get; set; }

    }
}