using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取企业下的 Pull Request 列表
    /// </summary>
    public partial class PullRequestResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// PR 的 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 仓库内唯一的 PR id 标识符
        /// </summary>
       [JsonProperty("iid")]
        public int? Iid { get; set; }
        /// <summary>
        /// PR 的标题
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 仓库 id
        /// </summary>
       [JsonProperty("project_id")]
        public int? Project_id { get; set; }
        /// <summary>
        /// PR 的状态(opened: 开启; reopened: 关闭后重开; closed: 关闭; merged: 已合并;)
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// PR 草稿状态： 草稿 - true, 非草稿 - false
        /// </summary>
       [JsonProperty("draft")]
        public bool? Draft { get; set; }
        /// <summary>
        /// PR的审查状态(0: 不需要审查; 1: 待审查; 2: 审查已全部通过;)
        /// </summary>
       [JsonProperty("check_state")]
        public int? Check_state { get; set; }
        /// <summary>
        /// PR的测试状态(0: 不需要测试; 1: 待测试; 2: 测试已全部通过;)
        /// </summary>
       [JsonProperty("test_state")]
        public int? Test_state { get; set; }
        /// <summary>
        /// PR 的优先级。(0: 不指定; 1: 不重要; 2: 次要; 3: 主要; 4: 严重;)
        /// </summary>
       [JsonProperty("priority")]
        public string Priority { get; set; }
        /// <summary>
        /// 是否轻量级 PR
        /// </summary>
       [JsonProperty("lightweight")]
        public bool? Lightweight { get; set; }
        /// <summary>
        /// PR 的创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// PR 的更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// PR 的合并时间
        /// </summary>
       [JsonProperty("merged_at")]
        public DateTime Merged_at { get; set; }
        /// <summary>
        /// PR 的标签列表
        /// </summary>
       [JsonProperty("labels")]
        public LabelResponseModel Labels { get; set; }
        /// <summary>
        /// PR 创建者
        /// </summary>
       [JsonProperty("author")]
        public LabelResponseModel Author { get; set; }
        /// <summary>
        /// 是否存在冲突
        /// </summary>
       [JsonProperty("conflict")]
        public bool? Conflict { get; set; }
        /// <summary>
        /// 所属仓库
        /// </summary>
       [JsonProperty("project")]
        public ProjectResponseModel Project { get; set; }
        /// <summary>
        /// 源分支
        /// </summary>
       [JsonProperty("source_branch")]
        public object Source_branch { get; set; }
        /// <summary>
        /// 目标分支
        /// </summary>
       [JsonProperty("target_branch")]
        public object Target_branch { get; set; }
        /// <summary>
        /// 是否可合并
        /// </summary>
       [JsonProperty("can_merge")]
        public string Can_merge { get; set; }
        /// <summary>
        /// 审查人员
        /// </summary>
       [JsonProperty("assignees")]
        public PrAssignResponseModel Assignees { get; set; }
        /// <summary>
        /// 最少审查人数
        /// </summary>
       [JsonProperty("pr_assign_num")]
        public int? Pr_assign_num { get; set; }
        /// <summary>
        /// 测试人员
        /// </summary>
       [JsonProperty("testers")]
        public PrAssignResponseModel Testers { get; set; }
        /// <summary>
        /// 合并 PR 后关闭关联的任务
        /// </summary>
       [JsonProperty("close_related_issue")]
        public int? Close_related_issue { get; set; }
        /// <summary>
        /// 合并 PR 后删除关联分支
        /// </summary>
       [JsonProperty("prune_branch")]
        public int? Prune_branch { get; set; }
        /// <summary>
        /// 最少测试人数
        /// </summary>
       [JsonProperty("pr_test_num")]
        public int? Pr_test_num { get; set; }
        /// <summary>
        /// 最后一次gitee scan扫描结果
        /// </summary>
       [JsonProperty("latest_scan_task")]
        public object Latest_scan_task { get; set; }
        /// <summary>
        /// 所属仓库GiteeGo服务是否可用
        /// </summary>
       [JsonProperty("gitee_go_enabled")]
        public bool? Gitee_go_enabled { get; set; }

    }
}