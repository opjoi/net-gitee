using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class IssueMainResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 任务 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 根结点 ID
        /// </summary>
       [JsonProperty("root_id")]
        public int? Root_id { get; set; }
        /// <summary>
        /// 任务全局唯一标识符
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 任务标题
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务状态标识符: open, progressing, closed, rejected
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 评论数量
        /// </summary>
       [JsonProperty("comments_count")]
        public int? Comments_count { get; set; }
        /// <summary>
        /// 优先级标识符
        /// </summary>
       [JsonProperty("priority")]
        public int? Priority { get; set; }
        /// <summary>
        /// 优先级中文名称
        /// </summary>
       [JsonProperty("priority_human")]
        public string Priority_human { get; set; }
        /// <summary>
        /// 任务负责人
        /// </summary>
       [JsonProperty("assignee")]
        public string Assignee { get; set; }
        /// <summary>
        /// 预计工时。（单位：分钟）
        /// </summary>
       [JsonProperty("duration")]
        public int? Duration { get; set; }
        /// <summary>
        /// 任务创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 任务完成时间
        /// </summary>
       [JsonProperty("finished_at")]
        public DateTime Finished_at { get; set; }
        /// <summary>
        /// 计划开始时间
        /// </summary>
       [JsonProperty("plan_started_at")]
        public DateTime Plan_started_at { get; set; }
        /// <summary>
        /// 截止时间
        /// </summary>
       [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        /// <summary>
        /// 是否过期
        /// </summary>
       [JsonProperty("is_overdue")]
        public bool? Is_overdue { get; set; }

    }
}