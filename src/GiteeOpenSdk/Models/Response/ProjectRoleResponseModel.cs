using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取仓库角色
    /// </summary>
    public partial class ProjectRoleResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 角色
        /// </summary>
       [JsonProperty("role")]
        public string Role { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
       [JsonProperty("access")]
        public string Access { get; set; }
        /// <summary>
        /// 角色等级
        /// </summary>
       [JsonProperty("role_level")]
        public string Role_level { get; set; }

    }
}