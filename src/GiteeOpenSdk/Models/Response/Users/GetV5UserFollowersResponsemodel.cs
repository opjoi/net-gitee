﻿using System;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 列出授权用户的关注者返回模型
    /// </summary>
    public class GetV5UserFollowersResponseModel : UserModel
    {

    }
}
