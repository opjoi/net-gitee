﻿using System;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 列出授权用户正关注的用户返回模型
    /// </summary>
    public class GetV5UserFollowingResponseModel : UserModel
    {

    }
}
