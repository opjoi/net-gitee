﻿using System;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 列出指定用户正在关注的用户
    /// </summary>
    public class GetV5UsersUsernameFollowingResponseModel : UserModel
    {

    }
}
