using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 更新任务状态
    /// </summary>
    public partial class IssueStateResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 任务状态 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 任务状态的名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务状态的颜色
        /// </summary>
       [JsonProperty("color")]
        public string Color { get; set; }
        /// <summary>
        /// 任务状态的 Icon
        /// </summary>
       [JsonProperty("icon")]
        public string Icon { get; set; }
        /// <summary>
        /// 任务状态的 指令
        /// </summary>
       [JsonProperty("command")]
        public string Command { get; set; }
        /// <summary>
        /// 任务状态的 排序
        /// </summary>
       [JsonProperty("serial")]
        public string Serial { get; set; }
        /// <summary>
        /// 关联该任务状态的任务类型
        /// </summary>
       [JsonProperty("issue_types")]
        public List<IssueTypeResponseModel> Issue_types { get; set; }
        /// <summary>
        /// 任务状态创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务状态更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}