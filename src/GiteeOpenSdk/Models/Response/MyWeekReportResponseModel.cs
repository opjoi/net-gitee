using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取未提交周报的用户列表
    /// </summary>
    public partial class MyWeekReportResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 周报的 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 周报所属年
        /// </summary>
       [JsonProperty("year")]
        public int? Year { get; set; }
        /// <summary>
        /// 周报所属月份
        /// </summary>
       [JsonProperty("month")]
        public int? Month { get; set; }
        /// <summary>
        /// 处于本年的第几周
        /// </summary>
       [JsonProperty("week_index")]
        public int? Week_index { get; set; }
        /// <summary>
        /// 起始日期
        /// </summary>
       [JsonProperty("begin_day")]
        public DateTime Begin_day { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
       [JsonProperty("end_day")]
        public DateTime End_day { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 周报内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 周报(html 格式)
        /// </summary>
       [JsonProperty("content_html")]
        public string Content_html { get; set; }

    }
}