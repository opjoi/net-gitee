using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 创建 Cherry Pick
    /// </summary>
    public partial class CherryPickResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 自动创建的 cherry pick 分支名
        /// </summary>
       [JsonProperty("cherry_branch")]
        public string Cherry_branch { get; set; }
        /// <summary>
        /// 创建状态
        /// </summary>
       [JsonProperty("status")]
        public string Status { get; set; }

    }
}