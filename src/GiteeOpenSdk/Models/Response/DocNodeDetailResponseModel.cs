using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 查看文件节点详情
    /// </summary>
    public partial class DocNodeDetailResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 父层级的 id
        /// </summary>
       [JsonProperty("parent_id")]
        public int? Parent_id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 权限值
        /// </summary>
       [JsonProperty("public")]
        public string Public { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
       [JsonProperty("public_name")]
        public string Public_name { get; set; }
        /// <summary>
        /// 关联项目
        /// </summary>
       [JsonProperty("program")]
        public ProgramResponseModel Program { get; set; }
        /// <summary>
        /// 关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile)
        /// </summary>
       [JsonProperty("file_type")]
        public string File_type { get; set; }
        /// <summary>
        /// 关联类型的 id
        /// </summary>
       [JsonProperty("file_id")]
        public string File_id { get; set; }
        /// <summary>
        /// 是否查看附件历史版本
        /// </summary>
       [JsonProperty("file_versions")]
        public bool? File_versions { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 是否已收藏
        /// </summary>
       [JsonProperty("is_favour")]
        public bool? Is_favour { get; set; }
        /// <summary>
        /// 附件相关信息
        /// </summary>
       [JsonProperty("attach_file")]
        public DocAttachFileResponseModel Attach_file { get; set; }
        /// <summary>
        /// 是否 wiki
        /// </summary>
       [JsonProperty("is_wiki")]
        public bool? Is_wiki { get; set; }
        /// <summary>
        /// 需要密码访问
        /// </summary>
       [JsonProperty("need_password")]
        public bool? Need_password { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
       [JsonProperty("is_top")]
        public bool? Is_top { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
       [JsonProperty("deleted_at")]
        public DateTime Deleted_at { get; set; }
        /// <summary>
        /// 层级目录
        /// </summary>
       [JsonProperty("breadcrumb")]
        public string Breadcrumb { get; set; }
        /// <summary>
        /// 预览token
        /// </summary>
       [JsonProperty("preview_token")]
        public string Preview_token { get; set; }

    }
}