using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取管理界面的字段列表
    /// </summary>
    public partial class IssueFieldResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 字段 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 字段名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 字段类型
        /// </summary>
       [JsonProperty("field_type")]
        public string Field_type { get; set; }
        /// <summary>
        /// 字段可选值
        /// </summary>
       [JsonProperty("options")]
        public string Options { get; set; }
        /// <summary>
        /// 是否为系统字段
        /// </summary>
       [JsonProperty("is_system")]
        public bool? Is_system { get; set; }
        /// <summary>
        /// 系统字段英文名
        /// </summary>
       [JsonProperty("system_name")]
        public string System_name { get; set; }
        /// <summary>
        /// 字段描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }

    }
}