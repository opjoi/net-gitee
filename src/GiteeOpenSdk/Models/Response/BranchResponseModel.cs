using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 编辑分支状态
    /// </summary>
    public partial class BranchResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 分支名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 分支类型
        /// </summary>
       [JsonProperty("branch_type")]
        public object Branch_type { get; set; }
        /// <summary>
        /// 是否默认分支
        /// </summary>
       [JsonProperty("is_default")]
        public bool? Is_default { get; set; }
        /// <summary>
        /// 保护分支的操作规则
        /// </summary>
       [JsonProperty("protection_rule")]
        public object Protection_rule { get; set; }
        /// <summary>
        /// commit信息
        /// </summary>
       [JsonProperty("commit")]
        public object Commit { get; set; }
        /// <summary>
        /// 操作相关权限
        /// </summary>
       [JsonProperty("operating")]
        public object Operating { get; set; }

    }
}