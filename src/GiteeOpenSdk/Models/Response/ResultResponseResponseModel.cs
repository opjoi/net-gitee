using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 新建仓库-导入仓库参数是否有效
    /// </summary>
    public partial class ResultResponseResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// private: 私有, success: 成功, duplicate: 仓库已经导入(仓库信息：project)
        /// </summary>
       [JsonProperty("result")]
        public string Result { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
       [JsonProperty("message")]
        public string Message { get; set; }

    }
}