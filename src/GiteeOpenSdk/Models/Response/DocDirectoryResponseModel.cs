using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取文件夹
    /// </summary>
    public partial class DocDirectoryResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 父层级的 id
        /// </summary>
       [JsonProperty("parent_id")]
        public int? Parent_id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 权限值
        /// </summary>
       [JsonProperty("public")]
        public string Public { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
       [JsonProperty("public_name")]
        public string Public_name { get; set; }
        /// <summary>
        /// 关联项目
        /// </summary>
       [JsonProperty("program")]
        public ProgramResponseModel Program { get; set; }
        /// <summary>
        /// 关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile)
        /// </summary>
       [JsonProperty("file_type")]
        public string File_type { get; set; }
        /// <summary>
        /// 关联类型的 id
        /// </summary>
       [JsonProperty("file_id")]
        public string File_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("children")]
        public string Children { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}