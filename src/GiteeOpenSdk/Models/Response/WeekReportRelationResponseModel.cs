using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取周报可关联issue和pull request
    /// </summary>
    public partial class WeekReportRelationResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 关联Pull request
        /// </summary>
       [JsonProperty("issues")]
        public IssueMainResponseModel Issues { get; set; }
        /// <summary>
        /// 关联Issues
        /// </summary>
       [JsonProperty("pull_requests")]
        public PullRequestResponseModel Pull_requests { get; set; }

    }
}