using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 更新企业公告内容
    /// </summary>
    public partial class EnterpriseResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 企业 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 企业名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 企业路径
        /// </summary>
       [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 企业简介
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 企业类型(0: 未公开 1: 公开)
        /// </summary>
       [JsonProperty("public")]
        public string Public { get; set; }
        /// <summary>
        /// 企业官网
        /// </summary>
       [JsonProperty("website")]
        public string Website { get; set; }
        /// <summary>
        /// 企业邮箱
        /// </summary>
       [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 企业手机号码
        /// </summary>
       [JsonProperty("phone")]
        public string Phone { get; set; }
        /// <summary>
        /// 企业头像
        /// </summary>
       [JsonProperty("avatar_url")]
        public string Avatar_url { get; set; }
        /// <summary>
        /// 企业套餐所属级别
        /// </summary>
       [JsonProperty("level")]
        public int? Level { get; set; }
        /// <summary>
        /// 企业套餐名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 企业拥有者
        /// </summary>
       [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 企业公告
        /// </summary>
       [JsonProperty("notice")]
        public string Notice { get; set; }
        /// <summary>
        /// 企业欢迎私信内容
        /// </summary>
       [JsonProperty("welcome_message")]
        public string Welcome_message { get; set; }
        /// <summary>
        /// 是否开启强制审核
        /// </summary>
       [JsonProperty("force_verify")]
        public bool? Force_verify { get; set; }
        /// <summary>
        /// 企业默认角色ID
        /// </summary>
       [JsonProperty("default_role_id")]
        public int? Default_role_id { get; set; }
        /// <summary>
        /// 是否允许外部成员申请加入
        /// </summary>
       [JsonProperty("open_application")]
        public int? Open_application { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("version")]
        public int? Version { get; set; }
        /// <summary>
        /// 套餐开始时间
        /// </summary>
       [JsonProperty("start_at")]
        public DateTime Start_at { get; set; }
        /// <summary>
        /// 套餐结束时间（免费版企业为空）
        /// </summary>
       [JsonProperty("stop_at")]
        public DateTime Stop_at { get; set; }
        /// <summary>
        /// 是否过期企业
        /// </summary>
       [JsonProperty("overdue")]
        public bool? Overdue { get; set; }
        /// <summary>
        /// 是否付费企业
        /// </summary>
       [JsonProperty("charged")]
        public bool? Charged { get; set; }
        /// <summary>
        /// 企业创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 上次信息变更时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 是否开启水印
        /// </summary>
       [JsonProperty("watermark")]
        public bool? Watermark { get; set; }
        /// <summary>
        /// 是否开启 Gitee Search, not_open、opening、success、failed
        /// </summary>
       [JsonProperty("gitee_search_status")]
        public string Gitee_search_status { get; set; }

    }
}