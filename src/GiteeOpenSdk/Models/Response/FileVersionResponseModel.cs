using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 查看历史版本
    /// </summary>
    public partial class FileVersionResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 版本 id
        /// </summary>
       [JsonProperty("id")]
        public string Id { get; set; }
        /// <summary>
        /// 提交信息
        /// </summary>
       [JsonProperty("message")]
        public string Message { get; set; }
        /// <summary>
        /// 提交日期
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 提交日期
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }

    }
}