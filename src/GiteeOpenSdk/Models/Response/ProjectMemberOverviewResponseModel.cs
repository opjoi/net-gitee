using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 成员概览
    /// </summary>
    public partial class ProjectMemberOverviewResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 观察者数量
        /// </summary>
       [JsonProperty("viewer")]
        public int? Viewer { get; set; }
        /// <summary>
        /// 报告者数量
        /// </summary>
       [JsonProperty("reporter")]
        public int? Reporter { get; set; }
        /// <summary>
        /// 开发者数量
        /// </summary>
       [JsonProperty("developer")]
        public int? Developer { get; set; }
        /// <summary>
        /// 管理者数量
        /// </summary>
       [JsonProperty("master")]
        public int? Master { get; set; }
        /// <summary>
        /// 所有成员数量
        /// </summary>
       [JsonProperty("all")]
        public int? All { get; set; }
        /// <summary>
        /// 待处理成员申请数量
        /// </summary>
       [JsonProperty("padding_apply_count")]
        public int? Padding_apply_count { get; set; }
        /// <summary>
        /// 允许外部成员申请加
        /// </summary>
       [JsonProperty("allow_external_apply")]
        public bool? Allow_external_apply { get; set; }

    }
}