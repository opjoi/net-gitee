using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DocAttachFileResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 下载链接
        /// </summary>
       [JsonProperty("download_url")]
        public string Download_url { get; set; }
        /// <summary>
        /// 预览地址
        /// </summary>
       [JsonProperty("preview_path")]
        public string Preview_path { get; set; }
        /// <summary>
        /// 文件类型
        /// </summary>
       [JsonProperty("file_type")]
        public string File_type { get; set; }
        /// <summary>
        /// 文件大小
        /// </summary>
       [JsonProperty("size")]
        public string Size { get; set; }
        /// <summary>
        /// 文件后缀
        /// </summary>
       [JsonProperty("ext")]
        public string Ext { get; set; }
        /// <summary>
        /// 是否仓库内的附件
        /// </summary>
       [JsonProperty("belong_project")]
        public bool? Belong_project { get; set; }
        /// <summary>
        /// 关联分支
        /// </summary>
       [JsonProperty("branch")]
        public string Branch { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 下载次数
        /// </summary>
       [JsonProperty("download_count")]
        public int? Download_count { get; set; }

    }
}