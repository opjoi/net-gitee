using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 新建标签
    /// </summary>
    public partial class TagResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 标签名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 标签信息
        /// </summary>
       [JsonProperty("message")]
        public string Message { get; set; }
        /// <summary>
        /// 标签对应 commit
        /// </summary>
       [JsonProperty("commit")]
        public object Commit { get; set; }
        /// <summary>
        /// 操作权限
        /// </summary>
       [JsonProperty("operating")]
        public object Operating { get; set; }

    }
}