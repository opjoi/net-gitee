using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 对比commit
    /// </summary>
    public partial class CommitCompareResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// commits 数量超出限制
        /// </summary>
       [JsonProperty("too_more_commits")]
        public int? Too_more_commits { get; set; }
        /// <summary>
        /// commits 数量
        /// </summary>
       [JsonProperty("commits_size")]
        public int? Commits_size { get; set; }
        /// <summary>
        /// commits 数量上限
        /// </summary>
       [JsonProperty("commits_limit_size")]
        public int? Commits_limit_size { get; set; }
        /// <summary>
        /// diff差异文件数量
        /// </summary>
       [JsonProperty("diffs_size")]
        public string Diffs_size { get; set; }
        /// <summary>
        /// commits 列表
        /// </summary>
       [JsonProperty("commits")]
        public CommitBaseResponseModel Commits { get; set; }
        /// <summary>
        /// diff内容
        /// </summary>
       [JsonProperty("diffs")]
        public DiffResponseModel Diffs { get; set; }

    }
}