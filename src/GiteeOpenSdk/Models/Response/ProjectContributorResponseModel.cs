using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取仓库贡献者列表
    /// </summary>
    public partial class ProjectContributorResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 姓名
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 贡献者名称
        /// </summary>
       [JsonProperty("committer_name")]
        public string Committer_name { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
       [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
       [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 是否为Gitee用户
        /// </summary>
       [JsonProperty("is_gitee_user")]
        public bool? Is_gitee_user { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
       [JsonProperty("image_path")]
        public string Image_path { get; set; }
        /// <summary>
        /// commit数
        /// </summary>
       [JsonProperty("commits_count")]
        public int? Commits_count { get; set; }
        /// <summary>
        /// 企业备注
        /// </summary>
       [JsonProperty("enterprise_remark")]
        public string Enterprise_remark { get; set; }

    }
}