using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取用户最近浏览的项目集合
    /// </summary>
    public partial class ProgramListResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 项目 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 项目编号
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 项目描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 项目状态（0:开始 1:暂停 2:关闭）
        /// </summary>
       [JsonProperty("status")]
        public int? Status { get; set; }
        /// <summary>
        /// 是否外包项目
        /// </summary>
       [JsonProperty("outsourced")]
        public bool? Outsourced { get; set; }
        /// <summary>
        /// 项目类型（内部、外包）
        /// </summary>
       [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
       [JsonProperty("color")]
        public string Color { get; set; }
        /// <summary>
        /// 项目类型
        /// </summary>
       [JsonProperty("category")]
        public string Category { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
       [JsonProperty("assignee")]
        public string Assignee { get; set; }
        /// <summary>
        /// 是否置顶项目
        /// </summary>
       [JsonProperty("is_topped")]
        public bool? Is_topped { get; set; }
        /// <summary>
        /// 成员数
        /// </summary>
       [JsonProperty("users_count")]
        public int? Users_count { get; set; }
        /// <summary>
        /// 仓库数
        /// </summary>
       [JsonProperty("projects_count")]
        public int? Projects_count { get; set; }
        /// <summary>
        /// 任务总数
        /// </summary>
       [JsonProperty("issues_count")]
        public int? Issues_count { get; set; }
        /// <summary>
        /// 已关闭的任务总数
        /// </summary>
       [JsonProperty("closed_issues_count")]
        public int? Closed_issues_count { get; set; }
        /// <summary>
        /// 是否被当前用户收藏
        /// </summary>
       [JsonProperty("star_status")]
        public bool? Star_status { get; set; }

    }
}