using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取授权用户对任务的权限
    /// </summary>
    public partial class IssueAuthResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 是否可查看此任务
        /// </summary>
       [JsonProperty("read")]
        public bool? Read { get; set; }
        /// <summary>
        /// 是否可更新此任务的属性等状态
        /// </summary>
       [JsonProperty("update")]
        public bool? Update { get; set; }
        /// <summary>
        /// 是否可更新此任务的标题或内容
        /// </summary>
       [JsonProperty("update_main")]
        public bool? Update_main { get; set; }
        /// <summary>
        /// 是否可删除此任务
        /// </summary>
       [JsonProperty("destroy")]
        public bool? Destroy { get; set; }
        /// <summary>
        /// 是否可评论此任务
        /// </summary>
       [JsonProperty("create_note")]
        public bool? Create_note { get; set; }
        /// <summary>
        /// 是否可创建子任务，关联任务等
        /// </summary>
       [JsonProperty("create_issue")]
        public bool? Create_issue { get; set; }

    }
}