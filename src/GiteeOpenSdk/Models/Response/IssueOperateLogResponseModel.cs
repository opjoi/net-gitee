using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取任务的操作日志列表
    /// </summary>
    public partial class IssueOperateLogResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
       [JsonProperty("icon")]
        public string Icon { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 操作者
        /// </summary>
       [JsonProperty("user")]
        public UserWithRemarkResponseModel User { get; set; }
        /// <summary>
        /// 改变前的值
        /// </summary>
       [JsonProperty("before_change")]
        public string Before_change { get; set; }
        /// <summary>
        /// 改变后的值
        /// </summary>
       [JsonProperty("after_change")]
        public string After_change { get; set; }

    }
}