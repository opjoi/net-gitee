using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class WeekReportResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 与我相关
        /// </summary>
       [JsonProperty("general")]
        public object General { get; set; }
        /// <summary>
        /// 全局数据
        /// </summary>
       [JsonProperty("global")]
        public object Global { get; set; }

    }
}