using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 更新企业角色
    /// </summary>
    public partial class EnterpriseRoleDetailResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 角色 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 角色备注信息
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 是否系统默认角色
        /// </summary>
       [JsonProperty("is_system_default")]
        public bool? Is_system_default { get; set; }
        /// <summary>
        /// 角色类型标识符
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 是否企业默认角色
        /// </summary>
       [JsonProperty("is_default")]
        public bool? Is_default { get; set; }
        /// <summary>
        /// 周报的权限属性
        /// </summary>
       [JsonProperty("week_report")]
        public WeekReportResponseModel Week_report { get; set; }
        /// <summary>
        /// 任务的权限属性
        /// </summary>
       [JsonProperty("issue")]
        public IssueResponseModel Issue { get; set; }
        /// <summary>
        /// 项目的权限属性
        /// </summary>
       [JsonProperty("program")]
        public ProgramResponseModel Program { get; set; }
        /// <summary>
        /// 仓库的权限属性
        /// </summary>
       [JsonProperty("project")]
        public ProjectResponseModel Project { get; set; }
        /// <summary>
        /// 文档的权限属性
        /// </summary>
       [JsonProperty("doc")]
        public DocResponseModel Doc { get; set; }
        /// <summary>
        /// 文档的权限属性
        /// </summary>
       [JsonProperty("member")]
        public MemberResponseModel Member { get; set; }
        /// <summary>
        /// 统计的权限属性
        /// </summary>
       [JsonProperty("statistic")]
        public StatisticResponseModel Statistic { get; set; }
        /// <summary>
        /// 是否是企业管理员角色
        /// </summary>
       [JsonProperty("is_admin")]
        public bool? Is_admin { get; set; }
        /// <summary>
        /// 测试用例计划权限属性
        /// </summary>
       [JsonProperty("test_plan")]
        public TestPlanResponseModel Test_plan { get; set; }
        /// <summary>
        /// 测试用例库权限属性
        /// </summary>
       [JsonProperty("test_repository")]
        public TestRepositoryResponseModel Test_repository { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("admin_rules")]
        public string Admin_rules { get; set; }
        /// <summary>
        /// 能否访问企业
        /// </summary>
       [JsonProperty("access")]
        public object Access { get; set; }
        /// <summary>
        /// 角色创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 角色更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}