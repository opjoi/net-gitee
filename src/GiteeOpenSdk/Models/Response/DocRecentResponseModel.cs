using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取最近编辑的文件
    /// </summary>
    public partial class DocRecentResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 文件 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 父级 id
        /// </summary>
       [JsonProperty("parent_id")]
        public int? Parent_id { get; set; }
        /// <summary>
        /// 最后编辑者
        /// </summary>
       [JsonProperty("editor")]
        public UserWithRemarkResponseModel Editor { get; set; }
        /// <summary>
        /// 正在编辑的用户
        /// </summary>
       [JsonProperty("editing_user")]
        public UserWithRemarkResponseModel Editing_user { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
       [JsonProperty("creator")]
        public UserWithRemarkResponseModel Creator { get; set; }
        /// <summary>
        /// 文件归属的文档信息
        /// </summary>
       [JsonProperty("wiki_info")]
        public WikiInfoResponseModel Wiki_info { get; set; }
        /// <summary>
        /// 最后更新时间
        /// </summary>
       [JsonProperty("last_edit_time")]
        public DateTime Last_edit_time { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}