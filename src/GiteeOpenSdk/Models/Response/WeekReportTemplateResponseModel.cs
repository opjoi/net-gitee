using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 编辑周报模版
    /// </summary>
    public partial class WeekReportTemplateResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 模版ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 模版内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 是否是默认模版
        /// </summary>
       [JsonProperty("is_default")]
        public bool? Is_default { get; set; }
        /// <summary>
        /// 模版名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }

    }
}