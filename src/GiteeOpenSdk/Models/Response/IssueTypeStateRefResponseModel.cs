using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取当前任务可流转的下一状态
    /// </summary>
    public partial class IssueTypeStateRefResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 任务状态 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 任务状态的名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 任务状态的属性
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 任务状态的颜色
        /// </summary>
       [JsonProperty("color")]
        public string Color { get; set; }
        /// <summary>
        /// 任务状态的 Icon
        /// </summary>
       [JsonProperty("icon")]
        public string Icon { get; set; }
        /// <summary>
        /// 任务状态创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务状态更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}