using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 周报可关联动态列表
    /// </summary>
    public partial class EventResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 动态id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [JsonProperty("target_type")]
        public string Target_type { get; set; }
        /// <summary>
        /// 动态标识
        /// </summary>
       [JsonProperty("ident")]
        public string Ident { get; set; }
        /// <summary>
        /// 是否合规
        /// </summary>
       [JsonProperty("is_proper")]
        public bool? Is_proper { get; set; }
        /// <summary>
        /// 动态产生的时间
        /// </summary>
       [JsonProperty("created_at")]
        public string Created_at { get; set; }
        /// <summary>
        /// 动作
        /// </summary>
       [JsonProperty("action")]
        public string Action { get; set; }
        /// <summary>
        /// 动作名称
        /// </summary>
       [JsonProperty("action_human_name")]
        public string Action_human_name { get; set; }
        /// <summary>
        /// 产生动态的成员
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 是否在仓库
        /// </summary>
       [JsonProperty("in_project")]
        public bool? In_project { get; set; }
        /// <summary>
        /// 是否在企业
        /// </summary>
       [JsonProperty("in_enterprise")]
        public bool? In_enterprise { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
       [JsonProperty("project")]
        public ProjectResponseModel Project { get; set; }
        /// <summary>
        /// 企业
        /// </summary>
       [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 不同类型动态的内容
        /// </summary>
       [JsonProperty("payload")]
        public object Payload { get; set; }

    }
}