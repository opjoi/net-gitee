using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取任务类型的工作流列表
    /// </summary>
    public partial class TreeNoteResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 评论的 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 评论类型
        /// </summary>
       [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// 评论发起人
        /// </summary>
       [JsonProperty("author")]
        public UserWithRemarkResponseModel Author { get; set; }
        /// <summary>
        /// 评论内容(markdown 格式)
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 评论内容(html 格式)
        /// </summary>
       [JsonProperty("content_html")]
        public string Content_html { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 祖先层级
        /// </summary>
       [JsonProperty("ancestry")]
        public string Ancestry { get; set; }
        /// <summary>
        /// 是否最外层评论
        /// </summary>
       [JsonProperty("head")]
        public string Head { get; set; }
        /// <summary>
        /// 是否父级评论
        /// </summary>
       [JsonProperty("parent")]
        public string Parent { get; set; }
        /// <summary>
        /// 评论编辑、删除权限
        /// </summary>
       [JsonProperty("operating")]
        public string Operating { get; set; }
        /// <summary>
        /// 表态
        /// </summary>
       [JsonProperty("reactions")]
        public ReactionResponseModel Reactions { get; set; }

    }
}