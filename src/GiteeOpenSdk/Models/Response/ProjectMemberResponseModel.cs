using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 仓库成员
    /// </summary>
    public partial class ProjectMemberResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 用户 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 用户昵称
        /// </summary>
       [JsonProperty("nickname")]
        public string Nickname { get; set; }
        /// <summary>
        /// 用户个性域名
        /// </summary>
       [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 跳转类型
        /// </summary>
       [JsonProperty("target")]
        public string Target { get; set; }
        /// <summary>
        /// 用户头像链接
        /// </summary>
       [JsonProperty("avatar_url")]
        public string Avatar_url { get; set; }
        /// <summary>
        /// 用户邮箱
        /// </summary>
       [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 是否是企业成员
        /// </summary>
       [JsonProperty("is_enterprise_member")]
        public bool? Is_enterprise_member { get; set; }
        /// <summary>
        /// 用户企业角色名称
        /// </summary>
       [JsonProperty("access_level")]
        public string Access_level { get; set; }
        /// <summary>
        /// 用户企业角色ident
        /// </summary>
       [JsonProperty("access_level_ident")]
        public string Access_level_ident { get; set; }
        /// <summary>
        /// 是否被锁定
        /// </summary>
       [JsonProperty("is_blocked")]
        public bool? Is_blocked { get; set; }
        /// <summary>
        /// 是否是自己
        /// </summary>
       [JsonProperty("is_myself")]
        public bool? Is_myself { get; set; }
        /// <summary>
        /// 用户在仓库中的级别
        /// </summary>
       [JsonProperty("project_access")]
        public string Project_access { get; set; }
        /// <summary>
        /// 用户在仓库中的级别名称
        /// </summary>
       [JsonProperty("project_access_name")]
        public string Project_access_name { get; set; }
        /// <summary>
        /// 是否是仓库创建者
        /// </summary>
       [JsonProperty("project_creator")]
        public bool? Project_creator { get; set; }
        /// <summary>
        /// 是否是仓库拥有者
        /// </summary>
       [JsonProperty("project_owner")]
        public bool? Project_owner { get; set; }
        /// <summary>
        /// 加入时间
        /// </summary>
       [JsonProperty("join_at")]
        public bool? Join_at { get; set; }

    }
}