using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ScrumVersionResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 版本ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 企业ID
        /// </summary>
       [JsonProperty("enterprise_id")]
        public int? Enterprise_id { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
       [JsonProperty("program_id")]
        public int? Program_id { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
       [JsonProperty("number")]
        public string Number { get; set; }
        /// <summary>
        /// 计划发版时间
        /// </summary>
       [JsonProperty("plan_released_at")]
        public string Plan_released_at { get; set; }
        /// <summary>
        /// 实际发版时间
        /// </summary>
       [JsonProperty("released_at")]
        public string Released_at { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
       [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }

    }
}