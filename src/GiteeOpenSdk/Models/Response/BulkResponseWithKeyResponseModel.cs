using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 批量处理申请记录
    /// </summary>
    public partial class BulkResponseWithKeyResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 消息数组模版[&#39;key&#39;, &#39;message&#39;]
        /// </summary>
       [JsonProperty("fields")]
        public object Fields { get; set; }
        /// <summary>
        /// 错误消息数组
        /// </summary>
       [JsonProperty("errors")]
        public object Errors { get; set; }
        /// <summary>
        /// 成功消息数组
        /// </summary>
       [JsonProperty("successes")]
        public object Successes { get; set; }

    }
}