using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 预览周报
    /// </summary>
    public partial class WeekReportPreviewResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 关联Pull request
        /// </summary>
       [JsonProperty("issues")]
        public IssueMainResponseModel Issues { get; set; }
        /// <summary>
        /// 关联Issues
        /// </summary>
       [JsonProperty("pull_requests")]
        public PullRequestResponseModel Pull_requests { get; set; }
        /// <summary>
        /// 关联动态
        /// </summary>
       [JsonProperty("events")]
        public EventResponseModel Events { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
       [JsonProperty("content")]
        public string Content { get; set; }

    }
}