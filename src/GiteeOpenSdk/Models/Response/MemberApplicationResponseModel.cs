using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 处理申请记录
    /// </summary>
    public partial class MemberApplicationResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 成员申请的 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 成员申请时的名称
        /// </summary>
       [JsonProperty("applicant_name")]
        public string Applicant_name { get; set; }
        /// <summary>
        /// 申请时的名称
        /// </summary>
       [JsonProperty("applicant")]
        public UserWithRemarkResponseModel Applicant { get; set; }
        /// <summary>
        /// 邀请者信息
        /// </summary>
       [JsonProperty("inviter")]
        public UserWithRemarkResponseModel Inviter { get; set; }
        /// <summary>
        /// 申请时的备注
        /// </summary>
       [JsonProperty("remark")]
        public string Remark { get; set; }
        /// <summary>
        /// 是否需要审核
        /// </summary>
       [JsonProperty("need_check")]
        public string Need_check { get; set; }
        /// <summary>
        /// 审核的状态。申请中：pending; 审核通过: approved; 已过期: overdue; 已拒绝: refused; 接收邀请链接：invite_pass
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 操作者
        /// </summary>
       [JsonProperty("operator")]
        public string Operator { get; set; }
        /// <summary>
        /// 角色 ID
        /// </summary>
       [JsonProperty("role_id")]
        public int? Role_id { get; set; }
        /// <summary>
        /// 权限属性
        /// </summary>
       [JsonProperty("access")]
        public int? Access { get; set; }
        /// <summary>
        /// 创建时间/申请时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }

    }
}