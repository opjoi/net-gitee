using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 
    /// </summary>
    public partial class MilestoneResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 里程碑 ID
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 里程碑标题名称
        /// </summary>
       [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 里程碑状态
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 里程碑起始日期
        /// </summary>
       [JsonProperty("start_date")]
        public DateTime Start_date { get; set; }
        /// <summary>
        /// 里程碑结束日期
        /// </summary>
       [JsonProperty("due_date")]
        public DateTime Due_date { get; set; }
        /// <summary>
        /// 任务标签创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 任务标签更新时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 里程碑所属项目 ID
        /// </summary>
       [JsonProperty("program_id")]
        public int? Program_id { get; set; }

    }
}