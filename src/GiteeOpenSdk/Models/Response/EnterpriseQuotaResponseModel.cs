using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取当前企业的配额信息
    /// </summary>
    public partial class EnterpriseQuotaResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 当前企业的团队总数
        /// </summary>
       [JsonProperty("groups_count")]
        public int? Groups_count { get; set; }
        /// <summary>
        /// 当前企业的成员总数（不包含观察者）
        /// </summary>
       [JsonProperty("members_count")]
        public int? Members_count { get; set; }
        /// <summary>
        /// 当前企业的观察者总数
        /// </summary>
       [JsonProperty("viewers_count")]
        public int? Viewers_count { get; set; }
        /// <summary>
        /// 当前企业的仓库总数
        /// </summary>
       [JsonProperty("projects_count")]
        public int? Projects_count { get; set; }
        /// <summary>
        /// 当前企业的项目总数
        /// </summary>
       [JsonProperty("programs_count")]
        public int? Programs_count { get; set; }
        /// <summary>
        /// 在企业内能创建的仓库数的总限制
        /// </summary>
       [JsonProperty("project_quota")]
        public int? Project_quota { get; set; }
        /// <summary>
        /// 当前企业套餐的仓库容量大小配额。单位：G
        /// </summary>
       [JsonProperty("space_quota")]
        public int? Space_quota { get; set; }
        /// <summary>
        /// 当前企业套餐的成员数配额
        /// </summary>
       [JsonProperty("member_quota")]
        public int? Member_quota { get; set; }
        /// <summary>
        /// 当前企业套餐的观察者数配额
        /// </summary>
       [JsonProperty("viewer_quota")]
        public int? Viewer_quota { get; set; }
        /// <summary>
        /// 当前企业套餐的附件配额。单位：G
        /// </summary>
       [JsonProperty("attach_file_quota")]
        public int? Attach_file_quota { get; set; }
        /// <summary>
        /// 当前企业套餐的 LFS 套餐配额。单位：G
        /// </summary>
       [JsonProperty("lfs_quota")]
        public int? Lfs_quota { get; set; }
        /// <summary>
        /// 企业附件已占用的容量。单位: 兆
        /// </summary>
       [JsonProperty("attach_file_used_space_quota")]
        public int? Attach_file_used_space_quota { get; set; }
        /// <summary>
        /// 企业 LFS 已使用的容量。单位: 兆
        /// </summary>
       [JsonProperty("lfs_space_used_quota")]
        public int? Lfs_space_used_quota { get; set; }
        /// <summary>
        /// 企业仓库已占用的容量。单位: 兆
        /// </summary>
       [JsonProperty("project_used_space_quota")]
        public int? Project_used_space_quota { get; set; }
        /// <summary>
        /// 当前企业套餐的单文件大小配额。单位: 兆
        /// </summary>
       [JsonProperty("single_file_quota")]
        public int? Single_file_quota { get; set; }

    }
}