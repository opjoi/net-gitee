using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 企业外的成员-从企业组织或仓库中移除(单个/批量)
    /// </summary>
    public partial class UserResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 用户 id
        /// </summary>
       [JsonProperty("id")]
        public int? Id { get; set; }
        /// <summary>
        /// 用户个性地址
        /// </summary>
       [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
       [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 用户头像
        /// </summary>
       [JsonProperty("avatar_url")]
        public string Avatar_url { get; set; }
        /// <summary>
        /// 账号是否可用(active: 可用 blocked: 已被系统屏蔽)
        /// </summary>
       [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
       [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }
        /// <summary>
        /// 最近活跃时间
        /// </summary>
       [JsonProperty("updated_at")]
        public DateTime Updated_at { get; set; }
        /// <summary>
        /// 用户代码风格
        /// </summary>
       [JsonProperty("user_color")]
        public int? User_color { get; set; }

    }
}