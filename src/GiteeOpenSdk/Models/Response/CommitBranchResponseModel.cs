using GiteeOpenSdk.Common;
using Newtonsoft.Json;
using System;

namespace GiteeOpenSdk.Models.Response
{
    /// <summary>
    /// 获取commit的分支和tag
    /// </summary>
    public partial class CommitBranchResponseModel : GiteeResponseModel
    {
        /// <summary>
        /// 分支
        /// </summary>
       [JsonProperty("branches")]
        public object Branches { get; set; }
        /// <summary>
        /// tag
        /// </summary>
       [JsonProperty("tags")]
        public object Tags { get; set; }

    }
}