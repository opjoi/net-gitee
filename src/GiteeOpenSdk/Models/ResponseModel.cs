﻿using System.Collections.Generic;
using System.Net;

namespace GiteeOpenSdk.Models
{
    /// <summary>
    /// SDK统一返回类型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseModel<T> where T : GiteeResponseModel
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }
        /// <summary>
        /// 返回内容
        /// </summary>
        public T ResponseContent { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMessage { get; set; }
    }
    /// <summary>
    /// SDK统一返回类型（列表型）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseModels<T> where T : GiteeResponseModel
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }
        /// <summary>
        /// 返回内容
        /// </summary>
        public List<T> ResponseContent { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}