﻿using GiteeOpenSdk;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class GiteeServiceExtensions
    {
        /// <summary>
        /// 添加Gitee服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="optionsAction"></param>
        public static void AddGitee(this IServiceCollection services, Action<GiteeOption> optionsAction = null)
        {
            if (optionsAction != null)
            {
                services.Configure(optionsAction);
            }
            services.AddSingleton(typeof(GiteeService));
        }
    }
}
