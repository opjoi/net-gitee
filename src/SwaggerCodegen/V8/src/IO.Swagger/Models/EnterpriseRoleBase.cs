/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class EnterpriseRoleBase : IEquatable<EnterpriseRoleBase>
    { 
        /// <summary>
        /// 角色 id
        /// </summary>
        /// <value>角色 id</value>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        /// <value>角色名称</value>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// 角色备注信息
        /// </summary>
        /// <value>角色备注信息</value>
        [DataMember(Name="description")]
        public string Description { get; set; }

        /// <summary>
        /// 是否系统默认角色
        /// </summary>
        /// <value>是否系统默认角色</value>
        [DataMember(Name="is_system_default")]
        public bool? IsSystemDefault { get; set; }

        /// <summary>
        /// 角色类型标识符
        /// </summary>
        /// <value>角色类型标识符</value>
        [DataMember(Name="ident")]
        public string Ident { get; set; }

        /// <summary>
        /// 是否企业默认角色
        /// </summary>
        /// <value>是否企业默认角色</value>
        [DataMember(Name="is_default")]
        public bool? IsDefault { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class EnterpriseRoleBase {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  IsSystemDefault: ").Append(IsSystemDefault).Append("\n");
            sb.Append("  Ident: ").Append(Ident).Append("\n");
            sb.Append("  IsDefault: ").Append(IsDefault).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((EnterpriseRoleBase)obj);
        }

        /// <summary>
        /// Returns true if EnterpriseRoleBase instances are equal
        /// </summary>
        /// <param name="other">Instance of EnterpriseRoleBase to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(EnterpriseRoleBase other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    Description == other.Description ||
                    Description != null &&
                    Description.Equals(other.Description)
                ) && 
                (
                    IsSystemDefault == other.IsSystemDefault ||
                    IsSystemDefault != null &&
                    IsSystemDefault.Equals(other.IsSystemDefault)
                ) && 
                (
                    Ident == other.Ident ||
                    Ident != null &&
                    Ident.Equals(other.Ident)
                ) && 
                (
                    IsDefault == other.IsDefault ||
                    IsDefault != null &&
                    IsDefault.Equals(other.IsDefault)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (Description != null)
                    hashCode = hashCode * 59 + Description.GetHashCode();
                    if (IsSystemDefault != null)
                    hashCode = hashCode * 59 + IsSystemDefault.GetHashCode();
                    if (Ident != null)
                    hashCode = hashCode * 59 + Ident.GetHashCode();
                    if (IsDefault != null)
                    hashCode = hashCode * 59 + IsDefault.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(EnterpriseRoleBase left, EnterpriseRoleBase right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EnterpriseRoleBase left, EnterpriseRoleBase right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
