/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 修改仓库成员权限
    /// </summary>
    [DataContract]
    public partial class Member : IEquatable<Member>
    { 
        /// <summary>
        /// 成员 id
        /// </summary>
        /// <value>成员 id</value>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// 成员个性地址
        /// </summary>
        /// <value>成员个性地址</value>
        [DataMember(Name="username")]
        public string Username { get; set; }

        /// <summary>
        /// 成员名称
        /// </summary>
        /// <value>成员名称</value>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// 成员在企业的备注姓名
        /// </summary>
        /// <value>成员在企业的备注姓名</value>
        [DataMember(Name="remark")]
        public string Remark { get; set; }

        /// <summary>
        /// 成员备注或名称拼音
        /// </summary>
        /// <value>成员备注或名称拼音</value>
        [DataMember(Name="pinyin")]
        public string Pinyin { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        /// <value>职位</value>
        [DataMember(Name="occupation")]
        public string Occupation { get; set; }

        /// <summary>
        /// 是否被锁定
        /// </summary>
        /// <value>是否被锁定</value>
        [DataMember(Name="is_block")]
        public bool? IsBlock { get; set; }

        /// <summary>
        /// 成员被锁定的原因
        /// </summary>
        /// <value>成员被锁定的原因</value>
        [DataMember(Name="block_message")]
        public string BlockMessage { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        /// <value>手机号码</value>
        [DataMember(Name="phone")]
        public string Phone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        /// <value>邮箱</value>
        [DataMember(Name="email")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or Sets User
        /// </summary>
        [DataMember(Name="user")]
        public User User { get; set; }

        /// <summary>
        /// Gets or Sets EnterpriseRole
        /// </summary>
        [DataMember(Name="enterprise_role")]
        public EnterpriseRoleBase EnterpriseRole { get; set; }

        /// <summary>
        /// 成员是否填写过
        /// </summary>
        /// <value>成员是否填写过</value>
        [DataMember(Name="is_feedback")]
        public bool? IsFeedback { get; set; }

        /// <summary>
        /// 企业新旧版本（ 0 代表旧版、1 代表新版 ）
        /// </summary>
        /// <value>企业新旧版本（ 0 代表旧版、1 代表新版 ）</value>
        [DataMember(Name="enterprise_version")]
        public int? EnterpriseVersion { get; set; }

        /// <summary>
        /// 是否完成引导
        /// </summary>
        /// <value>是否完成引导</value>
        [DataMember(Name="is_guided")]
        public bool? IsGuided { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Member {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Username: ").Append(Username).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Remark: ").Append(Remark).Append("\n");
            sb.Append("  Pinyin: ").Append(Pinyin).Append("\n");
            sb.Append("  Occupation: ").Append(Occupation).Append("\n");
            sb.Append("  IsBlock: ").Append(IsBlock).Append("\n");
            sb.Append("  BlockMessage: ").Append(BlockMessage).Append("\n");
            sb.Append("  Phone: ").Append(Phone).Append("\n");
            sb.Append("  Email: ").Append(Email).Append("\n");
            sb.Append("  User: ").Append(User).Append("\n");
            sb.Append("  EnterpriseRole: ").Append(EnterpriseRole).Append("\n");
            sb.Append("  IsFeedback: ").Append(IsFeedback).Append("\n");
            sb.Append("  EnterpriseVersion: ").Append(EnterpriseVersion).Append("\n");
            sb.Append("  IsGuided: ").Append(IsGuided).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Member)obj);
        }

        /// <summary>
        /// Returns true if Member instances are equal
        /// </summary>
        /// <param name="other">Instance of Member to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Member other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Username == other.Username ||
                    Username != null &&
                    Username.Equals(other.Username)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    Remark == other.Remark ||
                    Remark != null &&
                    Remark.Equals(other.Remark)
                ) && 
                (
                    Pinyin == other.Pinyin ||
                    Pinyin != null &&
                    Pinyin.Equals(other.Pinyin)
                ) && 
                (
                    Occupation == other.Occupation ||
                    Occupation != null &&
                    Occupation.Equals(other.Occupation)
                ) && 
                (
                    IsBlock == other.IsBlock ||
                    IsBlock != null &&
                    IsBlock.Equals(other.IsBlock)
                ) && 
                (
                    BlockMessage == other.BlockMessage ||
                    BlockMessage != null &&
                    BlockMessage.Equals(other.BlockMessage)
                ) && 
                (
                    Phone == other.Phone ||
                    Phone != null &&
                    Phone.Equals(other.Phone)
                ) && 
                (
                    Email == other.Email ||
                    Email != null &&
                    Email.Equals(other.Email)
                ) && 
                (
                    User == other.User ||
                    User != null &&
                    User.Equals(other.User)
                ) && 
                (
                    EnterpriseRole == other.EnterpriseRole ||
                    EnterpriseRole != null &&
                    EnterpriseRole.Equals(other.EnterpriseRole)
                ) && 
                (
                    IsFeedback == other.IsFeedback ||
                    IsFeedback != null &&
                    IsFeedback.Equals(other.IsFeedback)
                ) && 
                (
                    EnterpriseVersion == other.EnterpriseVersion ||
                    EnterpriseVersion != null &&
                    EnterpriseVersion.Equals(other.EnterpriseVersion)
                ) && 
                (
                    IsGuided == other.IsGuided ||
                    IsGuided != null &&
                    IsGuided.Equals(other.IsGuided)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Username != null)
                    hashCode = hashCode * 59 + Username.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (Remark != null)
                    hashCode = hashCode * 59 + Remark.GetHashCode();
                    if (Pinyin != null)
                    hashCode = hashCode * 59 + Pinyin.GetHashCode();
                    if (Occupation != null)
                    hashCode = hashCode * 59 + Occupation.GetHashCode();
                    if (IsBlock != null)
                    hashCode = hashCode * 59 + IsBlock.GetHashCode();
                    if (BlockMessage != null)
                    hashCode = hashCode * 59 + BlockMessage.GetHashCode();
                    if (Phone != null)
                    hashCode = hashCode * 59 + Phone.GetHashCode();
                    if (Email != null)
                    hashCode = hashCode * 59 + Email.GetHashCode();
                    if (User != null)
                    hashCode = hashCode * 59 + User.GetHashCode();
                    if (EnterpriseRole != null)
                    hashCode = hashCode * 59 + EnterpriseRole.GetHashCode();
                    if (IsFeedback != null)
                    hashCode = hashCode * 59 + IsFeedback.GetHashCode();
                    if (EnterpriseVersion != null)
                    hashCode = hashCode * 59 + EnterpriseVersion.GetHashCode();
                    if (IsGuided != null)
                    hashCode = hashCode * 59 + IsGuided.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Member left, Member right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Member left, Member right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
