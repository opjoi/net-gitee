/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 移动文件
    /// </summary>
    [DataContract]
    public partial class WikiSort : IEquatable<WikiSort>
    { 
        /// <summary>
        /// 文件 id
        /// </summary>
        /// <value>文件 id</value>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        /// <value>文件名称</value>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// 父级 id
        /// </summary>
        /// <value>父级 id</value>
        [DataMember(Name="parent_id")]
        public int? ParentId { get; set; }

        /// <summary>
        /// Gets or Sets Creator
        /// </summary>
        [DataMember(Name="creator")]
        public UserWithRemark Creator { get; set; }

        /// <summary>
        /// Gets or Sets Editor
        /// </summary>
        [DataMember(Name="editor")]
        public UserWithRemark Editor { get; set; }

        /// <summary>
        /// 文件类型(文件: 1; 文件夹: 2)
        /// </summary>
        /// <value>文件类型(文件: 1; 文件夹: 2)</value>
        [DataMember(Name="file_type")]
        public int? FileType { get; set; }

        /// <summary>
        /// 文件的最后一次版本号
        /// </summary>
        /// <value>文件的最后一次版本号</value>
        [DataMember(Name="version")]
        public string Version { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class WikiSort {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  ParentId: ").Append(ParentId).Append("\n");
            sb.Append("  Creator: ").Append(Creator).Append("\n");
            sb.Append("  Editor: ").Append(Editor).Append("\n");
            sb.Append("  FileType: ").Append(FileType).Append("\n");
            sb.Append("  Version: ").Append(Version).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((WikiSort)obj);
        }

        /// <summary>
        /// Returns true if WikiSort instances are equal
        /// </summary>
        /// <param name="other">Instance of WikiSort to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(WikiSort other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    ParentId == other.ParentId ||
                    ParentId != null &&
                    ParentId.Equals(other.ParentId)
                ) && 
                (
                    Creator == other.Creator ||
                    Creator != null &&
                    Creator.Equals(other.Creator)
                ) && 
                (
                    Editor == other.Editor ||
                    Editor != null &&
                    Editor.Equals(other.Editor)
                ) && 
                (
                    FileType == other.FileType ||
                    FileType != null &&
                    FileType.Equals(other.FileType)
                ) && 
                (
                    Version == other.Version ||
                    Version != null &&
                    Version.Equals(other.Version)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (ParentId != null)
                    hashCode = hashCode * 59 + ParentId.GetHashCode();
                    if (Creator != null)
                    hashCode = hashCode * 59 + Creator.GetHashCode();
                    if (Editor != null)
                    hashCode = hashCode * 59 + Editor.GetHashCode();
                    if (FileType != null)
                    hashCode = hashCode * 59 + FileType.GetHashCode();
                    if (Version != null)
                    hashCode = hashCode * 59 + Version.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(WikiSort left, WikiSort right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(WikiSort left, WikiSort right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
