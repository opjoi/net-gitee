/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 更新仓库设置
    /// </summary>
    [DataContract]
    public partial class ProjectBaseSetting : IEquatable<ProjectBaseSetting>
    { 
        /// <summary>
        /// 仓库ID
        /// </summary>
        /// <value>仓库ID</value>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// 仓库名称
        /// </summary>
        /// <value>仓库名称</value>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// 仓库路径
        /// </summary>
        /// <value>仓库路径</value>
        [DataMember(Name="path")]
        public string Path { get; set; }

        /// <summary>
        /// namespace/path
        /// </summary>
        /// <value>namespace/path</value>
        [DataMember(Name="path_with_namespace")]
        public string PathWithNamespace { get; set; }

        /// <summary>
        /// 仓库开源属性，0:私有，1:开源，2:内部开源
        /// </summary>
        /// <value>仓库开源属性，0:私有，1:开源，2:内部开源</value>
        [DataMember(Name="public")]
        public string Public { get; set; }

        /// <summary>
        /// 是否为GVP仓库
        /// </summary>
        /// <value>是否为GVP仓库</value>
        [DataMember(Name="is_gvp")]
        public bool? IsGvp { get; set; }

        /// <summary>
        /// 是否为空仓库
        /// </summary>
        /// <value>是否为空仓库</value>
        [DataMember(Name="is_empty_repo")]
        public bool? IsEmptyRepo { get; set; }

        /// <summary>
        /// 是否允许仓库被Fork
        /// </summary>
        /// <value>是否允许仓库被Fork</value>
        [DataMember(Name="fork_enabled")]
        public bool? ForkEnabled { get; set; }

        /// <summary>
        /// namespace_name/path
        /// </summary>
        /// <value>namespace_name/path</value>
        [DataMember(Name="name_with_namespace")]
        public string NameWithNamespace { get; set; }

        /// <summary>
        /// 仓库介绍
        /// </summary>
        /// <value>仓库介绍</value>
        [DataMember(Name="description")]
        public string Description { get; set; }

        /// <summary>
        /// 仓库主页
        /// </summary>
        /// <value>仓库主页</value>
        [DataMember(Name="homepage")]
        public string Homepage { get; set; }

        /// <summary>
        /// 仓库语言
        /// </summary>
        /// <value>仓库语言</value>
        [DataMember(Name="lang_id")]
        public string LangId { get; set; }

        /// <summary>
        /// 仓库状态(已关闭/开发中/已完结/维护中)
        /// </summary>
        /// <value>仓库状态(已关闭/开发中/已完结/维护中)</value>
        [DataMember(Name="status")]
        public string Status { get; set; }

        /// <summary>
        /// 默认分支
        /// </summary>
        /// <value>默认分支</value>
        [DataMember(Name="default_branch")]
        public string DefaultBranch { get; set; }

        /// <summary>
        /// 仓库类型，0内部，1外包
        /// </summary>
        /// <value>仓库类型，0内部，1外包</value>
        [DataMember(Name="outsourced")]
        public bool? Outsourced { get; set; }

        /// <summary>
        /// Gets or Sets Creator
        /// </summary>
        [DataMember(Name="creator")]
        public UserWithRemark Creator { get; set; }

        /// <summary>
        /// Gets or Sets Programs
        /// </summary>
        [DataMember(Name="programs")]
        public Program Programs { get; set; }

        /// <summary>
        /// 允许用户对仓库进行评论
        /// </summary>
        /// <value>允许用户对仓库进行评论</value>
        [DataMember(Name="can_comment")]
        public bool? CanComment { get; set; }

        /// <summary>
        /// 允许用户对\&quot;关闭\&quot;状态的Issues进行评论
        /// </summary>
        /// <value>允许用户对\&quot;关闭\&quot;状态的Issues进行评论</value>
        [DataMember(Name="issue_comment")]
        public bool? IssueComment { get; set; }

        /// <summary>
        /// 轻量级的issue跟踪系统
        /// </summary>
        /// <value>轻量级的issue跟踪系统</value>
        [DataMember(Name="issues_enabled")]
        public bool? IssuesEnabled { get; set; }

        /// <summary>
        /// 允许用户创建涉及敏感信息的Issue，提交后不公开此Issue（可见范围：仓库成员、企业成员）
        /// </summary>
        /// <value>允许用户创建涉及敏感信息的Issue，提交后不公开此Issue（可见范围：仓库成员、企业成员）</value>
        [DataMember(Name="security_hole_enabled")]
        public bool? SecurityHoleEnabled { get; set; }

        /// <summary>
        /// 是否允许仓库文件在线编辑
        /// </summary>
        /// <value>是否允许仓库文件在线编辑</value>
        [DataMember(Name="online_edit_enabled")]
        public bool? OnlineEditEnabled { get; set; }

        /// <summary>
        /// 接受pull request，协作开发
        /// </summary>
        /// <value>接受pull request，协作开发</value>
        [DataMember(Name="pull_requests_enabled")]
        public bool? PullRequestsEnabled { get; set; }

        /// <summary>
        /// 可以编写文档
        /// </summary>
        /// <value>可以编写文档</value>
        [DataMember(Name="wiki_enabled")]
        public bool? WikiEnabled { get; set; }

        /// <summary>
        /// 接受轻量级 Pull Request（用户可以发起轻量级 Pull Request 而无需 Fork 仓库）
        /// </summary>
        /// <value>接受轻量级 Pull Request（用户可以发起轻量级 Pull Request 而无需 Fork 仓库）</value>
        [DataMember(Name="lightweight_pr_enabled")]
        public bool? LightweightPrEnabled { get; set; }

        /// <summary>
        /// 开启的Pull Request，仅管理员、审查者、测试者可见
        /// </summary>
        /// <value>开启的Pull Request，仅管理员、审查者、测试者可见</value>
        [DataMember(Name="pr_master_only")]
        public bool? PrMasterOnly { get; set; }

        /// <summary>
        /// 禁止强制推送
        /// </summary>
        /// <value>禁止强制推送</value>
        [DataMember(Name="forbid_force_push")]
        public bool? ForbidForcePush { get; set; }

        /// <summary>
        /// 仓库远程地址(用于强制同步)
        /// </summary>
        /// <value>仓库远程地址(用于强制同步)</value>
        [DataMember(Name="import_url")]
        public bool? ImportUrl { get; set; }

        /// <summary>
        /// 禁止仓库同步(禁止从仓库远程地址或Fork的源仓库强制同步代码，禁止后将关闭同步按钮)
        /// </summary>
        /// <value>禁止仓库同步(禁止从仓库远程地址或Fork的源仓库强制同步代码，禁止后将关闭同步按钮)</value>
        [DataMember(Name="forbid_force_sync")]
        public bool? ForbidForceSync { get; set; }

        /// <summary>
        /// 使用SVN管理您的仓库
        /// </summary>
        /// <value>使用SVN管理您的仓库</value>
        [DataMember(Name="svn_enabled")]
        public bool? SvnEnabled { get; set; }

        /// <summary>
        /// 开启文件/文件夹只读功能(只读文件和SVN支持无法同时启用)
        /// </summary>
        /// <value>开启文件/文件夹只读功能(只读文件和SVN支持无法同时启用)</value>
        [DataMember(Name="can_readonly")]
        public bool? CanReadonly { get; set; }

        /// <summary>
        /// 企业是否禁用SVN
        /// </summary>
        /// <value>企业是否禁用SVN</value>
        [DataMember(Name="enterprise_forbids_svn")]
        public bool? EnterpriseForbidsSvn { get; set; }

        /// <summary>
        /// Gets or Sets Parent
        /// </summary>
        [DataMember(Name="parent")]
        public Project Parent { get; set; }

        /// <summary>
        /// 仓库是启用了GiteeGo
        /// </summary>
        /// <value>仓库是启用了GiteeGo</value>
        [DataMember(Name="gitee_go_enabled")]
        public bool? GiteeGoEnabled { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ProjectBaseSetting {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Path: ").Append(Path).Append("\n");
            sb.Append("  PathWithNamespace: ").Append(PathWithNamespace).Append("\n");
            sb.Append("  Public: ").Append(Public).Append("\n");
            sb.Append("  IsGvp: ").Append(IsGvp).Append("\n");
            sb.Append("  IsEmptyRepo: ").Append(IsEmptyRepo).Append("\n");
            sb.Append("  ForkEnabled: ").Append(ForkEnabled).Append("\n");
            sb.Append("  NameWithNamespace: ").Append(NameWithNamespace).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  Homepage: ").Append(Homepage).Append("\n");
            sb.Append("  LangId: ").Append(LangId).Append("\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  DefaultBranch: ").Append(DefaultBranch).Append("\n");
            sb.Append("  Outsourced: ").Append(Outsourced).Append("\n");
            sb.Append("  Creator: ").Append(Creator).Append("\n");
            sb.Append("  Programs: ").Append(Programs).Append("\n");
            sb.Append("  CanComment: ").Append(CanComment).Append("\n");
            sb.Append("  IssueComment: ").Append(IssueComment).Append("\n");
            sb.Append("  IssuesEnabled: ").Append(IssuesEnabled).Append("\n");
            sb.Append("  SecurityHoleEnabled: ").Append(SecurityHoleEnabled).Append("\n");
            sb.Append("  OnlineEditEnabled: ").Append(OnlineEditEnabled).Append("\n");
            sb.Append("  PullRequestsEnabled: ").Append(PullRequestsEnabled).Append("\n");
            sb.Append("  WikiEnabled: ").Append(WikiEnabled).Append("\n");
            sb.Append("  LightweightPrEnabled: ").Append(LightweightPrEnabled).Append("\n");
            sb.Append("  PrMasterOnly: ").Append(PrMasterOnly).Append("\n");
            sb.Append("  ForbidForcePush: ").Append(ForbidForcePush).Append("\n");
            sb.Append("  ImportUrl: ").Append(ImportUrl).Append("\n");
            sb.Append("  ForbidForceSync: ").Append(ForbidForceSync).Append("\n");
            sb.Append("  SvnEnabled: ").Append(SvnEnabled).Append("\n");
            sb.Append("  CanReadonly: ").Append(CanReadonly).Append("\n");
            sb.Append("  EnterpriseForbidsSvn: ").Append(EnterpriseForbidsSvn).Append("\n");
            sb.Append("  Parent: ").Append(Parent).Append("\n");
            sb.Append("  GiteeGoEnabled: ").Append(GiteeGoEnabled).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((ProjectBaseSetting)obj);
        }

        /// <summary>
        /// Returns true if ProjectBaseSetting instances are equal
        /// </summary>
        /// <param name="other">Instance of ProjectBaseSetting to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ProjectBaseSetting other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    Path == other.Path ||
                    Path != null &&
                    Path.Equals(other.Path)
                ) && 
                (
                    PathWithNamespace == other.PathWithNamespace ||
                    PathWithNamespace != null &&
                    PathWithNamespace.Equals(other.PathWithNamespace)
                ) && 
                (
                    Public == other.Public ||
                    Public != null &&
                    Public.Equals(other.Public)
                ) && 
                (
                    IsGvp == other.IsGvp ||
                    IsGvp != null &&
                    IsGvp.Equals(other.IsGvp)
                ) && 
                (
                    IsEmptyRepo == other.IsEmptyRepo ||
                    IsEmptyRepo != null &&
                    IsEmptyRepo.Equals(other.IsEmptyRepo)
                ) && 
                (
                    ForkEnabled == other.ForkEnabled ||
                    ForkEnabled != null &&
                    ForkEnabled.Equals(other.ForkEnabled)
                ) && 
                (
                    NameWithNamespace == other.NameWithNamespace ||
                    NameWithNamespace != null &&
                    NameWithNamespace.Equals(other.NameWithNamespace)
                ) && 
                (
                    Description == other.Description ||
                    Description != null &&
                    Description.Equals(other.Description)
                ) && 
                (
                    Homepage == other.Homepage ||
                    Homepage != null &&
                    Homepage.Equals(other.Homepage)
                ) && 
                (
                    LangId == other.LangId ||
                    LangId != null &&
                    LangId.Equals(other.LangId)
                ) && 
                (
                    Status == other.Status ||
                    Status != null &&
                    Status.Equals(other.Status)
                ) && 
                (
                    DefaultBranch == other.DefaultBranch ||
                    DefaultBranch != null &&
                    DefaultBranch.Equals(other.DefaultBranch)
                ) && 
                (
                    Outsourced == other.Outsourced ||
                    Outsourced != null &&
                    Outsourced.Equals(other.Outsourced)
                ) && 
                (
                    Creator == other.Creator ||
                    Creator != null &&
                    Creator.Equals(other.Creator)
                ) && 
                (
                    Programs == other.Programs ||
                    Programs != null &&
                    Programs.Equals(other.Programs)
                ) && 
                (
                    CanComment == other.CanComment ||
                    CanComment != null &&
                    CanComment.Equals(other.CanComment)
                ) && 
                (
                    IssueComment == other.IssueComment ||
                    IssueComment != null &&
                    IssueComment.Equals(other.IssueComment)
                ) && 
                (
                    IssuesEnabled == other.IssuesEnabled ||
                    IssuesEnabled != null &&
                    IssuesEnabled.Equals(other.IssuesEnabled)
                ) && 
                (
                    SecurityHoleEnabled == other.SecurityHoleEnabled ||
                    SecurityHoleEnabled != null &&
                    SecurityHoleEnabled.Equals(other.SecurityHoleEnabled)
                ) && 
                (
                    OnlineEditEnabled == other.OnlineEditEnabled ||
                    OnlineEditEnabled != null &&
                    OnlineEditEnabled.Equals(other.OnlineEditEnabled)
                ) && 
                (
                    PullRequestsEnabled == other.PullRequestsEnabled ||
                    PullRequestsEnabled != null &&
                    PullRequestsEnabled.Equals(other.PullRequestsEnabled)
                ) && 
                (
                    WikiEnabled == other.WikiEnabled ||
                    WikiEnabled != null &&
                    WikiEnabled.Equals(other.WikiEnabled)
                ) && 
                (
                    LightweightPrEnabled == other.LightweightPrEnabled ||
                    LightweightPrEnabled != null &&
                    LightweightPrEnabled.Equals(other.LightweightPrEnabled)
                ) && 
                (
                    PrMasterOnly == other.PrMasterOnly ||
                    PrMasterOnly != null &&
                    PrMasterOnly.Equals(other.PrMasterOnly)
                ) && 
                (
                    ForbidForcePush == other.ForbidForcePush ||
                    ForbidForcePush != null &&
                    ForbidForcePush.Equals(other.ForbidForcePush)
                ) && 
                (
                    ImportUrl == other.ImportUrl ||
                    ImportUrl != null &&
                    ImportUrl.Equals(other.ImportUrl)
                ) && 
                (
                    ForbidForceSync == other.ForbidForceSync ||
                    ForbidForceSync != null &&
                    ForbidForceSync.Equals(other.ForbidForceSync)
                ) && 
                (
                    SvnEnabled == other.SvnEnabled ||
                    SvnEnabled != null &&
                    SvnEnabled.Equals(other.SvnEnabled)
                ) && 
                (
                    CanReadonly == other.CanReadonly ||
                    CanReadonly != null &&
                    CanReadonly.Equals(other.CanReadonly)
                ) && 
                (
                    EnterpriseForbidsSvn == other.EnterpriseForbidsSvn ||
                    EnterpriseForbidsSvn != null &&
                    EnterpriseForbidsSvn.Equals(other.EnterpriseForbidsSvn)
                ) && 
                (
                    Parent == other.Parent ||
                    Parent != null &&
                    Parent.Equals(other.Parent)
                ) && 
                (
                    GiteeGoEnabled == other.GiteeGoEnabled ||
                    GiteeGoEnabled != null &&
                    GiteeGoEnabled.Equals(other.GiteeGoEnabled)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (Path != null)
                    hashCode = hashCode * 59 + Path.GetHashCode();
                    if (PathWithNamespace != null)
                    hashCode = hashCode * 59 + PathWithNamespace.GetHashCode();
                    if (Public != null)
                    hashCode = hashCode * 59 + Public.GetHashCode();
                    if (IsGvp != null)
                    hashCode = hashCode * 59 + IsGvp.GetHashCode();
                    if (IsEmptyRepo != null)
                    hashCode = hashCode * 59 + IsEmptyRepo.GetHashCode();
                    if (ForkEnabled != null)
                    hashCode = hashCode * 59 + ForkEnabled.GetHashCode();
                    if (NameWithNamespace != null)
                    hashCode = hashCode * 59 + NameWithNamespace.GetHashCode();
                    if (Description != null)
                    hashCode = hashCode * 59 + Description.GetHashCode();
                    if (Homepage != null)
                    hashCode = hashCode * 59 + Homepage.GetHashCode();
                    if (LangId != null)
                    hashCode = hashCode * 59 + LangId.GetHashCode();
                    if (Status != null)
                    hashCode = hashCode * 59 + Status.GetHashCode();
                    if (DefaultBranch != null)
                    hashCode = hashCode * 59 + DefaultBranch.GetHashCode();
                    if (Outsourced != null)
                    hashCode = hashCode * 59 + Outsourced.GetHashCode();
                    if (Creator != null)
                    hashCode = hashCode * 59 + Creator.GetHashCode();
                    if (Programs != null)
                    hashCode = hashCode * 59 + Programs.GetHashCode();
                    if (CanComment != null)
                    hashCode = hashCode * 59 + CanComment.GetHashCode();
                    if (IssueComment != null)
                    hashCode = hashCode * 59 + IssueComment.GetHashCode();
                    if (IssuesEnabled != null)
                    hashCode = hashCode * 59 + IssuesEnabled.GetHashCode();
                    if (SecurityHoleEnabled != null)
                    hashCode = hashCode * 59 + SecurityHoleEnabled.GetHashCode();
                    if (OnlineEditEnabled != null)
                    hashCode = hashCode * 59 + OnlineEditEnabled.GetHashCode();
                    if (PullRequestsEnabled != null)
                    hashCode = hashCode * 59 + PullRequestsEnabled.GetHashCode();
                    if (WikiEnabled != null)
                    hashCode = hashCode * 59 + WikiEnabled.GetHashCode();
                    if (LightweightPrEnabled != null)
                    hashCode = hashCode * 59 + LightweightPrEnabled.GetHashCode();
                    if (PrMasterOnly != null)
                    hashCode = hashCode * 59 + PrMasterOnly.GetHashCode();
                    if (ForbidForcePush != null)
                    hashCode = hashCode * 59 + ForbidForcePush.GetHashCode();
                    if (ImportUrl != null)
                    hashCode = hashCode * 59 + ImportUrl.GetHashCode();
                    if (ForbidForceSync != null)
                    hashCode = hashCode * 59 + ForbidForceSync.GetHashCode();
                    if (SvnEnabled != null)
                    hashCode = hashCode * 59 + SvnEnabled.GetHashCode();
                    if (CanReadonly != null)
                    hashCode = hashCode * 59 + CanReadonly.GetHashCode();
                    if (EnterpriseForbidsSvn != null)
                    hashCode = hashCode * 59 + EnterpriseForbidsSvn.GetHashCode();
                    if (Parent != null)
                    hashCode = hashCode * 59 + Parent.GetHashCode();
                    if (GiteeGoEnabled != null)
                    hashCode = hashCode * 59 + GiteeGoEnabled.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(ProjectBaseSetting left, ProjectBaseSetting right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ProjectBaseSetting left, ProjectBaseSetting right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
