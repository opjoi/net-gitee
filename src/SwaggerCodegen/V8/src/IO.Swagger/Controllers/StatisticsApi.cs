/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;

using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class StatisticsApiController : ControllerBase
    { 
        /// <summary>
        /// 获取企业概览数据
        /// </summary>
        /// <remarks>获取企业概览数据</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">获取企业概览数据</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/dashboard_statistics/overview")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdDashboardStatisticsOverview")]
        public virtual IActionResult GetEnterpriseIdDashboardStatisticsOverview([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取当前用户的统计信息
        /// </summary>
        /// <remarks>获取当前用户的统计信息</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="200">获取当前用户的统计信息</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/dashboard_statistics/user_dashboard")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdDashboardStatisticsUserDashboard")]
        public virtual IActionResult GetEnterpriseIdDashboardStatisticsUserDashboard([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 获取企业开源仓库详情列表
        /// </summary>
        /// <remarks>获取企业开源仓库详情列表</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="projectId">仓库id</param>
        /// <param name="startDate">查询的起始时间。(格式：yyyy-mm-dd)</param>
        /// <param name="endDate">查询的结束时间。(格式：yyyy-mm-dd)</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/statistics/enterprise_os_project_list")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdStatisticsEnterpriseOsProjectList")]
        [SwaggerResponse(statusCode: 200, type: typeof(OsProjectsList), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdStatisticsEnterpriseOsProjectList([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? projectId, [FromQuery]string startDate, [FromQuery]string endDate)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(OsProjectsList));
            string exampleJson = null;
            exampleJson = "{\n  \"projects\" : {\n    \"path\" : \"path\",\n    \"enterprise_project_path\" : \"enterprise_project_path\",\n    \"public\" : 6,\n    \"name\" : \"name\",\n    \"id\" : 0,\n    \"name_with_namespace\" : \"name_with_namespace\"\n  }\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<OsProjectsList>(exampleJson)
                        : default(OsProjectsList);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取企业项目的统计信息
        /// </summary>
        /// <remarks>获取企业项目的统计信息</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="programId">项目id</param>
        /// <param name="startDate">查询的起始时间。(格式：yyyy-mm-dd)</param>
        /// <param name="endDate">查询的结束时间。(格式：yyyy-mm-dd)</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/statistics/enterprise_program_list")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdStatisticsEnterpriseProgramList")]
        [SwaggerResponse(statusCode: 200, type: typeof(EnterpriseProgramsList), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdStatisticsEnterpriseProgramList([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? programId, [FromQuery]string startDate, [FromQuery]string endDate)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(EnterpriseProgramsList));
            string exampleJson = null;
            exampleJson = "{\n  \"programs\" : {\n    \"path\" : \"path\",\n    \"name\" : \"name\",\n    \"id\" : 6,\n    \"assignee_id\" : 0\n  }\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<EnterpriseProgramsList>(exampleJson)
                        : default(EnterpriseProgramsList);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取企业仓库的统计信息
        /// </summary>
        /// <remarks>获取企业仓库的统计信息</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="projectId">仓库id</param>
        /// <param name="startDate">查询的起始时间。(格式：yyyy-mm-dd)</param>
        /// <param name="endDate">查询的结束时间。(格式：yyyy-mm-dd)</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/statistics/enterprise_project_list")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdStatisticsEnterpriseProjectList")]
        [SwaggerResponse(statusCode: 200, type: typeof(EnterpriseProjectsList), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdStatisticsEnterpriseProjectList([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? projectId, [FromQuery]string startDate, [FromQuery]string endDate)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(EnterpriseProjectsList));
            string exampleJson = null;
            exampleJson = "{\n  \"projects\" : {\n    \"path\" : \"path\",\n    \"enterprise_project_path\" : \"enterprise_project_path\",\n    \"public\" : 6,\n    \"name\" : \"name\",\n    \"id\" : 0\n  }\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<EnterpriseProjectsList>(exampleJson)
                        : default(EnterpriseProjectsList);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取成员的统计信息
        /// </summary>
        /// <remarks>获取成员的统计信息</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="userId">查找指定用户的数据。（可多选，用逗号分割）</param>
        /// <param name="startDate">统计的起始时间。(格式：yyyy-mm-dd)</param>
        /// <param name="endDate">统计的结束时间。(格式：yyyy-mm-dd)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="projectId">仓库 id</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/statistics/user_statistic")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdStatisticsUserStatistic")]
        [SwaggerResponse(statusCode: 200, type: typeof(List<UserStatistic>), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdStatisticsUserStatistic([FromRoute][Required]int? enterpriseId, [FromQuery][Required()]string userId, [FromQuery][Required()]string startDate, [FromQuery][Required()]string endDate, [FromQuery]string accessToken, [FromQuery]int? projectId)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(List<UserStatistic>));
            string exampleJson = null;
            exampleJson = "[ {\n  \"date\" : \"date\",\n  \"commit_count\" : 5,\n  \"create_issue_count\" : 2,\n  \"user_id\" : 6,\n  \"merge_pr_count\" : 9,\n  \"add_code_line\" : 3,\n  \"create_pr_count\" : 7,\n  \"close_issue_count\" : 5,\n  \"id\" : 0,\n  \"enterprise_id\" : 1\n}, {\n  \"date\" : \"date\",\n  \"commit_count\" : 5,\n  \"create_issue_count\" : 2,\n  \"user_id\" : 6,\n  \"merge_pr_count\" : 9,\n  \"add_code_line\" : 3,\n  \"create_pr_count\" : 7,\n  \"close_issue_count\" : 5,\n  \"id\" : 0,\n  \"enterprise_id\" : 1\n} ]";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<List<UserStatistic>>(exampleJson)
                        : default(List<UserStatistic>);            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
