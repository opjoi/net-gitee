/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;

using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class DeployKeyApiController : ControllerBase
    { 
        /// <summary>
        /// 部署公钥移除仓库
        /// </summary>
        /// <remarks>部署公钥移除仓库</remarks>
        /// <param name="deployKeyId">部署公钥id</param>
        /// <param name="projectIds">添加仓库id，英文逗号分隔</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <response code="204">返回格式</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/deploy_keys/{deploy_key_id}/projects")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdDeployKeysDeployKeyIdProjects")]
        public virtual IActionResult DeleteEnterpriseIdDeployKeysDeployKeyIdProjects([FromRoute][Required]int? deployKeyId, [FromQuery][Required()]string projectIds, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 查看企业部署公钥
        /// </summary>
        /// <remarks>查看企业部署公钥</remarks>
        /// <param name="enterpriseId">企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="projectId">仓库id</param>
        /// <param name="search">公钥SHA256指纹或标题</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/deploy_keys")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdDeployKeys")]
        [SwaggerResponse(statusCode: 200, type: typeof(EnterpriseDeployKey), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdDeployKeys([FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? projectId, [FromQuery]string search, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(EnterpriseDeployKey));
            string exampleJson = null;
            exampleJson = "{\n  \"projects\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"system\" : true,\n  \"is_pages\" : true,\n  \"projects_count\" : 6,\n  \"id\" : 0,\n  \"title\" : \"title\",\n  \"sha256_fingerprint\" : \"sha256_fingerprint\",\n  \"user\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"key\" : \"key\",\n  \"is_bae\" : true\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<EnterpriseDeployKey>(exampleJson)
                        : default(EnterpriseDeployKey);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 查看公钥部署的仓库
        /// </summary>
        /// <remarks>查看公钥部署的仓库</remarks>
        /// <param name="deployKeyId">部署公钥id</param>
        /// <param name="enterpriseId"></param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/deploy_keys/{deploy_key_id}/projects")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdDeployKeysDeployKeyIdProjects")]
        [SwaggerResponse(statusCode: 200, type: typeof(Project), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdDeployKeysDeployKeyIdProjects([FromRoute][Required]int? deployKeyId, [FromRoute][Required]int? enterpriseId, [FromQuery]string accessToken, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(Project));
            string exampleJson = null;
            exampleJson = "{\n  \"path\" : \"path\",\n  \"public\" : 5,\n  \"name\" : \"name\",\n  \"id\" : 1,\n  \"enterprise_id\" : 5\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Project>(exampleJson)
                        : default(Project);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 添加部署公钥
        /// </summary>
        /// <remarks>添加部署公钥</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId"></param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/deploy_keys")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdDeployKeys")]
        [SwaggerResponse(statusCode: 201, type: typeof(EnterpriseDeployKey), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdDeployKeys([FromBody]Body79 body, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(EnterpriseDeployKey));
            string exampleJson = null;
            exampleJson = "{\n  \"projects\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"system\" : true,\n  \"is_pages\" : true,\n  \"projects_count\" : 6,\n  \"id\" : 0,\n  \"title\" : \"title\",\n  \"sha256_fingerprint\" : \"sha256_fingerprint\",\n  \"user\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"key\" : \"key\",\n  \"is_bae\" : true\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<EnterpriseDeployKey>(exampleJson)
                        : default(EnterpriseDeployKey);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 部署公钥添加仓库
        /// </summary>
        /// <remarks>部署公钥添加仓库</remarks>
        /// <param name="body"></param>
        /// <param name="deployKeyId">部署公钥id</param>
        /// <param name="enterpriseId"></param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/deploy_keys/{deploy_key_id}/projects")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdDeployKeysDeployKeyIdProjects")]
        [SwaggerResponse(statusCode: 201, type: typeof(Project), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdDeployKeysDeployKeyIdProjects([FromBody]Body80 body, [FromRoute][Required]int? deployKeyId, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(Project));
            string exampleJson = null;
            exampleJson = "{\n  \"path\" : \"path\",\n  \"public\" : 5,\n  \"name\" : \"name\",\n  \"id\" : 1,\n  \"enterprise_id\" : 5\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Project>(exampleJson)
                        : default(Project);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 修改部署公钥
        /// </summary>
        /// <remarks>修改部署公钥</remarks>
        /// <param name="body"></param>
        /// <param name="deployKeyId">部署公钥id</param>
        /// <param name="enterpriseId"></param>
        /// <response code="200">返回格式</response>
        [HttpPut]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/deploy_keys/{deploy_key_id}")]
        [ValidateModelState]
        [SwaggerOperation("PutEnterpriseIdDeployKeysDeployKeyId")]
        [SwaggerResponse(statusCode: 200, type: typeof(EnterpriseDeployKey), description: "返回格式")]
        public virtual IActionResult PutEnterpriseIdDeployKeysDeployKeyId([FromBody]Body81 body, [FromRoute][Required]int? deployKeyId, [FromRoute][Required]int? enterpriseId)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(EnterpriseDeployKey));
            string exampleJson = null;
            exampleJson = "{\n  \"projects\" : {\n    \"path\" : \"path\",\n    \"public\" : 5,\n    \"name\" : \"name\",\n    \"id\" : 1,\n    \"enterprise_id\" : 5\n  },\n  \"system\" : true,\n  \"is_pages\" : true,\n  \"projects_count\" : 6,\n  \"id\" : 0,\n  \"title\" : \"title\",\n  \"sha256_fingerprint\" : \"sha256_fingerprint\",\n  \"user\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  },\n  \"key\" : \"key\",\n  \"is_bae\" : true\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<EnterpriseDeployKey>(exampleJson)
                        : default(EnterpriseDeployKey);            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
