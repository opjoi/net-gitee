/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Body71 : IEquatable<Body71>
    { 
        /// <summary>
        /// 用户授权码
        /// </summary>
        /// <value>用户授权码</value>
        [DataMember(Name="access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 企业角色：member => 普通成员, outsourced => 外包成员, admin => 管理员
        /// </summary>
        /// <value>企业角色：member => 普通成员, outsourced => 外包成员, admin => 管理员</value>
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum RoleEnum
        {
            /// <summary>
            /// Enum AdminEnum for admin
            /// </summary>
            [EnumMember(Value = "admin")]
            AdminEnum = 0,
            /// <summary>
            /// Enum MemberEnum for member
            /// </summary>
            [EnumMember(Value = "member")]
            MemberEnum = 1,
            /// <summary>
            /// Enum OutsourcedEnum for outsourced
            /// </summary>
            [EnumMember(Value = "outsourced")]
            OutsourcedEnum = 2        }

        /// <summary>
        /// 企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员
        /// </summary>
        /// <value>企业角色：member &#x3D;&gt; 普通成员, outsourced &#x3D;&gt; 外包成员, admin &#x3D;&gt; 管理员</value>
        [DataMember(Name="role")]
        public RoleEnum? Role { get; set; }

        /// <summary>
        /// 是否可访问企业资源，默认:是。（若选否则禁止该用户访问企业资源）
        /// </summary>
        /// <value>是否可访问企业资源，默认:是。（若选否则禁止该用户访问企业资源）</value>
        [DataMember(Name="active")]
        public bool? Active { get; set; }

        /// <summary>
        /// 企业成员真实姓名（备注）
        /// </summary>
        /// <value>企业成员真实姓名（备注）</value>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Body71 {\n");
            sb.Append("  AccessToken: ").Append(AccessToken).Append("\n");
            sb.Append("  Role: ").Append(Role).Append("\n");
            sb.Append("  Active: ").Append(Active).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Body71)obj);
        }

        /// <summary>
        /// Returns true if Body71 instances are equal
        /// </summary>
        /// <param name="other">Instance of Body71 to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Body71 other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    AccessToken == other.AccessToken ||
                    AccessToken != null &&
                    AccessToken.Equals(other.AccessToken)
                ) && 
                (
                    Role == other.Role ||
                    Role != null &&
                    Role.Equals(other.Role)
                ) && 
                (
                    Active == other.Active ||
                    Active != null &&
                    Active.Equals(other.Active)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (AccessToken != null)
                    hashCode = hashCode * 59 + AccessToken.GetHashCode();
                    if (Role != null)
                    hashCode = hashCode * 59 + Role.GetHashCode();
                    if (Active != null)
                    hashCode = hashCode * 59 + Active.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Body71 left, Body71 right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Body71 left, Body71 right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
