/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Body25 : IEquatable<Body25>
    { 
        /// <summary>
        /// 用户授权码
        /// </summary>
        /// <value>用户授权码</value>
        [DataMember(Name="access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push
        /// </summary>
        /// <value>成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push</value>
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum PermissionEnum
        {
            /// <summary>
            /// Enum PullEnum for pull
            /// </summary>
            [EnumMember(Value = "pull")]
            PullEnum = 0,
            /// <summary>
            /// Enum PushEnum for push
            /// </summary>
            [EnumMember(Value = "push")]
            PushEnum = 1,
            /// <summary>
            /// Enum AdminEnum for admin
            /// </summary>
            [EnumMember(Value = "admin")]
            AdminEnum = 2        }

        /// <summary>
        /// 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push
        /// </summary>
        /// <value>成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push</value>
        [Required]
        [DataMember(Name="permission")]
        public PermissionEnum? Permission { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Body25 {\n");
            sb.Append("  AccessToken: ").Append(AccessToken).Append("\n");
            sb.Append("  Permission: ").Append(Permission).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Body25)obj);
        }

        /// <summary>
        /// Returns true if Body25 instances are equal
        /// </summary>
        /// <param name="other">Instance of Body25 to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Body25 other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    AccessToken == other.AccessToken ||
                    AccessToken != null &&
                    AccessToken.Equals(other.AccessToken)
                ) && 
                (
                    Permission == other.Permission ||
                    Permission != null &&
                    Permission.Equals(other.Permission)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (AccessToken != null)
                    hashCode = hashCode * 59 + AccessToken.GetHashCode();
                    if (Permission != null)
                    hashCode = hashCode * 59 + Permission.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Body25 left, Body25 right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Body25 left, Body25 right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
