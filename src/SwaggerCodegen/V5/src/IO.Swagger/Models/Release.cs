/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 更新仓库Release
    /// </summary>
    [DataContract]
    public partial class Release : IEquatable<Release>
    { 
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// Gets or Sets TagName
        /// </summary>
        [DataMember(Name="tag_name")]
        public string TagName { get; set; }

        /// <summary>
        /// Gets or Sets TargetCommitish
        /// </summary>
        [DataMember(Name="target_commitish")]
        public string TargetCommitish { get; set; }

        /// <summary>
        /// Gets or Sets Prerelease
        /// </summary>
        [DataMember(Name="prerelease")]
        public string Prerelease { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Body
        /// </summary>
        [DataMember(Name="body")]
        public string Body { get; set; }

        /// <summary>
        /// Gets or Sets Author
        /// </summary>
        [DataMember(Name="author")]
        public string Author { get; set; }

        /// <summary>
        /// Gets or Sets CreatedAt
        /// </summary>
        [DataMember(Name="created_at")]
        public DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Gets or Sets Assets
        /// </summary>
        [DataMember(Name="assets")]
        public string Assets { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Release {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  TagName: ").Append(TagName).Append("\n");
            sb.Append("  TargetCommitish: ").Append(TargetCommitish).Append("\n");
            sb.Append("  Prerelease: ").Append(Prerelease).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Body: ").Append(Body).Append("\n");
            sb.Append("  Author: ").Append(Author).Append("\n");
            sb.Append("  CreatedAt: ").Append(CreatedAt).Append("\n");
            sb.Append("  Assets: ").Append(Assets).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Release)obj);
        }

        /// <summary>
        /// Returns true if Release instances are equal
        /// </summary>
        /// <param name="other">Instance of Release to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Release other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    TagName == other.TagName ||
                    TagName != null &&
                    TagName.Equals(other.TagName)
                ) && 
                (
                    TargetCommitish == other.TargetCommitish ||
                    TargetCommitish != null &&
                    TargetCommitish.Equals(other.TargetCommitish)
                ) && 
                (
                    Prerelease == other.Prerelease ||
                    Prerelease != null &&
                    Prerelease.Equals(other.Prerelease)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    Body == other.Body ||
                    Body != null &&
                    Body.Equals(other.Body)
                ) && 
                (
                    Author == other.Author ||
                    Author != null &&
                    Author.Equals(other.Author)
                ) && 
                (
                    CreatedAt == other.CreatedAt ||
                    CreatedAt != null &&
                    CreatedAt.Equals(other.CreatedAt)
                ) && 
                (
                    Assets == other.Assets ||
                    Assets != null &&
                    Assets.Equals(other.Assets)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (TagName != null)
                    hashCode = hashCode * 59 + TagName.GetHashCode();
                    if (TargetCommitish != null)
                    hashCode = hashCode * 59 + TargetCommitish.GetHashCode();
                    if (Prerelease != null)
                    hashCode = hashCode * 59 + Prerelease.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (Body != null)
                    hashCode = hashCode * 59 + Body.GetHashCode();
                    if (Author != null)
                    hashCode = hashCode * 59 + Author.GetHashCode();
                    if (CreatedAt != null)
                    hashCode = hashCode * 59 + CreatedAt.GetHashCode();
                    if (Assets != null)
                    hashCode = hashCode * 59 + Assets.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Release left, Release right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Release left, Release right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
